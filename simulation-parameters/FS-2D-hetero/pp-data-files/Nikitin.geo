nop=32;
lc = 1/nop;

Point(1) = {0, 0, 0, lc};
Point(2) = {0.5, 0, 0, lc};
Point(3) = {1, 0, 0, lc};
Point(4) = {1, 0.5, 0, lc};
Point(5) = {1, 1, 0, lc};
Point(6) = {.5, 1, 0, lc};
Point(7) = {0, 1, 0, lc};
Point(8) = {0, .5, 0, lc};
Line(1) = {1, 2};
Line(2) = {2, 8};
Line(3) = {8, 1};
Line(4) = {7, 3};
Line(5) = {3, 4};
Line(6) = {4, 6};
Line(7) = {6, 7};
Line(8) = {7, 8};
Line(9) = {2, 3};
Line(10) = {4, 5};
Line(11) = {5, 6};
Line Loop(12) = {1, 2, 3};
Plane Surface(13) = {12};
Line Loop(14) = {8, -2, 9, -4};
Plane Surface(15) = {14};
Line Loop(16) = {4, 5, 6, 7};
Plane Surface(17) = {16};
Line Loop(18) = {11, -6, 10};
Plane Surface(19) = {18};

Physical Point(10) = {1};
Physical Point(51) = {5};
Physical Point(1100) = {7,3};

Physical Line(2000) = {1,2,3,4,5,6,7,8,9,10,11};

Physical Surface(3300) = {13};
Physical Surface(3301) = {15};
Physical Surface(3302) = {17};
Physical Surface(3303) = {19};
