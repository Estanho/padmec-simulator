cl = 0.01;
Point(1) = {-0.5, -0.5, 0, cl};
Point(2) = {0.5, -0.5, 0, cl};
Point(3) = {0.5, 0.5, 0, cl};
Point(4) = {-0.5, 0.5, 0, cl};
Point(5) = {-0.15, -0.2598, 0, cl};
Point(6) = {0.15, -0.2598, 0, cl};
Point(7) = {0, 0, 0, cl};
Point(8) = {0.49, 0, 0, cl};
Line(1) = {4, 1};
Line(2) = {1, 2};
Line(3) = {2, 3};
Line(4) = {3, 4};
Circle(5) = {8, 7, 8};
Line(6) = {5, 6};
Line(7) = {6, 7};
Line(8) = {7, 5};

Line Loop(9) = {4, 1, 2, 3};
Line Loop(10) = {5};
Plane Surface(11) = {9, 10};
Line Loop(12) = {8, 6, 7};
Plane Surface(13) = {10, 12};
Plane Surface(14) = {12};

Physical Point(10) = {7};
Physical Point(51) = {5,6};

Physical Point(1100) = {1, 2, 3, 4};

Physical Line(2000) = {1, 2, 3, 4, 5};

Physical Surface(3300) = {13, 14};
Physical Surface(3301) = {11};
