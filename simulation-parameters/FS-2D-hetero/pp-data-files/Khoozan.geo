cl__1 = 0.02;
Point(1) = {0, 0, 0, cl__1};
Point(2) = {450/750, 0, 0, cl__1};
Point(3) = {750/750, 0, 0, cl__1};
Point(4) = {750/750, 30/750, 0, cl__1};
Point(5) = {750/750, 150/750, 0, cl__1};
Point(6) = {300/750, 150/750, 0, cl__1};
Point(7) = {0, 150/750, 0, cl__1};
Point(8) = {0, 120/750, 0, cl__1};
Point(9) = {0, 90/750, 0, cl__1};
Point(10) = {150/750, 30/750, 0, cl__1};
Point(11) = {600/750, 30/750, 0, cl__1};
Point(12) = {600/750, 60/750, 0, cl__1};
Point(13) = {600/750, 90/750, 0, cl__1};
Point(14) = {300/750, 90/750, 0, cl__1};
Point(15) = {300/750, 120/750, 0, cl__1};
Point(16) = {150/750, 90/750, 0, cl__1};
Point(17) = {150/750, 60/750, 0, cl__1};
Point(18) = {450/750, 30/750, 0, cl__1};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 9};
Line(9) = {9, 1};
Line(10) = {10, 18};
Line(11) = {18, 11};
Line(12) = {11, 12};
Line(13) = {12, 13};
Line(14) = {13, 14};
Line(15) = {14, 16};
Line(16) = {16, 17};
Line(17) = {17, 10};
Line(18) = {17, 12};
Line(19) = {18, 2};
Line(20) = {6, 15};
Line(21) = {15, 14};
Line(22) = {9, 16};
Line(23) = {8, 15};
Line(24) = {11, 4};
Line Loop(26) = {6, 7, 23, -20};
Plane Surface(26) = {26};
Line Loop(28) = {5, 20, 21, -14, -13, -12, 24, 4};
Plane Surface(28) = {28};
Line Loop(30) = {24, -3, -2, -19, 11};
Plane Surface(30) = {30};
Line Loop(32) = {1, -19, -10, -17, -16, -22, 9};
Plane Surface(32) = {32};
Line Loop(34) = {23, 21, 15, -22, -8};
Plane Surface(34) = {34};
Line Loop(36) = {15, 16, 18, 13, 14};
Plane Surface(36) = {36};
Line Loop(38) = {18, -12, -11, -10, -17};
Plane Surface(38) = {38};

// injection well
Physical Point(10)={1};
//Physical Line(10)={7,8,9};

// production well
Physical Point(51)={5};
//Physical Line(51)={3,4};

// dirichlet
Physical Point(1100) = {7, 3};

// boundary domains
Physical Line(2000)={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24};

// domains
Physical Surface(3300)={28,32}; // K = 1
Physical Surface(3301)={26,30,36}; // K = 10
Physical Surface(3302)={34,38}; // K = 100
