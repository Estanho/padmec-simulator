#include "FileManager.h"
#include <fstream>
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include "FileManagerFunctions.h"


#include <stdio.h>
#include <stdlib.h>
#include "Gmsh.h"
#include "GModel.h"
//#include "MElement.h"
#include "MVertexGMSH.h"
#include "meshGEdgeGMSH.h"
#include <sstream>
#include <iostream>
#include <set>
#include "SPoint3GMSH.h"



#include "AMR.h"
#include "Gmsh.h"
#include "ErrorEstimator.h"
#include "MRE.h"
//#include "Neural.h"
#include "Rebuilder2.h"
#include "Rebuilder3.h"
#include "Gmsh.h"
#include <stdio.h>
#include <stdlib.h>
#include "Gmsh.h"
#include <sstream>
#include <iostream>
#include <set>
#include "GModel.h"
#include "PView.h"
#include "PViewData.h"
#include "PluginManager.h"
#include "auxiliar.h"





using std::ofstream;
using std::ifstream;
using std::ios;

using std::endl;
using std::cout;


FileManager::FileManager (pMesh theMesh, set <pFace> BoundaryFaces, set <pEdge> BoundaryEdges, set <pVertex> BoundaryNodes,int PSCounter){
	this->theMesh=theMesh;
	this->BoundaryFaces=BoundaryFaces;
	this->BoundaryEdges=BoundaryEdges;
	this->BoundaryNodes=BoundaryNodes;
	this->PSCounter=PSCounter;
}

FileManager::FileManager (pMesh theMesh, set <pEdge> BoundaryEdges, set <pVertex> BoundaryNodes, int PSCounter){
	this->theMesh=theMesh;
	this->BoundaryEdges=BoundaryEdges;
	this->BoundaryNodes=BoundaryNodes;
	this->PSCounter=PSCounter;
}

FileManager::FileManager (){
}

FileManager::~FileManager (){
	Clear_Containers();
}

void FileManager::FileReader(char *IDS){ // Esta funcao le o arquivo de entrada, e só... atribui os elementos a um set de inteiros chamado ListofElements... A funcao iterador vai usar esse set para encontrar pelos ids os elementos na malha e coloca-los em um set de faces ou tetras a depender do caso, outras funcoes irão removê-los da malha...
	ifstream infile;
	infile.open (IDS, ifstream::in);
	infile.seekg (0, ifstream::beg);
	string line;

	while (! infile.eof() ){
		getline (infile,line);
		if (!line.empty()){
			ListofElements.insert(atoi(line.c_str()));
		}
	}
	infile.close();
}

set<int> FileManager::ReturnListofElements(){
	return ListofElements;
}


//          //          Funcao SaveFileGeometry_2D         //           //
// Funcao para salvar arquivo de Geometria .geo


void FileManager::SaveFileGeometry_2D(float Cl, pMesh theMesh, int PSCounter, int ndom, set<int> setdomains, map<int, list< set<pEdge> > > DomainsPS, set<pEdge> M_Edges, Rmsh::MainDrive *drive){ 

	SurfacesDomains.clear();

	// Salva um ARQUIVO DE GEOMETRIA padrao do gmesh DO CONTORNO DO BURACO para gerar a malha com o cl escolhido   
	//General.Verbosity
	//Level of information printed during processing (0=no information)
	//Default value: 5
	//Saved in: General.OptionsFileName

	GmshSetOption("General", "Verbosity", 99.);
	GmshSetOption("General", "DetachedMenu", 0.);
	GmshSetOption("General", "Terminal", 1.);
	GmshSetOption("Mesh", "Algorithm", 5.);
	
	
	map<int, list< set<pEdge> > >::iterator itDPS;
	list< set<pEdge> >:: iterator lit;
	set<pEdge>:: iterator eit;
	
	//Funções:
	//Recombine Surface {8};
	//Mesh.RecombineAll=1;
	//GmshSetOption("Mesh", "RecombineAll", 1.);//usar quadrangulos
	
	//const double lcMin = 1.e-3*0.01;
	const double lcMin = 1.e-2*0.1;
	const double lcMax = 10.0*0.01;
	GmshSetOption("Mesh","CharacteristicLengthMin",lcMin);
	//  GmshSetOption("Mesh","CharacteristicLengthMin",lcMax);
	GModel *a = new GModel();
	int NewEdgeID = 0;

	//cout << "ndom " << ndom << endl;
	//STOP();

	int MDOM = 0;

	set<int>::iterator std;
	for (std = setdomains.begin(); std != setdomains.end(); std++){
		int H = *std;
		if (MDOM<H){
			MDOM = H;
		}
	}



	//set<pVertex> PhysicalPoint;

	set<int> PhysicalSurf[MDOM+1];

	system("rm Geometria2D.geo");
	system("rm Geometria2D.msh");

	cout << endl;
	cout << "Salvando geometria 2D - Geometria2D" << endl;
	ofstream Myfile("./Geometria2D.geo", ios::app);

	if (!Myfile) {
		Myfile.open("./Geometria2D.geo");
	}


	// ROTINA QUE CRIA A LISTA DE PONTOS INICIAL DO ARQUIVO - IDS E COORDENADAS DE TODOS OS PONTOS
	

	Myfile << "cl1 = " << Cl << ";" << endl;
	int i = 0;

	set<pVertex> NodesJaListados;
	for (std = setdomains.begin(); std != setdomains.end(); std++){
		
		int DOMN = *std;
		itDPS = DomainsPS.find(DOMN); // Encontrando a lista do dominio

		for (lit=(itDPS->second).begin(); lit!=(itDPS->second).end() ;lit++){ // iterando dentro da lista do dominio de set em set; lit é iterador para um set.


			for (eit=(lit)->begin(); eit!=(lit)->end() ;eit++){

				pEdge D = *eit;			
				pVertex V1,V2;
				V1 = E_vertex(D, 0);
				V2 = E_vertex(D, 1);
				NodesJaListados.insert(V1);
				NodesJaListados.insert(V2);

			}

		}
		
	}

	set<pVertex>::iterator Vc;
		
	for (Vc = NodesJaListados.begin(); Vc != NodesJaListados.end() ; Vc++){ 
		double xyz[3];
		V_coord(*Vc, xyz);
		if (drive) drive->addGeoPoint(EN_id(*Vc), xyz[0], xyz[1], xyz[2], Cl);
		//if (EN_id(*Vc) == 5 || EN_id(*Vc) == 1){
			
		//	Myfile<<"Point("<<EN_id(*Vc)<<")"<<" "<<"="<<" "<<"{"<<xyz[0]<<","<<" "<<xyz[1]<<","<<" "<<xyz[2]<<","<<" 				"<<0.02<<"};"<<endl;
		//}
		//else{
			Myfile<<"Point("<<EN_id(*Vc)<<")"<<" "<<"="<<" "<<"{"<<xyz[0]<<","<<" "<<xyz[1]<<","<<" "<<xyz[2]<<","<<" 				"<<Cl<<"};"<<endl;
		//}
	}


	map <int, GVertexGMSH*> BoundaryPoints;
	BoundaryNodes.clear();
	set<pEdge>::iterator its;



	// FIM DA ROTINA, PARTE INICIAL DO ARQUIVO GRAVADO


	//		//		//		//		//		//		//		//		//


	// ROTINA QUE SALVA NO ARQUIVO TODAS AS LINES E IDS DOS PONTOS QUE AS COMPOEM AINDA DENTRO DA FUNCAO SaveFileGeometry_2D
	
	set<pEdge> BEdges;
	set<pEdge>::iterator itEds;
	pVertex Point1;
	pVertex Point2;
	BEdges.clear();

	for (itEds=BoundaryEdges.begin(); itEds != BoundaryEdges.end(); itEds++){
		BEdges.insert(*itEds);
	}

	i=0;
	int QTPS=0;
	int FGH =0;

	set<pEdge>::iterator edgit;


	//FIM DA ROTINA QUE SALVA NO ARQUIVO A LISTA DE ARESTAS e cria a copia de BoundaryEdges e o map
	FileManagerFunctions *F = new FileManagerFunctions();
	int w=1;
	int c=BEdges.size();
	int PSurf;
	set<pEdge> ArraySet[PSCounter+1]; // Array de sets as arestas de cada plane surface preenchem um set do array
	
	
	int LL;
	int P=1;
	int PS=1;  

	
	
	//int IdentifyDomains[ndom+1];
	int IdentifyDomains[MDOM+1];

	set<pEdge> NEdges; // Arestas criadas que tambem devem receber o flag 1100
		
	set<pEdge> PhyEdges;
	
		for(std=setdomains.begin();std!=setdomains.end() ;std++){  //Preciso modificar isso para iterar dentro do DomainsPS
			int DOMN = *std;
			

			itDPS = DomainsPS.find(DOMN); // Encontrando a lista do dominio

	

			for (lit=(itDPS->second).begin(); lit!=(itDPS->second).end() ;lit++){ // iterando dentro da lista do dominio de set em set; lit é iterador para iterar nas arestas de um set.

				set <int> lineloops;
				
				cout << "Verificando as arestas que savefilegeometry recebeu" << endl;
				set<pEdge>:: iterator RT;
				for(RT=lit->begin();RT!=lit->end();RT++){
				
					pVertex V1 = E_vertex(*RT,0);
					pVertex V2 = E_vertex(*RT,1);

					cout << "Aresta " << EN_id(*RT) << " Nós " << EN_id(V1) << " " << EN_id(V2) << endl;
				
				}
			
				cout << endl;
			
				while (lit->size()!=0){ ////// um while aqui para criar todos os line loops do PS
				
					// agora tenho que iterar pela lista de sets e depois dentro de cada set, cada set desse entrará no lugar de ArraySet[] e também DOMN ocupará o lugar de Domain.
	
					//int DMN=0;
					P++;
					w=1;
					
					//int Domain=0;

					//	while (w==1){
				
					for (int t=0; t <= MDOM+1; t++){
						IdentifyDomains[t] = 0;
					}
					c++;
				
					set<pEdge> CommonEdges;
	
					set<int> nwEdges;
					
			
					//if (lit->size()<4){
					//CommonEdges = *lit;
					//}
					
		
					
					
					
				
					CommonEdges = F->SeparateCommonEdges(*lit, PS); //SeparateCommonEdges retornou o set commonEdges para ser usado por createPlaneSurface, retornou UM lineloop daquele plane surface dentro de CommonEdges

					for(itEds=CommonEdges.begin(); itEds != CommonEdges.end(); itEds++){
					
						int DMN1 = 0;
						int DMN2 = 0;
						EN_getDataInt (*itEds, MD_lookupMeshDataId("Domain"), &DMN1);
						EN_getDataInt (*itEds, MD_lookupMeshDataId("Domain2"), &DMN2);
						if (DMN1!=DMN2){
							PhyEdges.insert(*itEds);
						}
					}
				
			
				
					int PD = 0;
	
					int dim = CommonEdges.size();
					//cout << "	Agora criando o lineloop, CommonEdges.size() " << CommonEdges.size() << endl; 
					LL=c;
		
					//F->CreateLineLoop(CommonEdges, c,"Geometria2D.geo",theMesh);
					F->CreateLineLoop(CommonEdges, c,"Geometria2D.geo",theMesh, drive);  //CreateLineLoop reordena estes edges
		
					lineloops.insert(LL);
					
			
					int DomainSize = 0;
					int Domain =0;
						
					for(int f=0;f<=MDOM+1;f++){
						
						if (IdentifyDomains[f]>DomainSize){ // Ta procurando qual valor do array é o maior
					
							DomainSize = IdentifyDomains[f];
							Domain = f; // O dominio do PS é o valor de f...
		
						}
			
					}
				
		
					///Retirando do set do PS para poder haver o controle
				
					for (eit=CommonEdges.begin(); eit!=CommonEdges.end() ;eit++){ 
						
						lit->erase(*eit);
					
					}
					
					CommonEdges.clear();
					
					
			}	//////////////// Fim da rotina que cria os lineloops
					
					
					PhysicalSurf[DOMN].insert(PS);
			//		cout << "Domain " << Domain << " PS " << PS << endl;

				//}
				

			//}
	
				
		
					Myfile << "Plane Surface(" << PS << ") = {"; // gravação no arquivo dos Plane surfaces
	
	


		//}//////////// Aqui fecha o laço do iterador de dominios

		//Domain = 0;	

					set<int>::iterator itints;
					itints=lineloops.begin();
					if (drive) drive->addGeo(Rmsh::MainDrive::PLANE_SURFACE, PS, *itints);
					Myfile << *itints;
					for (itints=lineloops.begin();itints!=lineloops.end();itints++){
						if (itints!=lineloops.begin()){
							if (drive) drive->addGeo(Rmsh::MainDrive::PLANE_SURFACE, PS, *itints);
							Myfile << ", " << *itints;
						}
					}
					Myfile << "};" << endl;
					lineloops.clear();
					ArraySet[PS].clear(); // Acho que nao precisa mais disso
					PS++;
		
				//}
			}
		}

	
	set<int>::iterator ints;

	Myfile << "Physical Line (2200)={";
	/*for(itEds=PhyEdges.begin();itEds!=PhyEdges.end() ;itEds++){
		if(itEds==PhyEdges.begin()){
			Myfile << EN_id(*itEds);
		}
		else{
			Myfile << ", " << EN_id(*itEds);
		}
	}*/
	for(itEds=M_Edges.begin();itEds!=M_Edges.end() ;itEds++){
		if (drive) drive->addGeo(Rmsh::MainDrive::PHYSICAL_LINE, 2200, EN_id(*itEds));
		if(itEds==M_Edges.begin()){
			Myfile << EN_id(*itEds);
		}
		else{
			Myfile << ", " << EN_id(*itEds);
		}
	}
	
	Myfile << "};" << endl;


	int M=0;





	for (std = setdomains.begin(); std != setdomains.end(); std++){ // Agora está geral para qualquer flag de surface

		M = *std;
		
		//cout <<" O size " << PhysicalSurf[M].size() << " M " << M <<  endl;
		
		
		//if (PhysicalSurf[M].size() > 0.5){
			Myfile << "Physical Surface(" << M << ") = {";
			ints=PhysicalSurf[M].begin();
			if (drive) drive->addGeo(Rmsh::MainDrive::PHYSICAL_SURFACE, M, *ints);
			Myfile << *ints;
			SurfacesDomains.insert ( pair<int, int>(*ints , M) );
			
			//int GeoF = 0;
			//EN_getDataInt (entity, MD_lookupMeshDataId("geoflag"), &GeoF);
			
			cout << "PS: " << *ints << " Physical: " << M << endl;
			for (ints=PhysicalSurf[M].begin();ints!=PhysicalSurf[M].end();ints++){
				if (ints!=PhysicalSurf[M].begin()){
					if (drive) drive->addGeo(Rmsh::MainDrive::PHYSICAL_SURFACE, M, *ints);
					Myfile << ", " << *ints;
					
					SurfacesDomains.insert ( pair<int, int>(*ints , M) );
					cout << "PS: " << *ints << " Physical: " << M << endl;
				}
			}
			Myfile << "};" << endl;
		//}
		
		M++;
	}
	
	
	
	for (M = 0; M != MDOM+1; M++){

		if (PhysicalSurf[M].size()!=0){
			//cout << "PhysicalSurf[M].size() != 0  = " << PhysicalSurf[M].size() << " M = " << M << endl;
		}
	
	}
	//STOP();


	Myfile << "nan = 0.5;" << endl;
	Myfile << "Merge \'View2D.pos\';" << endl; // gravação no arquivo dos Plane surfaces
	Myfile.close(); // fim da gravação da geometria
	cout << "fim da gravacao da geometria da malha2 2D" << endl;
	cout << endl;

	delete F; 
	//F = 0;
	//delete a; a = 0;
}


void FileManager::Save_Geometry_Lines(float Cl, pMesh theMesh, int PSCounter, int ndom, set<int> setdomains, map<int, list< set<pEdge> > > DomainsPS){ 


	SurfacesDomains.clear();

	map<int, list< set<pEdge> > >::iterator itDPS;
	list< set<pEdge> >:: iterator lit;
	set<pEdge>:: iterator eit;
	

	int NewEdgeID = 0;

	int MDOM = 0;

	set<int>::iterator std;
	for (std = setdomains.begin(); std != setdomains.end(); std++){
		int H = *std;
		if (MDOM<H){
			MDOM = H;
		}
	}


	set<int> PhysicalSurf[MDOM+1];

	system("rm Lines2D.geo");
	
	ofstream Myfile("./Lines2D.geo", ios::app);
	if (!Myfile) {
		Myfile.open("./Lines2D.geo");
	}


	// ROTINA QUE CRIA A LISTA DE PONTOS INICIAL DO ARQUIVO - IDS E COORDENADAS DE TODOS OS PONTOS
	

	Myfile << "cl1 = " << Cl << ";" << endl;
	int i = 0;

	set<pVertex> NodesJaListados;
	for (std = setdomains.begin(); std != setdomains.end(); std++){
		
		int DOMN = *std;
		itDPS = DomainsPS.find(DOMN); // Encontrando a lista do dominio

		for (lit=(itDPS->second).begin(); lit!=(itDPS->second).end() ;lit++){ // iterando dentro da lista do dominio de set em set; lit é iterador para um set.


			for (eit=(lit)->begin(); eit!=(lit)->end() ;eit++){

				pEdge D = *eit;			
				pVertex V1,V2;
				V1 = E_vertex(D, 0);
				V2 = E_vertex(D, 1);
				NodesJaListados.insert(V1);
				NodesJaListados.insert(V2);

			}

		}
		
	}

	set<pVertex>::iterator Vc;
		
	for (Vc = NodesJaListados.begin(); Vc != NodesJaListados.end() ; Vc++){ 
		double xyz[3];
		V_coord(*Vc, xyz);
		Myfile<<"Point("<<EN_id(*Vc)<<")"<<" "<<"="<<" "<<"{"<<xyz[0]<<","<<" "<<xyz[1]<<","<<" "<<xyz[2]<<","<<" 				"<<Cl<<"};"<<endl;
	}

	set<pEdge> EdgesJaListados;
	set<pEdge>::iterator ETI;

	for (std = setdomains.begin(); std != setdomains.end(); std++){
		
		int DOMN = *std;
		itDPS = DomainsPS.find(DOMN); // Encontrando a lista do dominio

		for (lit=(itDPS->second).begin(); lit!=(itDPS->second).end() ;lit++){ // iterando dentro da lista do dominio de set em set; lit é iterador para um set.


			for (eit=(lit)->begin(); eit!=(lit)->end() ;eit++){

				pEdge D = *eit;			
				pVertex V1,V2;
				V1 = E_vertex(D, 0);
				V2 = E_vertex(D, 1);
				EdgesJaListados.insert(D);
				

			}

		}
		
	}

	for (ETI = EdgesJaListados.begin(); ETI != EdgesJaListados.end() ; ETI++){ 
		
		pVertex V1,V2;
		V1 = E_vertex(*ETI, 0);
		V2 = E_vertex(*ETI, 1);
	
		Myfile<<"Line("<<EN_id(*ETI)<<")"<<" = " <<"{" << EN_id(V1) << ", " << EN_id(V2) << "};"<<endl;
	}




	// FIM DA ROTINA, PARTE INICIAL DO ARQUIVO GRAVADO


	//		//		//		//		//		//		//		//		//


	// ROTINA QUE SALVA NO ARQUIVO TODAS AS LINES E IDS DOS PONTOS QUE AS COMPOEM AINDA DENTRO DA FUNCAO SaveFileGeometry_2D
	


	Myfile.close(); // fim da gravação da geometria
	cout << "fim da gravacao da geometria da malha2 2D" << endl;
	cout << endl;

	//delete F; 
	//F = 0;
	//delete a; a = 0;
}


void FileManager::Clear_Containers(){
	ListofElements.clear();
	BoundaryFaces.clear();
	BoundaryEdges.clear();
	BoundaryNodes.clear();
	BoundVertex.clear();
	Ident = 0;
	PSCounter = 0;
}

void FileManager::SaveFileMshRenumber(pMesh theMesh, string Filename, double newID){

	cout << "Entrou em SaveFileMshRenumber" << endl;

	set<pEdge> Sedges;
	BVertex.clear();
	CVertex.clear();
	Sedges.clear();

	FileManagerFunctions *H = new FileManagerFunctions();

	EIter edgiter = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(edgiter))
	{
		if (E_numFaces(entity)==1){
		
			pVertex Vertex1 = E_vertex(entity, 0);
			pVertex Vertex2 = E_vertex(entity, 1);

			string XY = H->Create_Key_Point(Vertex1);  // Cria uma chave com as coordenadas do ponto
			string XY2 = H->Create_Key_Point(Vertex2);

			BVertex.insert ( pair<string, int>(XY, EN_id(Vertex1)) );
			BVertex.insert ( pair<string, int>(XY2, EN_id(Vertex2)) );

			CVertex.insert(Vertex1);
			CVertex.insert(Vertex2);

		}
	}
	EIter_delete(edgiter);


	int TotalEdges=0;

	EIter edgit = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(edgit))
	{

		int geoflag = 0;
		EN_getDataInt (entity, MD_lookupMeshDataId("geoflag"), &geoflag);
		
		if (geoflag != 0){
		
			Sedges.insert(entity);
		
		}
		
	}
	EIter_delete(edgit);






	string pontobarra = "./";
	ostringstream os;
	os << pontobarra;
	os << Filename;

	std::string file_name = os.str();

	string rm = "rm ";
	ostringstream rms;
	rms << rm;
	rms << Filename;

	std::string Command_rm = rms.str();

	system(Command_rm.c_str());

	cout << "Salvando malha 2D - " << file_name << endl;
	

	ofstream Myfile(file_name.c_str(), ios::app);

	if (!Myfile) {
		Myfile.open(file_name.c_str());
	}

	int TotalVertex = M_numVertices(theMesh);

	Myfile << "$NOD" << endl;
	Myfile << TotalVertex << endl;


	// Criando os vertex no arquivo de malha
	//double newID = 0;
	VIter itVertex = M_vertexIter(theMesh);
	while (pVertex VT = VIter_next(itVertex)){
		newID++;
		EN_attachDataInt (VT, MD_lookupMeshDataId("RenumberID"), newID); // Atribuindo um Id renumerado, em ordem para todos os vertex
		double xyz[3];
		V_coord(VT, xyz);
		Myfile << newID << " " << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;
	}
	VIter_delete(itVertex);



	int TotalFaces = M_numFaces(theMesh);
	Myfile << "$ENDNOD" << endl;
	Myfile << "$ELM" << endl;
	Myfile << TotalFaces + Sedges.size() << endl;


	set<pEdge>::iterator FG;
	for (FG=Sedges.begin();FG!=Sedges.end();FG++){
		newID++;
		
		pVertex V1 = E_vertex(*FG, 0);
		pVertex V2 = E_vertex(*FG, 1);
		
		int geoflag = 0;
		EN_getDataInt (*FG, MD_lookupMeshDataId("geoflag"), &geoflag);
		
		int ID1 = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("RenumberID"), &ID1);

		int ID2 = 0;
		EN_getDataInt (V2, MD_lookupMeshDataId("RenumberID"), &ID2);
		
		Myfile << newID << " 1 "<< "2000" << " " << geoflag << " 2 " << ID1 << " " << ID2 << endl;

	}


	FIter itFace = M_faceIter(theMesh);
	int df = 0;

	map<int,int>::iterator mit;
	
	cout << "VERIFICANDO O MAP -----" << SurfacesDomains.size() << endl;

	for (mit=SurfacesDomains.begin(); mit!=SurfacesDomains.end(); mit++){
	
		cout << "Chave: " << (*mit).first << " Conteudo " << (*mit).second << endl;
	
	}
	


	while (pFace FC = FIter_next(itFace)){
		newID++;
		df = getFaceFlag(FC);
		pVertex V1 = F_vertex(FC, 0);
		int ID1 = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("RenumberID"), &ID1);

		pVertex V2 = F_vertex(FC, 1);
		int ID2 = 0;
		EN_getDataInt (V2, MD_lookupMeshDataId("RenumberID"), &ID2);

		pVertex V3 = F_vertex(FC, 2);
		int ID3 = 0;
		EN_getDataInt (V3, MD_lookupMeshDataId("RenumberID"), &ID3);
		
		int geoflag = 0;
		EN_getDataInt (FC, MD_lookupMeshDataId("geoflag"), &geoflag);

		mit = SurfacesDomains.find(geoflag);
		Myfile << newID << " 2 "<< df << " " << getFaceFlag(FC) << " 3 " << ID1 << " " << ID2 << " " << ID3 << endl;
//		Myfile << newID << " 2 "<< (*mit).second << " " << geoflag << " 3 " << ID1 << " " << ID2 << " " << ID3 << endl;


	}
	FIter_delete(itFace);

//STOP();

	Myfile << "$ENDELM" << endl;
}




void FileManager::SaveFileMshRenumber_II(pMesh theMesh, string Filename, double newID, map <int,int> GMap){  //Tem que conservar os geoflags

	cout << "Entrou em SaveFileMshRenumber" << endl;

	set<pEdge> Sedges;
	BVertex.clear();
	CVertex.clear();
	Sedges.clear();

	FileManagerFunctions *H = new FileManagerFunctions();

	EIter edgiter = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(edgiter))
	{
		if (E_numFaces(entity)==1){
		
			pVertex Vertex1 = E_vertex(entity, 0);
			pVertex Vertex2 = E_vertex(entity, 1);

			string XY = H->Create_Key_Point(Vertex1);  // Cria uma chave com as coordenadas do ponto
			string XY2 = H->Create_Key_Point(Vertex2);

			BVertex.insert ( pair<string, int>(XY, EN_id(Vertex1)) );
			BVertex.insert ( pair<string, int>(XY2, EN_id(Vertex2)) );

			CVertex.insert(Vertex1);
			CVertex.insert(Vertex2);

		}
	}
	EIter_delete(edgiter);


	int TotalEdges=0;

	EIter edgit = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(edgit))
	{

		int geoflag = 0;
		EN_getDataInt (entity, MD_lookupMeshDataId("geoflag"), &geoflag);
		
		if (geoflag != 0){
		
			Sedges.insert(entity);
		
		}
		
	}
	EIter_delete(edgit);






	string pontobarra = "./";
	ostringstream os;
	os << pontobarra;
	os << Filename;

	std::string file_name = os.str();

	string rm = "rm ";
	ostringstream rms;
	rms << rm;
	rms << Filename;

	std::string Command_rm = rms.str();

	system(Command_rm.c_str());

	cout << "Salvando malha 2D - " << file_name << endl;
	

	ofstream Myfile(file_name.c_str(), ios::app);

	if (!Myfile) {
		Myfile.open(file_name.c_str());
	}

	int TotalVertex = M_numVertices(theMesh);

	Myfile << "$NOD" << endl;
	Myfile << TotalVertex << endl;


	// Criando os vertex no arquivo de malha
	//double newID = 0;
	VIter itVertex = M_vertexIter(theMesh);
	while (pVertex VT = VIter_next(itVertex)){
		newID++;
		EN_attachDataInt (VT, MD_lookupMeshDataId("RenumberID"), newID); // Atribuindo um Id renumerado, em ordem para todos os vertex
		double xyz[3];
		V_coord(VT, xyz);
		Myfile << newID << " " << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;
	}
	VIter_delete(itVertex);



	int TotalFaces = M_numFaces(theMesh);
	Myfile << "$ENDNOD" << endl;
	Myfile << "$ELM" << endl;
	Myfile << TotalFaces + Sedges.size() << endl;

	map<int,int>::iterator GT;
	set<pEdge>::iterator FG;
	for (FG=Sedges.begin();FG!=Sedges.end();FG++){
		newID++;
		
		pVertex V1 = E_vertex(*FG, 0);
		pVertex V2 = E_vertex(*FG, 1);
		
		int geoflag = 0;
		EN_getDataInt (*FG, MD_lookupMeshDataId("geoflag"), &geoflag);
		
		int ID1 = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("RenumberID"), &ID1);

		int ID2 = 0;
		EN_getDataInt (V2, MD_lookupMeshDataId("RenumberID"), &ID2);
		
		GT = GMap.find(geoflag);
		
		//Myfile << newID << " 1 "<< "2000" << " " << geoflag << " 2 " << ID1 << " " << ID2 << endl;
		Myfile << newID << " 1 "<< "2000" << " " << GT->second << " 2 " << ID1 << " " << ID2 << endl;
		
		
	

	}



	FIter itFace = M_faceIter(theMesh);
	int df = 0;

	map<int,int>::iterator mit;
	
	cout << "VERIFICANDO O MAP -----" << SurfacesDomains.size() << endl;

	for (mit=SurfacesDomains.begin(); mit!=SurfacesDomains.end(); mit++){
	
		cout << "Chave: " << (*mit).first << " Conteudo " << (*mit).second << endl;
	
	}
	


	while (pFace FC = FIter_next(itFace)){
		newID++;
		df = getFaceFlag(FC);
		pVertex V1 = F_vertex(FC, 0);
		int ID1 = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("RenumberID"), &ID1);

		pVertex V2 = F_vertex(FC, 1);
		int ID2 = 0;
		EN_getDataInt (V2, MD_lookupMeshDataId("RenumberID"), &ID2);

		pVertex V3 = F_vertex(FC, 2);
		int ID3 = 0;
		EN_getDataInt (V3, MD_lookupMeshDataId("RenumberID"), &ID3);
		
		int geoflag = 0;
		EN_getDataInt (FC, MD_lookupMeshDataId("geoflag"), &geoflag);

		mit = SurfacesDomains.find(geoflag);
//		Myfile << newID << " 2 "<< df << " " << getFaceFlag(FC) << " 3 " << ID1 << " " << ID2 << " " << ID3 << endl;
		Myfile << newID << " 2 "<< (*mit).second << " " << geoflag << " 3 " << ID1 << " " << ID2 << " " << ID3 << endl;


	}
	FIter_delete(itFace);

//STOP();

	Myfile << "$ENDELM" << endl;
	
}



void FileManager::Merge_msh(pMesh theMesh, pMesh theMesh2){

	FileManagerFunctions *H = new FileManagerFunctions();

	set <pVertex> QTDVIZINHOS;

	map <string, int> NewBoundVertex;

	int tinha = M_numFaces(theMesh);



	EIter edgiter = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(edgiter))
	{
		if (E_numFaces(entity)==1){
			pVertex Vertex1 = E_vertex(entity, 0);
			pVertex Vertex2 = E_vertex(entity, 1);

			string XY = H->Create_Key_Point(Vertex1);
			string XY2 = H->Create_Key_Point(Vertex2);

			BoundVertex.insert ( pair<string, pVertex>(XY, Vertex1) );
			BoundVertex.insert ( pair<string, pVertex>(XY2, Vertex2) );


		}
	}
	EIter_delete(edgiter);


	//////////////////////////////////////////////
	int newids = 100 * M_numVertices(theMesh);                      // Criar Rotina para definir newids de forma segura uma maneira é renumerar theMesh antes de mergê-la, assim fica int newids = M_numVertices(theMesh)
	cout << endl << " --------------------  Entrou em Merge_msh newids é " << newids << "   ----------------------" << endl << endl;					//////////////////////////////////////////////

	FIter fit = M_faceIter(theMesh2); // Notar que a malha percorrida é a theMesh2 <-- 2!

	while (pFace OrFc = FIter_next(fit)){

		pEdge Edg1 = F_edge(OrFc, 0);
		pEdge Edg2 = F_edge(OrFc, 1);
		pEdge Edg3 = F_edge(OrFc, 2);

		pVertex Vert1 = F_vertex(OrFc, 0);
		pVertex Vert2 = F_vertex(OrFc, 1);
		pVertex Vert3 = F_vertex(OrFc, 2); 

		EN_attachDataInt (Vert1, MD_lookupMeshDataId("BNode"), 0); //Colocando valor 0 em todos os vertices de theMesh2
		EN_attachDataInt (Vert2, MD_lookupMeshDataId("BNode"), 0);
		EN_attachDataInt (Vert3, MD_lookupMeshDataId("BNode"), 0);


		int NNeighEdg1 = E_numFaces(Edg1);

		//cout << "Qtd de vizinhos dessa aresta: " << NNeighEdg1 << endl;

		if (NNeighEdg1==1){
			pVertex Vt1 = E_vertex(Edg1, 0);
			pVertex Vt2 = E_vertex(Edg1, 1);
			EN_attachDataInt (Vt1, MD_lookupMeshDataId("BNode"), 1); //Substituindo o zero por 1 nas arestas de contorno
			EN_attachDataInt (Vt2, MD_lookupMeshDataId("BNode"), 1);


			string XY = H->Create_Key_Point(Vt1);

			string XY2 = H->Create_Key_Point(Vt2);

			NewBoundVertex.insert ( pair<string, int>(XY, EN_id(Vt1) + newids) ); // Esse map atualizará o map BoundVertex ao fim da função
			NewBoundVertex.insert ( pair<string, int>(XY2, EN_id(Vt2) + newids) );


		}

		int NNeighEdg2 = E_numFaces(Edg2);

		if (NNeighEdg2==1){
			pVertex Vt1 = E_vertex(Edg2, 0);
			pVertex Vt2 = E_vertex(Edg2, 1);
			EN_attachDataInt (Vt1, MD_lookupMeshDataId("BNode"), 1);//Substituindo o zero por 1 nas arestas de contorno
			EN_attachDataInt (Vt2, MD_lookupMeshDataId("BNode"), 1);


			//Criando a chave para inserir no novo map que servirá para atualizar BoundVertex

			string XY = H->Create_Key_Point(Vt1);

			string XY2 = H->Create_Key_Point(Vt2);

			NewBoundVertex.insert ( pair<string, int>(XY, EN_id(Vt1) + newids) ); // Esse map atualizará o map BoundVertex ao fim da função
			NewBoundVertex.insert ( pair<string, int>(XY2, EN_id(Vt2) + newids) );



		}
		int NNeighEdg3 = E_numFaces(Edg3);
		//cout << "Qtd de vizinhos dessa aresta: " << NNeighEdg3 << endl;
		if (NNeighEdg3==1){
			pVertex Vt1 = E_vertex(Edg3, 0);
			pVertex Vt2 = E_vertex(Edg3, 1);
			EN_attachDataInt (Vt1, MD_lookupMeshDataId("BNode"), 1);//Substituindo o zero por 1 nas arestas de contorno
			EN_attachDataInt (Vt2, MD_lookupMeshDataId("BNode"), 1);


			string XY = H->Create_Key_Point(Vt1);

			string XY2 = H->Create_Key_Point(Vt2);

			NewBoundVertex.insert ( pair<string, int>(XY, EN_id(Vt1) + newids) ); // Esse map atualizará o map BoundVertex ao fim da função
			NewBoundVertex.insert ( pair<string, int>(XY2, EN_id(Vt2) + newids) );


		}


		// Preparando a criação dos pontos

		pGEntity ent1 = V_whatIn(Vert1);
		pGEntity ent2 = V_whatIn(Vert2);
		pGEntity ent3 = V_whatIn(Vert3);

		double *xyz;
		xyz = (double*)malloc(3*sizeof(double));

		V_coord(Vert1, xyz);

		double *xyz2;
		xyz2 = (double*)malloc(3*sizeof(double));

		V_coord(Vert2, xyz);

		double *xyz3;
		xyz3 = (double*)malloc(3*sizeof(double));

		V_coord(Vert3, xyz);

		double *paramet;
		paramet = (double*)malloc(sizeof(double));
		paramet[0]=0;

		int BdNode = 0;

		map<string,pVertex>::iterator itBNd;

		int OriginalID;

		pVertex vetx;
		pVertex vetx2;
		pVertex vetx3;

		double coords[3];
		EN_getDataInt (Vert1, MD_lookupMeshDataId("BNode"), &BdNode);
		V_coord(Vert1, coords);


		if (BdNode==1) { // Se o vertex é do contorno da geometria, entao deve ser usado o id do ponto equivalente na malha original, procura-se no map BoundVertex, atraves das coordenadas que funcionam como key.


			string XY = H->Create_Key_Point(Vert1);

			itBNd=BoundVertex.find(XY); // Buscando no map o id original

			if (itBNd!=BoundVertex.end()) {
				vetx = (*itBNd).second;
			}
			else {
				BdNode=0;
			}
		}
		if (BdNode==0) { // Se o nó nao for de contorno, então é criado normalmente, com o newids mesmo...
			int IDS = EN_id(Vert1);
			int nids = IDS + newids;
			vetx = M_createVP2(theMesh, coords, paramet, nids, ent1);
		}

		EN_getDataInt (Vert2, MD_lookupMeshDataId("BNode"), &BdNode);
		V_coord(Vert2, coords);

		if (BdNode==1) { // Repetindo tudo para Vert2...

			string XY = H->Create_Key_Point(Vert2);

			itBNd=BoundVertex.find(XY); // Buscando no map o id original

			if (itBNd!=BoundVertex.end()) {
				vetx2 = (*itBNd).second;
			}
			else {
				BdNode=0;
			}

		}

		if (BdNode==0) { // Se o nó nao for de contorno, então é criado normalmente, com o newids mesmo...
			int IDS = EN_id(Vert2);
			int nids = IDS + newids;
			vetx2 = M_createVP2(theMesh, coords, paramet, nids, ent2);
		}

		EN_getDataInt (Vert3, MD_lookupMeshDataId("BNode"), &BdNode);
		V_coord(Vert3, coords);

		if (BdNode==1) { // Se o vertex é do contorno da geometria, entao deve ser usado o id do ponto equivalente na malha original, procura-se no map BoundVertex, atraves das coordenadas que funcionam como key.

			string XY = H->Create_Key_Point(Vert3);

			itBNd=BoundVertex.find(XY); // Buscando no map o id original


			if (itBNd!=BoundVertex.end()) {
				vetx3 = (*itBNd).second;
			}
			else {
				BdNode=0;
			}
		}
		if (BdNode==0) { // Se o nó nao for de contorno, então é criado normalmente, com o newids mesmo...
			int IDS = EN_id(Vert3);
			int nids = IDS + newids;
			vetx3 = M_createVP2(theMesh, coords, paramet, nids, ent3);
		}

		// Criando as arestas agora

		pEdge *edg;
		edg = (pEdge*)malloc(3*sizeof(pEdge));

		edg[0] = M_createE(theMesh, vetx, vetx2, ent1);
		edg[1] = M_createE(theMesh, vetx, vetx3, ent2);
		edg[2] = M_createE(theMesh, vetx2, vetx3, ent3);

		// Criando a face agora

		int *ide;
		ide = (int*)malloc(sizeof(int));
		ide[0]=2020;
		pFace fce = M_createF(theMesh, 3, edg, ide, ent1);

		// setando o id da face

		EN_setID(fce , 100000);

	}

	FIter_delete(fit);

	int TotalCount = 0;
	int NeighCount = 0;
	VIter vit = M_vertexIter(theMesh);
	while (pVertex ver = VIter_next(vit)){
		int ct = V_numEdges(ver);
		if (ct<=1){
			NeighCount++;
		}
		TotalCount++;
	}
	VIter_delete(vit);

	BoundVertex.clear();

	delete H; H = 0;
}



void FileManager::SaveFileGeometry_3D(float Cl){ // Salva um ARQUIVO DE GEOMETRIA padrao do gmesh DO CONTORNO DO BURACO para gerar a malha com o cl escolhido, SUBDIVIDINDO arestas

	// 1 - itera atraves de boundaryfaces para cada face cria um plane surface
	// 2 - Une todos os plane surfaces em um volume


	system("rm Geometria3D");
	system("rm Geometria3D.msh");

	cout << endl;
	cout << "Salvando geometria 3D - Geometria3D" << endl;
	ofstream Myfile("./Geometria3D", ios::app);

	if (!Myfile) {
		Myfile.open("./Geometria3D");
	}

	// ROTINA QUE CRIA A LISTA DE PONTOS INICIAL DO ARQUIVO - IDS E COORDENADAS DE TODOS OS PONTOS

	Myfile << "cl1 = " << Cl << ";" << endl;
	int i = 0;
	set<pVertex>::iterator itnodes;

	for ( itnodes=BoundaryNodes.begin() ; itnodes != BoundaryNodes.end(); itnodes++ ){

		i=i+1;
		double xyz[3];
		V_coord(*itnodes, xyz);
		Myfile<<"Point("<<EN_id(*itnodes)<<")"<<" "<<"="<<" "<<"{"<<xyz[0]<<","<<" "<<xyz[1]<<","<<" "<<xyz[2]<<","<<" 			"<<Cl<<"};"<<endl;
	}

	// FIM DA ROTINA, PARTE INICIAL DO ARQUIVO GRAVADO


	//		//		//		//		//		//		//		//		//




	// ROTINA QUE SALVA NO ARQUIVO TODAS AS LINES E IDS DOS PONTOS QUE AS COMPOEM AINDA DENTRO DA FUNCAO SaveFileGeometry_2D

	set<pEdge>::iterator itEds;
	pVertex Point1;
	pVertex Point2;


	i=0;
	map<int,pEdge> EdgesMap;  // criei um map com as arestas para atribuir ids às arestas... Uso para isso o indice do map
	map<int,pEdge>::iterator mapitedges;

	for(itEds=BoundaryEdges.begin(); itEds!=BoundaryEdges.end() ; itEds++ ){  //inserindo as arestas no map
		i=i+1;
		EdgesMap.insert ( pair<int,pEdge>(i,*itEds) );

	}

	for(mapitedges=EdgesMap.begin();mapitedges!=EdgesMap.end();mapitedges++){ // lendo o map e gravando no arquivo os dados.

		pEdge Edgex = (*mapitedges).second;
		Point1 = E_vertex(Edgex, 0);
		Point2 = E_vertex(Edgex, 1);

		int IDPoint1 = EN_id(Point1);
		int IDPoint2 = EN_id(Point2);

		Myfile<<"Line("<<EN_id(Edgex)<<")"<<" "<<"="<<" "<<"{"<<IDPoint1<<","<<" "<<IDPoint2<<"};"<<endl;

	}
	FileManagerFunctions *F = new FileManagerFunctions();
	int c=0;

	set<pFace>::iterator itFaces;


	for ( itFaces=BoundaryFaces.begin() ; itFaces != BoundaryFaces.end(); itFaces++ ){

		set<pEdge> PSEdges;

		PSEdges.insert(F_edge(*itFaces, 0));
		PSEdges.insert(F_edge(*itFaces, 1));
		PSEdges.insert(F_edge(*itFaces, 2));

		int PSurf;
		int LL;

		c++;

		int dim = PSEdges.size();

		int VFS=0;
		EN_getDataInt (*itFaces, MD_lookupMeshDataId("BoundaryFace"), &VFS);

		if ((F_numRegions(*itFaces)==0 && VFS == 1) || (F_numRegions(*itFaces)==1)){

			F->CreateLineLoop_3D(PSEdges, EN_id(*itFaces),"Geometria3D",theMesh); // troquei c por EN_id(*itFaces)

			LL=EN_id(*itFaces); // troquei c por EN_id(*itFaces)

			PSEdges.clear();

			Myfile << "Plane Surface(" << EN_id(*itFaces) << ") = { " << LL << "};" << endl; // gravação no arquivo dos Plane surfaces o planesurface é identificado pelo id da face, cada face é um plane surface.

		}
	}

	set <pFace> controlfaces;

	for ( itFaces=BoundaryFaces.begin() ; itFaces != BoundaryFaces.end(); itFaces++ ){
		controlfaces.insert(*itFaces);
	}

	int facescount = 0;
	int surfacescount = 0;
	while (controlfaces.size()>0) {

		set <pFace> commonVolumesurfaces;
		set <int> SurfacesCount;
		set <pFace> commonSurfacePlanes;

		facescount++;

		for ( itFaces=controlfaces.begin() ; itFaces != controlfaces.end(); itFaces++ ){
			int CSF = 0;
			EN_getDataInt (*itFaces, MD_lookupMeshDataId("Volume"), &CSF);
			if (CSF == facescount) {
				commonVolumesurfaces.insert(*itFaces);
			}
		}

		for ( itFaces=commonVolumesurfaces.begin() ; itFaces != commonVolumesurfaces.end(); itFaces++ ){
			controlfaces.erase(*itFaces);
		}

		int surfCount = facescount;



		while(commonVolumesurfaces.size()>0){ // Todas as faces sao do mesmo volume.

			surfacescount++;

			SurfacesCount.insert(surfacescount);

			// Colocando rotina que separa as plane surfaces
			commonSurfacePlanes.insert(*commonVolumesurfaces.begin());


			for ( itFaces=commonSurfacePlanes.begin() ; itFaces != commonSurfacePlanes.end(); itFaces++ ){

				pEdge E1 = F_edge(*itFaces , 0);
				pEdge E2 = F_edge(*itFaces , 1);
				pEdge E3 = F_edge(*itFaces , 2);

				int Q1 = E_numFaces(E1);
				int Q2 = E_numFaces(E2);
				int Q3 = E_numFaces(E3);

				for (int t=0; t<Q1;t++){ // Pegando as faces vizinhas da face através da aresta...
					pFace F1 = E_face(E1, t);
					int V = 0;
					EN_getDataInt (F1, MD_lookupMeshDataId("Volume"), &V);
					if (V==facescount){ // Se a face pertence ao volume em estudo, insere no set.
						commonSurfacePlanes.insert(F1);
					}
				}

				for (int t=0; t<Q2;t++){
					pFace F2 = E_face(E2, t);
					int V = 0;
					EN_getDataInt (F2, MD_lookupMeshDataId("Volume"), &V);
					if (V==facescount){
						commonSurfacePlanes.insert(F2);
					}
				}

				for (int t=0; t<Q3;t++){
					pFace F3 = E_face(E3, t);
					int V = 0;
					EN_getDataInt (F3, MD_lookupMeshDataId("Volume"), &V);
					if (V==facescount){
						commonSurfacePlanes.insert(F3);
					}
				}


			}

			// O volume é regido pelo facescount
			// as surface loops sao regidas pelo surfacescount

			Myfile << "Surface Loop(" << surfacescount << ") = { ";
			int VFS=0;

			for ( itFaces=commonSurfacePlanes.begin() ; itFaces != commonSurfacePlanes.end(); itFaces++ ){

				EN_getDataInt (*itFaces, MD_lookupMeshDataId("BoundaryFace"), &VFS);

				if ((F_numRegions(*itFaces)==0 && VFS == 1) || (F_numRegions(*itFaces)==1)){

					if(*itFaces!=*commonSurfacePlanes.begin()){
						Myfile << ", ";
					}

					Myfile << EN_id(*itFaces);
				}
			}
			Myfile << "};" << endl;

			for ( itFaces=commonSurfacePlanes.begin() ; itFaces != commonSurfacePlanes.end(); itFaces++ ){

				commonVolumesurfaces.erase(*itFaces);

			}
		}

		Myfile << "Volume(" << facescount << ") = { ";

		set<int>::iterator its;
		for ( its=SurfacesCount.begin() ; its != SurfacesCount.end(); its++ ){
			if(*its!=*SurfacesCount.begin()){
				Myfile << ", ";
			}
			Myfile << *its;
		}

		Myfile << "};" << endl;

		commonSurfacePlanes.clear();


	}


	Myfile.close(); // fim da gravação da geometria

	cout << "fim da gravacao da geometria da malha2 3D" << endl;
	cout << endl;

	delete F; F = 0;

}




