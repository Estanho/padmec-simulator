#include <fstream>
#include <iostream>
#include <string>
#include "FileManagerFunctions.h"
#include <cstring>
#include <sstream>

#include <stdio.h>
#include <stdlib.h>
#include "Gmsh.h"
#include "GModel.h"
//#include "MElement.h"
#include "MVertexGMSH.h"
#include "meshGEdgeGMSH.h"
#include <sstream>
#include <iostream>
#include <set>
#include "SPoint3GMSH.h"

#include "auxiliar.h"

FileManagerFunctions::FileManagerFunctions (){

}

FileManagerFunctions::~FileManagerFunctions (){

}

bool FileManagerFunctions::Compare_Nodes(pVertex V1, pVertex V2, double tol) { 

	double xyz1[3];
	double xyz2[3];
	V_coord(V1, xyz1);
	V_coord(V2, xyz2);

	double Dist = sqrt(  (xyz1[0]-xyz2[0])*(xyz1[0]-xyz2[0])   +   (xyz1[1]-xyz2[1])*(xyz1[1]-xyz2[1]));
	
	bool RT = false;
	
	if (Dist<tol){
	
	RT = true;
	
	}
	
	return RT;

}

void FileManagerFunctions::CreateLineLoop(set<pEdge> CommonEdges, int c, string FileName, pMesh theMesh) { 

//void FileManagerFunctions::CreateLineLoop(set<pEdge> CommonEdges, int c, string FileName, pMesh theMesh, map<int, GVertexGMSH*> BoundaryPoints, GModel *a) { // "recebe" um set de Edges e cria uma matriz nx3 O set é o BEdges tantas vezes quantas forem necessárias

	// Preciso que esta funcão retorne as linhas de line loops para uma matriz externa, que servirá de parametro para outra funcao, "CreatePlaneSurface" e esta sim gravará no arquivo as plane surfaces.

	//map <int, GEdgeGMSH*> BoundaryLines;
	
	cout << "Iniciou create Lineloop CommonEdges.size(): " << CommonEdges.size() << endl;
	if (CommonEdges.size()<3){
		cout << "CommonEdges menor do que 3!!!" << endl;
		//STOP();
	}
		
	
	string pontobarra = "./";
	ostringstream os;
	os << pontobarra;
	os << FileName;

	std::string filename = os.str();

	ofstream Myfile(filename.c_str(), ios::app);



	set<pEdge>:: iterator tet;
	for(tet=CommonEdges.begin();tet!=CommonEdges.end(); tet++){

		pVertex Vx1 = E_vertex(*tet, 0);
		pVertex Vx2 = E_vertex(*tet, 1);
		if(AListedEdges.count(*tet)==0){
			Myfile<<"Line("<<EN_id(*tet)<<")"<<" "<<"="<<" "<<"{"<< EN_id(Vx1) <<","<<" "<< EN_id(Vx2) <<"};"<<endl;
			AListedEdges.insert(*tet);	
		}


	}

	//STOP();

	set<pEdge>::iterator itEds;
	pVertex Point1;
	pVertex Point2;

	int Dim=CommonEdges.size(); 

	//int Dimat3 = EdgesMap.size();
	int Dimat3 = CommonEdges.size();
	int matriz3[Dimat3][3];

	for (int m=1; m<=Dimat3; m++){
		matriz3[m][1] =0;
		matriz3[m][2] =0;
		matriz3[m][3] =0;
	}




	int i=1;
	int matriz1[Dim][3];
	int matriz2[Dim][3];
	for (int m=1; m<=Dim; m++){
		matriz1[m][1] =0;
		matriz1[m][2] =0;
		matriz1[m][3] =0;
		matriz2[m][1] =0;
		matriz2[m][2] =0;
		matriz2[m][3] =0;
	}

	for ( itEds=CommonEdges.begin() ; itEds != CommonEdges.end(); itEds++ ){

		pEdge Edgex = *itEds;

		Point1 = E_vertex(Edgex, 0);
		Point2 = E_vertex(Edgex, 1);

		int IDPoint1 = EN_id(Point1);
		int IDPoint2 = EN_id(Point2);

		int k = EN_id(Edgex);
		matriz1 [i][1]=k;     // Criando a matriz Atribuindo ids às arestas usando o indice do conjunto map em vez de set
		matriz1 [i][2]=IDPoint1;
		matriz1 [i][3]=IDPoint2;
		i++;
		//cout << i <<" "<< IDPoint1 <<" "<< IDPoint2 << endl;

	}



	int b=1;
	for (int u=1; u<=3; u++){
		matriz2[1][u]=matriz1[1][u];
	}
	for(int t = 1; t<=3;t++){//zerando a 1a linha da matriz1
		matriz1[1][t]=0;
	}



	for(int f=1;f<Dim;f++){
		b=1;
		for(int h = 2; h<=3; h++){
			for (int k=2;k<=3;k++){
				if (b==1){
					for (int g=1;g<=Dim;g++){
						if (matriz2[f][h]==matriz1[g][k]){ //compara as colunas
							//copia a linha e zera a da primeira matriz
							for (int u=1; u<=3; u++){
								matriz2[f+1][u]=matriz1[g][u];

							}
							for(int r = 1; r<=3;r++){//zerando a linha correspondente da matriz1
								matriz1[g][r]=0;
							}
							b=0;
							break;
						}
					}
				}
			}
		}
	}


	for (int n=1; n<Dim;n++){
		if(matriz2[n][2]==matriz2[n+1][3]){
			matriz2[n+1][1]=-matriz2[n+1][1];
		}
		if(matriz2[n][3]==matriz2[n+1][3]){
			matriz2[n+1][1]=-matriz2[n+1][1];
		}
		if(n==Dim-1){
			if(matriz2[n+1][2]==matriz2[1][3]){
				matriz2[1][1]=-matriz2[1][1];
			}
			if(matriz2[n+1][3]==matriz2[1][3]){
				matriz2[1][1]=-matriz2[1][1];
			}
		}
	}

	int LL = c;

	Myfile <<"Line Loop("<< LL <<") = {" << matriz2[1][1];

	for ( int k = 2 ; k<=Dim; k++ ){
		Myfile << ", " << matriz2[k][1]; // a matriz2 de cada vez guarda os ids e pontos das arestas já na ordem do line loop.
	}
	Myfile <<"};" << endl;

	// Agora criar matriz de duas colunas para armazenar PS e LL.

	int psreader = 0;

	set<pEdge>::iterator itCommonEdges;
	itCommonEdges = CommonEdges.begin();
	pEdge firstEdge = *itCommonEdges;

	EN_getDataInt (firstEdge, MD_lookupMeshDataId("PlaneSurface"), &psreader);

	//cout << "O line loop " << LL << " pertence ao PS " << psreader << endl;

	//Myfile << "Plane Surface(" << PS << ") = {" << PS << "};" << endl;

	Myfile.close();

}




 // funcao alterada por Markos do Ceara, mas ficou dando problemas depois que ele colocou esse "Drive" nos parametros...
void FileManagerFunctions::CreateLineLoop(set<pEdge> CommonEdges, int c, string FileName, pMesh theMesh, Rmsh::MainDrive *drive) { 


	
	cout << "Iniciou create Lineloop CommonEdges.size(): " << CommonEdges.size() << endl;
	
	
	string pontobarra = "./";
	ostringstream os;
	os << pontobarra;
	os << FileName;

	std::string filename = os.str();

	ofstream Myfile(filename.c_str(), ios::app);



	set<pEdge>:: iterator tet;
	for(tet=CommonEdges.begin();tet!=CommonEdges.end(); tet++){

		pVertex Vx1 = E_vertex(*tet, 0);
		pVertex Vx2 = E_vertex(*tet, 1);
		if(AListedEdges.count(*tet)==0){
			if (drive) drive->addGeoList(Rmsh::MainDrive::LINE, EN_id(*tet), 2, EN_id(Vx1), EN_id(Vx2));
			Myfile<<"Line("<<EN_id(*tet)<<")"<<" "<<"="<<" "<<"{"<< EN_id(Vx1) <<","<<" "<< EN_id(Vx2) <<"};"<<endl;
			AListedEdges.insert(*tet);	
		}


	}

	//STOP();

	set<pEdge>::iterator itEds;
	pVertex Point1;
	pVertex Point2;

	int Dim=CommonEdges.size(); 

	//int Dimat3 = EdgesMap.size();
	int Dimat3 = CommonEdges.size();
	int matriz3[Dimat3][3];

	for (int m=1; m<=Dimat3; m++){
		matriz3[m][1] =0;
		matriz3[m][2] =0;
		matriz3[m][3] =0;
	}




	int i=1;
	int matriz1[Dim][3];
	int matriz2[Dim][3];
	for (int m=1; m<=Dim; m++){
		matriz1[m][1] =0;
		matriz1[m][2] =0;
		matriz1[m][3] =0;
		matriz2[m][1] =0;
		matriz2[m][2] =0;
		matriz2[m][3] =0;
	}

	for ( itEds=CommonEdges.begin() ; itEds != CommonEdges.end(); itEds++ ){

		pEdge Edgex = *itEds;

		Point1 = E_vertex(Edgex, 0);
		Point2 = E_vertex(Edgex, 1);

		int IDPoint1 = EN_id(Point1);
		int IDPoint2 = EN_id(Point2);

		int k = EN_id(Edgex);
		matriz1 [i][1]=k;     // Criando a matriz Atribuindo ids às arestas usando o indice do conjunto map em vez de set
		matriz1 [i][2]=IDPoint1;
		matriz1 [i][3]=IDPoint2;
		i++;
		//cout << i <<" "<< IDPoint1 <<" "<< IDPoint2 << endl;

	}



	int b=1;
	for (int u=1; u<=3; u++){
		matriz2[1][u]=matriz1[1][u];
	}
	for(int t = 1; t<=3;t++){//zerando a 1a linha da matriz1
		matriz1[1][t]=0;
	}



	for(int f=1;f<Dim;f++){
		b=1;
		for(int h = 2; h<=3; h++){
			for (int k=2;k<=3;k++){
				if (b==1){
					for (int g=1;g<=Dim;g++){
						if (matriz2[f][h]==matriz1[g][k]){ //compara as colunas
							//copia a linha e zera a da primeira matriz
							for (int u=1; u<=3; u++){
								matriz2[f+1][u]=matriz1[g][u];

							}
							for(int r = 1; r<=3;r++){//zerando a linha correspondente da matriz1
								matriz1[g][r]=0;
							}
							b=0;
							break;
						}
					}
				}
			}
		}
	}


	for (int n=1; n<Dim;n++){
		if(matriz2[n][2]==matriz2[n+1][3]){
			matriz2[n+1][1]=-matriz2[n+1][1];
		}
		if(matriz2[n][3]==matriz2[n+1][3]){
			matriz2[n+1][1]=-matriz2[n+1][1];
		}
		if(n==Dim-1){
			if(matriz2[n+1][2]==matriz2[1][3]){
				matriz2[1][1]=-matriz2[1][1];
			}
			if(matriz2[n+1][3]==matriz2[1][3]){
				matriz2[1][1]=-matriz2[1][1];
			}
		}
	}

	int LL = c;

	if (drive) drive->addGeo(Rmsh::MainDrive::LINE_LOOP, LL, matriz2[1][1]);
	
	cout << "Lineloop c: " << c << endl;
	
	
	Myfile <<"Line Loop("<< c <<") = {" << matriz2[1][1];

	

	for ( int k = 2 ; k<=Dim; k++ ){
		if (drive) drive->addGeo(Rmsh::MainDrive::LINE_LOOP, LL, matriz2[k][1]);
		Myfile << ", " << matriz2[k][1]; // a matriz2 de cada vez guarda os ids e pontos das arestas já na ordem do line loop.
	}
	Myfile <<"};" << endl;
	
	//STOP();

	// Agora criar matriz de duas colunas para armazenar PS e LL.

	int psreader = 0;

	set<pEdge>::iterator itCommonEdges;
	itCommonEdges = CommonEdges.begin();
	//pEdge firstEdge = *itCommonEdges;

	//EN_getDataInt (firstEdge, MD_lookupMeshDataId("PlaneSurface"), &psreader);

	//cout << "O line loop " << LL << " pertence ao PS " << psreader << endl;

	//Myfile << "Plane Surface(" << PS << ") = {" << PS << "};" << endl;

	Myfile.close();

}


	
void FileManagerFunctions::CreateLineLoop_3D(set<pEdge> CommonEdges, int c, string FileName, pMesh theMesh) { // "recebe" um set de Edges e cria uma matriz nx3 O set é o BEdges tantas vezes quantas forem necessárias

	// Preciso que esta funcão retorne as linhas de line loops para uma matriz externa, que servirá de parametro para outra funcao, "CreatePlaneSurface" e esta sim gravará no arquivo as plane surfaces.


	string pontobarra = "./";
	ostringstream os;
	os << pontobarra;
	os << FileName;

	std::string filename = os.str();

	ofstream Myfile(filename.c_str(), ios::app);



	set<pEdge>:: iterator tet;
	for(tet=CommonEdges.begin();tet!=CommonEdges.end(); tet++){
		pVertex Vx1 = E_vertex(*tet, 0);
		pVertex Vx2 = E_vertex(*tet, 1);
		//Myfile<<"Line("<<EN_id(*tet)<<")"<<" "<<"="<<" "<<"{"<< EN_id(Vx1) <<","<<" "<< EN_id(Vx2) <<"};"<<endl;
	}



	set<pEdge>::iterator itEds;
	pVertex Point1;
	pVertex Point2;

	int Dim=CommonEdges.size(); 

	//int Dimat3 = EdgesMap.size();
	int Dimat3 = CommonEdges.size();
	int matriz3[Dimat3][3];

	for (int m=1; m<=Dimat3; m++){
		matriz3[m][1] =0;
		matriz3[m][2] =0;
		matriz3[m][3] =0;
	}



	//cout << "Set CommonEdges CommonEdges.size() " << CommonEdges.size() << endl;
	for ( itEds=CommonEdges.begin() ; itEds != CommonEdges.end(); itEds++ ){

		pEdge Edgex = *itEds;

		Point1 = E_vertex(Edgex, 0);
		Point2 = E_vertex(Edgex, 1);

		int IDPoint1 = EN_id(Point1);
		int IDPoint2 = EN_id(Point2);
		//cout << "ID ARESTA " << EN_id(Edgex) << " ID PONTOS " << EN_id(Point1) << " " << EN_id(Point2) << endl;
	}

	int i=1;
	int matriz1[Dim][3];
	int matriz2[Dim][3];
	for (int m=1; m<=Dim; m++){
		matriz1[m][1] =0;
		matriz1[m][2] =0;
		matriz1[m][3] =0;
		matriz2[m][1] =0;
		matriz2[m][2] =0;
		matriz2[m][3] =0;
	}
	for ( itEds=CommonEdges.begin() ; itEds != CommonEdges.end(); itEds++ ){

		pEdge Edgex = *itEds;

		Point1 = E_vertex(Edgex, 0);
		Point2 = E_vertex(Edgex, 1);

		int IDPoint1 = EN_id(Point1);
		int IDPoint2 = EN_id(Point2);

		int k = EN_id(Edgex);
		matriz1 [i][1]=k;     // Criando a matriz Atribuindo ids às arestas usando o indice do conjunto map em vez de set
		matriz1 [i][2]=IDPoint1;
		matriz1 [i][3]=IDPoint2;
		i++;
		//cout << i <<" "<< IDPoint1 <<" "<< IDPoint2 << endl;

	}



	int b=1;
	for (int u=1; u<=3; u++){
		matriz2[1][u]=matriz1[1][u];
	}
	for(int t = 1; t<=3;t++){//zerando a 1a linha da matriz1
		matriz1[1][t]=0;
	}



	for(int f=1;f<Dim;f++){
		b=1;
		for(int h = 2; h<=3; h++){
			for (int k=2;k<=3;k++){
				if (b==1){
					for (int g=1;g<=Dim;g++){
						if (matriz2[f][h]==matriz1[g][k]){ //compara as colunas
							//copia a linha e zera a da primeira matriz
							for (int u=1; u<=3; u++){
								matriz2[f+1][u]=matriz1[g][u];

							}
							for(int r = 1; r<=3;r++){//zerando a linha correspondente da matriz1
								matriz1[g][r]=0;
							}
							b=0;
							break;
						}
					}
				}
			}
		}
	}


	for (int n=1; n<Dim;n++){
		if(matriz2[n][2]==matriz2[n+1][3]){
			matriz2[n+1][1]=-matriz2[n+1][1];
		}
		if(matriz2[n][3]==matriz2[n+1][3]){
			matriz2[n+1][1]=-matriz2[n+1][1];
		}
		if(n==Dim-1){
			if(matriz2[n+1][2]==matriz2[1][3]){
				matriz2[1][1]=-matriz2[1][1];
			}
			if(matriz2[n+1][3]==matriz2[1][3]){
				matriz2[1][1]=-matriz2[1][1];
			}
		}
	}

	//return matriz2;




	int LL = c;

	Myfile <<"Line Loop("<< LL <<") = {" << matriz2[1][1];

	for ( int k = 2 ; k<=Dim; k++ ){
		Myfile << ", " << matriz2[k][1]; // a matriz2 de cada vez guarda os ids e pontos das arestas já na ordem do line loop.
	}
	Myfile <<"};" << endl;

	// Agora criar matriz de duas colunas para armazenar PS e LL.

	int psreader = 0;

	set<pEdge>::iterator itCommonEdges;
	itCommonEdges = CommonEdges.begin();
	pEdge firstEdge = *itCommonEdges;

	EN_getDataInt (firstEdge, MD_lookupMeshDataId("PlaneSurface"), &psreader);

	//cout << "O line loop " << LL << " pertence ao PS " << psreader << endl;

	//Myfile << "Plane Surface(" << PS << ") = {" << PS << "};" << endl;

	Myfile.close();

}


///////////////////////////////////



///////////////////////////////////


set<pEdge> FileManagerFunctions::SeparateCommonEdges(set<pEdge> PSEdges, int PS){ // pega uma aresta e procura quais das outras arestas tem um ponto em comum com ela, separa UM set e deixa o resto das arestas no set original.

	cout << "Entrou em SeparateCommonEdges" << endl;

	set <pEdge> CommonEdges;
	set <pEdge> RMV_PSEDGES;
	pEntity Nodes[PSEdges.size()][3];
	pEntity Edges[PSEdges.size()][4];

	CommonEdges.clear();
	set<pEdge> CopyPSEdges;
	
	
	cout << "Arestas deste PS" << endl;
	
	set <pEdge>:: iterator rte;
	int indice = 0;


	for(rte=PSEdges.begin(); rte!=PSEdges.end(); rte++){
		CopyPSEdges.insert(*rte);
		Edges[indice][1]=*rte;
		Edges[indice][2]=E_vertex(*rte,0);
		Edges[indice][3]=E_vertex(*rte,1);
		indice++;
	}
	
	for(int i=0; i<PSEdges.size(); i++){
		cout << "Aresta " << EN_id(Edges[i][1]) << " Nós " << EN_id(Edges[i][2]) << " " << EN_id(Edges[i][3]) << " " << i << endl;
	}

	if (PSEdges.size()<3){
		cout << endl << "Plane surface com menos de 3 arestas, apenas " << PSEdges.size() << " - STOP() - FileManagerFunctions - 483" << endl << endl;
		STOP();
	}

	pEdge AntEdge = *CopyPSEdges.begin();  // Pegando uma aresta... Agora vai ver quais sao do mesmo lineloop, ou seja, quais tem pontos em comum...

	CommonEdges.insert(AntEdge);


	pVertex Node = E_vertex(AntEdge,1);


	
	int c = 0;
	int g=1;
	int size = 0;

	//STOP();

	while (g==1){  
		cout << "To no while" << endl;  // To no while procurando a outra aresta do vertex

		set<pEdge> ES; //set para armazenar as arestas dos nodes
		
;
		
		for(int i=0; i<PSEdges.size(); i++){

			if (Edges[i][2]==Node){
				cout << "encontrado o Node 1 da aresta " << EN_id(Edges[i][2]) << endl;
				ES.insert(Edges[i][1]);
			}

			if (Edges[i][3]==Node){
				cout << "Inserido o Node 2 da aresta " << EN_id(Edges[i][3]) << endl;
				ES.insert(Edges[i][1]);
			}

		}



		set<pEdge>:: iterator ESD;
		
		pEdge ProXEdg;
		
		cout <<"ES.size() " << ES.size() << endl;
		cout << "Nó " << EN_id(Node);
		
		for(ESD=ES.begin();ESD!=ES.end();ESD++){
			
			cout << "  ES.size() " << ES.size() << endl;

			pEdge edgex = *ESD;
			cout << " Aresta " << EN_id(edgex) << " ";
			if(edgex!=AntEdge){ // Se edgex for a outra aresta:
				cout << "é a outra aresta!" << endl;
				CommonEdges.insert(edgex);
				ProXEdg=edgex;


				cout << "Encontrada a outra aresta e inserida ao set, CommonEdges.size() = " << CommonEdges.size() << endl;
				cout << "Nodes da aresta : " << EN_id(E_vertex(edgex,0)) << " " << EN_id(E_vertex(edgex,1)) << endl;

			}
			else {
				cout <<"Não é a outra aresta..." << endl << endl;
			}
		}
		
		if(size==CommonEdges.size()){
			g=0;
		}
		
		size=CommonEdges.size();
		
		AntEdge = ProXEdg;
		pVertex ProX;
		for(int i=0; i<PSEdges.size(); i++){
			
			if (Edges[i][1]==AntEdge){
				if(Edges[i][2]!=Node){
					 ProX = Edges[i][2];
				}
				if(Edges[i][3]!=Node){
					 ProX = Edges[i][3];
				}
			}
			
		}
		Node = ProX;
		ES.clear();

	}

	return CommonEdges;

}



string FileManagerFunctions::Create_Key_Point(pVertex Vt1){ 

	double coords1[3];

	V_coord(Vt1, coords1);

	string ponto = "."; 	//Criando a chave para inserir no novo map que servirá para atualizar BoundVertex

	ostringstream oss;
	oss << coords1[0];
	oss << ponto;
	oss << coords1[1];
	string XY = oss.str();

	return XY;
}













