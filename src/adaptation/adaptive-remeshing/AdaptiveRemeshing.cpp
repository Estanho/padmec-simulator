/*
 * AdaptiveRemeshing.cpp
 *
 *  Created on: 26/04/2013
 *      Author: Saulo Menezes
 */
#include "AdaptiveRemeshing.h"
#include "Rmsh/MainDrive.h"


// Preciso criar uma rotina mais forte de gerenciamento dos ids dos nodes para que não haja problema nos flags da malha

void AdaptiveRemeshing::Refresh(pMesh & theMesh){

	theMesh->modifyState(3,2,1);
	theMesh->modifyState(3,1,1);
	theMesh->modifyState(3,0);

	theMesh->modifyState(2,1);
	theMesh->modifyState(2,3);
	theMesh->modifyState(2,0);

	theMesh->modifyState(1,3);
	theMesh->modifyState(1,2);
	theMesh->modifyState(1,0);

	theMesh->modifyState(0,2);
	theMesh->modifyState(0,1);
	theMesh->modifyState(0,3);

}


AdaptiveRemeshing::AdaptiveRemeshing(int argc, char* argv[]) {
	GmshInitialize(argc, argv);
	domList = 0;
	
}

AdaptiveRemeshing::~AdaptiveRemeshing() {
	GmshFinalize();
}

set<int> setdomains;

void getdomains1(pMesh m, int &n, int* domList){

	pEntity ent;
	FIter fit = M_faceIter(m);
	while ( (ent = FIter_next(fit)) ){
		setdomains.insert(GEN_tag(ent->getClassification()));

		if (GEN_tag(ent->getClassification())==0){
		
			cout << endl << "Dominio igual a zero!!" << endl << endl;
			STOP();
		
		}
	}
	FIter_delete(fit);
	cout << "Number of domains: " << setdomains.size() << endl;

	n = (int)setdomains.size();
	domList = new int[n];
	int i = 0;
	set<int>::iterator iter = setdomains.begin();
	for(;iter!=setdomains.end();iter++){
		domList[i] = *iter;
		i++;
	}

}





void AdaptiveRemeshing::rodar(ErrorAnalysis  *pErrorAnalysis, pMesh & theMesh){

	bool useCRAbMesh = false;

	system("rm Geometria2D.msh");
	system("rm Geometria2D.geo");
	system("rm View2D.pos");

	#ifdef TRACKING_PROGRAM_STEPS
	cout << "TRACKING_PROGRAM_STEPS: AdaptiveRemeshing\tIN\n";
	#endif

	static ofstream FID;
	static bool key = true;
	static int counter = 0;

	if(key){
		FID.open("adaptation-monitor.txt");
		key = false;
		getdomains1(theMesh,ndom,domList);
	}


	if (!theMesh){
		throw Exception(__LINE__,__FILE__,"NULL Mesh!");
	}

	ErrorEstimator *E = new ErrorEstimator(theMesh);

	std::set<pEntity> nodesBGMesh; 

	E->Create_Domains(theMesh);  // OK!
	
	pErrorAnalysis->getRefUnrefElementsList(theMesh,E->elementList,nodesBGMesh);
	cout << "                                    : " << M_numFaces(theMesh) << endl;
	cout << "Number of elements to be (un)refined: " << E->elementList.size() << endl;

	system("rm View2D.pos");

	FID << "Adaptation: " << ++counter << endl;
	FID << "Before\n";
	FID << "Num. elements: " << M_numFaces(theMesh) << endl;
	FID << "Num. Vertices: " << M_numVertices(theMesh) << endl;
	FID << "Num. Elements to be removed: " << E->elementList.size() << endl;

	if (!E->elementList.size()){
		throw Exception(__LINE__,__FILE__,"No elements to be (un)refined");
	}
	else{

		Rmsh::MainDrive *drive = NULL;

		if (useCRAbMesh) drive = new Rmsh::MainDrive();

		Refresh(theMesh);

		E->LeitorDeLista(pErrorAnalysis,theMesh); // OK!
		
		

		int I=0;
		if (M_numRegions(theMesh)!=0){ // verificando se a malha é 3D ou 2D...
			I=3; // malha 3D
		}
		else{
			I=2; // malha 2D
		}

		cout << "Num tetras: " << M_numRegions(theMesh) << endl;
		cout << "Num vertices: " << M_numVertices(theMesh) << endl;
		cout << "Num faces: " << M_numFaces(theMesh) << endl;

		Refresh(theMesh);
		
		Remover *p = new Remover(theMesh);
		int GrtID = p->Iterador(E->ElementsIds, E->elementList,I, theMesh);
		
		if (I==2){

			set<pFace> Stg_Elements;

			Rebuilder3 *Q;
			Q = new Rebuilder3();
			Q->get_Flags(theMesh);

			Stg_Elements = p->Identify_StgElements_II(theMesh, E->elementList, setdomains);


			cout << "Adaptive_Remeshing - Stg_Elements.size() " << Stg_Elements.size() << endl;
			

			system("rm Final_01.msh");
			
			p->BoundaryElements2D(theMesh,E->elementList, Stg_Elements);
			
			

			int numElementsToRemove = (int)E->elementList.size();

/////////////////////////
			//p->SaveBGMView1(E->elementList,numElementsToRemove, pPPData); // Salvando o arquivo da View da malha de background.
			cout <<"Prestes a entrar em SaveBGMView1" << endl;
			p->SaveBGMView1(E->elementList,numElementsToRemove); // Salvando o arquivo da View da malha de background.
			

			int PSCounter = 0;  // Contador de plane surfaces

			PSCounter = p->Identify_and_Remove_Edges_2D_II(theMesh, setdomains, E->elementList, Stg_Elements );
			
			Refresh(theMesh);

			//p->SaveADPFile(PSCounter, numElementsToRemove); //Necessário para integração com o RMesh do Ceará
			
			theMesh->modifyState(0,1,0);
			theMesh->modifyState(0,1);

			theMesh->modifyState(0,1,0);
			theMesh->modifyState(0,1,1);
			theMesh->modifyState(0,2,0);
			theMesh->modifyState(0,2,1);

			p->SaveBGMView2(numElementsToRemove, theMesh, drive); // Salvando o arquivo da View da malha de background.
			
			float GE = p->GreatestEdge;
			
			// Agora salvando os arquivos:
			
			FileManager *fm;
			fm = new FileManager(theMesh, p->ReturnBoundaryEdges(), p->ReturnBoundaryNodes(),PSCounter);
			
			set<pEdge> Merged_Edges = p->Merge_Edges(theMesh, ndom, setdomains);
			
			fm->Save_Geometry_Lines(GE, theMesh, PSCounter, ndom, setdomains, p->rDomainsPS()); 
			
			fm->SaveFileGeometry_2D(GE, theMesh, PSCounter, ndom, setdomains, p->rDomainsPS(), Merged_Edges, drive); // Neste momento o cl dos elementos é o comprimento da maior aresta  - GE
			
			cout << "Saiu de SaveFileGeometry" << endl;
			fm->Clear_Containers();

			p->removeexternaledges(theMesh);
			p->removeexternalnodes(theMesh);

			Refresh(theMesh);

			if (drive)
			{
				drive->execute();
			}
			else
			{
				Q->MakeMesh_2D("Geometria2D.geo");
				//Q->MakeMesh_2D_RMSH("Geometria2D.geo");
			}
			
			pMesh theMesh2;
			theMesh2 = MS_newMesh(0);

			cout <<"Lendo Geometria2D.msh"<< endl;
			
			if (drive)
			{
				for (Data::Mesh::VertexIterator iter = drive->vBegin();
				     iter != drive->vEnd(); iter++)
				{
					double x, y;
					int id, idGeom, idPhys;

					drive->get(iter, x, y, id, idGeom, idPhys);
					
					std::cout << id << " " << x << " " << y << " " << idPhys << " " << idGeom << std::endl;
					
					if (idGeom == -1) idGeom = 0;
					if (idPhys == -1) idPhys = 0;
					
					theMesh2->createVertex(id, x, y, 0.0, theMesh2->getGEntity(idPhys, 0));
				}

				int id = 0;

				for (Data::Boundary::FrontElementIterator iter = drive->bfeBegin();
				     iter != drive->bfeEnd(); iter++)
				{
					int id1, id2;
					int idGeom, idPhys;

					drive->get(iter, id1, id2, idGeom, idPhys);
					
					std::cout << ++id << " " << 1 << " " << idPhys << " " << idGeom << " " << 2 << " " << id1 << " " << id2 << std::endl;

					if (idPhys == -1) idPhys = 0;
					if (idGeom == -1) idGeom = 0;
					
					mEntity *theEntity = theMesh2->createEdge(id1, id2, theMesh2->getGEntity(idPhys, 1));
					EN_attachDataInt (theEntity, MD_lookupMeshDataId("geoflag"), idGeom);
				}

				for (Data::Mesh::ElementIterator iter = drive->eBegin();
				     iter != drive->eEnd(); iter++)
				{
					int id1, id2, id3;
					int idGeom, idPhys;

					drive->get(iter, id1, id2, id3, idGeom, idPhys);
					
					std::cout << ++id << " " << 2 << " " << idPhys << " " << idGeom << " " << 3 << " " << id1 << " " << id2 << " " << id3 << std::endl;
					
					if (idPhys == -1) idPhys = 0;
					if (idGeom == -1) idGeom = 0;
					
					mEntity *theEntity = theMesh2->createFaceWithVertices(id1, id2, id3, theMesh2->getGEntity(idPhys, 2));
					EN_attachDataInt (theEntity, MD_lookupMeshDataId("geoflag"), idGeom);
				}
				
				delete drive;
			}
			else
			{
				M_load(theMesh2,"Geometria2D.msh");
			}

			cout << "Lido o arquivo de malha gerado" << endl;
			cout << "Num edges: " << M_numEdges(theMesh2) << endl;

			Refresh(theMesh2);
			Refresh(theMesh);

			//STOP();
			
/////////////////////////////

			double GID = (double)GrtID;

			fm->SaveFileMshRenumber(theMesh, "theMesh.msh",0); // Este terceiro parametro é o ponto inicial de contagem dos novos ids tem que conservar os geoflags	OK!		
						
//			system("rm MalhaArrancados2D.msh");
//			fm->SaveFileMshRenumber(theMesh, "MalhaArrancados.msh",0);

			fm->SaveFileMshRenumber_II(theMesh2, "theMesh2.msh",M_numVertices(theMesh), p->ReturnGeoMap());  // SaveFileMshRenumber tem que conservar os geoflags OK!

			deleteMesh(theMesh);

			cout << endl << "Lendo theMesh.msh renumerado" << endl << endl;
			
			M_load(theMesh,"theMesh.msh");

			Refresh(theMesh);

			cout << endl <<"Lendo theMesh2.msh renumerado"<< endl << endl;

			deleteMesh(theMesh2);
			M_load(theMesh2,"theMesh2.msh");

			Refresh(theMesh2);

			Q->MergeMesh(theMesh, theMesh2);

			deleteMesh(theMesh);

			cout << endl <<"Lendo Final_02.msh "<< endl << endl;

			M_load(theMesh,"Final_02.msh");

			Refresh(theMesh);
		
			fm->SaveFileMshRenumber(theMesh, "Final_02.msh",0);  // Pega theMesh e salva com o nome de Final_02.msh
	
			deleteMesh(theMesh);
		
			M_load(theMesh,"Final_02.msh");

			Refresh(theMesh);

			Q->AttachFlags(theMesh,p->ReturnGeoMap());  // Attacha os flags e salva com Final_01.msh

			deleteMesh(theMesh2);
			deleteMesh(theMesh);

			E->Clear_Containers(); // Classe Error Estimator
			fm->Clear_Containers(); // Classe filemanager
			p->Clear_Containers(); // Classe Remover
			
			FID << "After\n";
			FID << "Num. elements: " << M_numFaces(theMesh) << endl;
			FID << "Num. Vertices: " << M_numVertices(theMesh) << "\n\n";
			
			delete E; E = 0;   //Error estimator
			delete p; p = 0;	//Remover
			delete Q; Q = 0;	//Rebuilder
			delete fm; fm = 0;	//Filemanager
			cout << "Fim da adaptacao" << endl << endl;
			
		
			
			//STOP();
			
		}
	}
	//system("rm View2D.pos Geometria2D Geometria2D.msh theMesh.msh theMesh2.msh");
	#ifdef TRACKING_PROGRAM_STEPS
	cout << "TRACKING_PROGRAM_STEPS: AdaptiveRemeshing\tOUT\n";
	#endif
}






