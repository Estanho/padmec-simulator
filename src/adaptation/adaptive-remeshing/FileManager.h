//FileManager class

#include "includes.h"
#include <string>

#include "Rmsh/MainDrive.h"

class FileManager
{
public:
	// Contructor and Destructor

	pMesh theMesh;
	pMesh theMesh2;


	FileManager ();
	FileManager (pMesh theMesh, pMesh theMesh2);
	FileManager(pMesh theMesh, set<pFace> BoundaryFaces, set<pEdge> BoundaryEdges, set<pVertex> BoundaryNodes, int PSCounter);
	FileManager(pMesh theMesh, set<pEdge> BoundaryEdges, set<pVertex> BoundaryNodes, int PSCounter);
	~FileManager();

	void Clear_Containers(); // Reseta as variaveis da classe filemanager

	void SaveFileMshRenumber (pMesh, string, double);
	void SaveFileMshRenumber_II (pMesh, string, double, map <int,int>);

	void Merge_msh (pMesh, pMesh);  // Esta funcao une os arquivos de malha
	void SaveFileGeometry_2D(float Cl, pMesh theMesh, int PSCounter, int ndom, set<int> setdomains, map<int, list< set<pEdge> > > DomainsPS, set<pEdge>, Rmsh::MainDrive *drive);  // Esta funcao Salva o arquivo de geometria necessario para o remeshing

	void Save_Geometry_Lines(float Cl, pMesh theMesh, int PSCounter, int ndom, set<int> setdomains, map<int, list< set<pEdge> > > DomainsPS);
	
	void SaveFileGeometry_3D(float Cl);  // Esta funcao Salva o arquivo de geometria necessario para o remeshing

	void FileReader (char *IDS);

	set <int> ReturnListofElements();
	int Ident;
	int PSCounter;


private:

	map <int,int> SurfacesDomains; // Mapeia qual Plane surface pertence a qual domain...
	
	map <string,pVertex > BoundVertex; // mapa dos vertices da malha original necessário para merge_msh encontrar os BoundVertex e comparar com os pontos que ela está criando.
	map <string,int > BVertex;
	set <pVertex> CVertex;
	set <int> ListofElements;
	set <pFace> BoundaryFaces;
	set <pEdge> BoundaryEdges;
	set <pVertex> BoundaryNodes;

};

