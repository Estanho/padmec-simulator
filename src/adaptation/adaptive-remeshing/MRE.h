#include "includes.h"
#include <string>

#include "Rmsh/MainDrive.h"

class Remover
{
public:

	// Contructor and Destructor
	Remover(pMesh theMesh);
	~Remover();

	std::set<pEdge> DomainEdges[];
	std::set<pEdge> PlaneSurfaceEdges;
	std::set<pVertex> GeometryBoundNodes; // acho que este está sem uso... verificar...
	std::set<pVertex> GeomBoundNodes;
	std::set<pEdge> GeomBoundEdges;
	std::set<pEdge> MeshBoundEdges;
	std::set<pEdge> MeshBoundNodes;
	std::set<pVertex> AdaptNodes;
	std::set<pEdge> AdaptEdges;
	std::set<pVertex> BoundaryNodes;
	std::set<pEdge> BoundaryEdges;
	std::set<pEdge> RemovefromBoundEdges; // tive que colocar pra nao dar erro na contagem.
	std::set<pVertex> RemovefromBoundNodes;
	std::set<pFace> RemovefromBoundFaces;
	//std::list<pFace> BoundaryFaces; // transformei o antigo std::set Faces em std::set BoundaryFaces
	std::set<pFace> GeomBoundFaces; // criado o GeomBoundFaces

	std::set<pRegion> Tetras; // Originalmente so este std::set é pRegion e o resto é pFace.
	std::vector <int> argi;
	std::map <string,pVertex> CommonVertex;
	float GreatestEdge;

	std::set <pFace> triangles;
	std::map <int,float> VertexClsAux; // Colocar esse como uma variavel local da funcao...
	std::map <int,float> VertexCls;

	typedef struct eleMatriz{
		int ID;
 		float x1,x2,x3,y1,y2,y3,z1,z2,z3;
 		float Cl1, Cl2, Cl3;
		pVertex Pt1, Pt2, Pt3;



	} EleMatriz;

	class operadorEleMatriz {
	public:
		bool operator()(const EleMatriz a, const EleMatriz b) {
			if(a.ID < b.ID) {
				return true;
			}
			return false;
		}
	};

	

	//void ClReader (char *CLS);
	
	
	void wrt_merged_mesh(pMesh theMesh);
	void get_merged_meshes(pMesh theMesh);

	void Save_LineLoop2D(pMesh theMesh, set<pEdge> EDGES);

	set<pFace> Identify_StgElements_II(pMesh Vmesh,std::list<pEntity> &elementList, set<int> setdomains);
	set<pFace> Conta_Arestas(pMesh theMesh, pVertex Vertex);
	void Clear_Containers();
	//void SaveBGMView1(const std::list<pEntity>& elementList, int size, PRS::PhysicPropData *pPPData); // Rotina para salvar view da malha de background
	void SaveBGMView1(const std::list<pEntity>& elementList, int size); // Rotina para salvar view da malha de background
	void SaveBGMView2(int size, pMesh theMesh, Rmsh::MainDrive *drive); // Rotina para salvar view da malha de background
	void Merge_Edges(pMesh theMesh2);
	void ResetMRE();
	void removeinternalfaces_2D (pMesh theMesh, std::list<pEntity> &elementList);
	set<int> removeinternalfaces (pMesh Vmesh, std::list<pEntity> &elementList);
	void removetetras (pMesh theMesh);
	int Iterador (std::set <int> ListofElements, const std::list<pEntity>& elementList ,int I, pMesh theMesh);
	void BoundaryElements3D (std::set <int> ListofElements, pMesh theMesh);
	void BoundaryElements2D (pMesh theMesh, const std::list<pEntity>& elementList,set<pFace>);
	void removeinternaledges (pMesh theMesh);
	void removeinternalnodes (pMesh theMesh);
	void removeexternaledges(pMesh theMesh);
	void removeexternalnodes(pMesh theMesh);
	void RemoveStgElements_2D(pMesh theMesh, float GE);
	void RemoveStgElements_3D(pMesh theMesh);
	void removestgNodes(pMesh theMesh);
	void SaveADPFile(int,int);
	void removestgNodes_II(pMesh theMesh, std::list<pEntity> &elementList);
	set<pEdge> Merge_PS_Edges(set<pEdge>, pMesh, int, set<pEdge> &M_Edges);
	void Get_Angular_Coef(set<pEdge> CommonEdges);
	double Get_Angular_Coef_II(pEdge);
	
	set<pEdge> Merge_Edges(pMesh theMesh, int ndom, set<int> setdomains);
	void Merge_PS(map<int, list< set<pEdge> > > DomainsPS);
	
	void CriadordeLista(pMesh theMesh); // lembrar de retirar esse daqui depois
	void CriadordeLista3D(pMesh theMesh); // lembrar de retirar esse daqui depois
	//int identifysurfaces_2D(pMesh Mesh);
	int Identify_and_Remove_Edges_2D(pMesh theMesh, set<int> setdomains);
	int Identify_and_Remove_Edges_2D_II(pMesh theMesh, set<int> setdomains, std::list<pEntity>& elementList,set<pFace> FCS);
	int identifyvolumes_3D(pMesh Mesh);
	
	void Refresh(pMesh & theMesh);

	int Ver_AC(pMesh theMesh, pEdge entity);


	map <int,int> ReturnGeoMap();
	
	map<int, list< set<pEdge> > > rDomainsPS();

	map <int,int> GeoMap;

	std::set <EleMatriz, operadorEleMatriz> STriangles;
	eleMatriz * MtxTriangles; // As coordenadas e cls dos triangulos que formam a malha de background

	std::set <pFace> ReturnBoundaryEdges();
	std::set <pFace> ReturnBoundaryNodes();

	std::map <int, pFace> MeshMap;

	list< set<pVertex>* > ListaSets;

	map<int, list< set<pEdge> > > DomainsPS;
	

private:
	///char meshFilename[];
	
	
};


