#include "Rebuilder3.h"
#include <string>

#include "AMR.h"

#include "Gmsh.h"
#include "GModel.h"
#include "GRegionGMSH.h"
#include "MEdgeGMSH.h"
#include "MVertexGMSH.h"
#include "MFaceGMSH.h"

#include <cstring>
#include <sstream>



Rebuilder3::Rebuilder3 (){
}
Rebuilder3::~Rebuilder3 (){
}

void Rebuilder3::MakeMesh_2D_RMSH(string FileName){ // Essa funcao pega a geometria que foi criada dos elementos de contorno e recria a malha



	cout << "Iniciando geração da malha a partir do arquivo de geometria" << endl; 
	
    	system("/home/padmec/jmesh/bin/made/Rmsh -g /usr/local/bin/gmsh /home/padmec/TwoPhase_Adaptation/Geometria2D.geo -o /home/padmec/TwoPhase_Adaptation/Geometria2D.msh -bg BGM.adp");

	cout << endl;
	cout << "Criado arquivo de malha Geometria2D.msh" << endl;
	cout << endl; 
	

}



void Rebuilder3::MakeMesh_2D(string FileName){ // Essa funcao pega a geometria que foi criada dos elementos de contorno e recria a malha

//	string gmsh = "./mainSimple ";
//	ostringstream os;
//	os << gmsh;
//	os << FileName;

//	std::string Command = os.str();

//	ofstream Myfile(filename.c_str(), ios::app);

	cout << "Iniciando geração da malha a partir do arquivo de geometria - Geometria2D Cl escolhido: " << endl; 
   	system("gmsh -2 Geometria2D.geo -o Geometria2D.msh -format msh1 -saveall");
   	
    	//system("/home/padmec/jmesh/bin/made/Rmsh -g /usr/local/bin/gmsh /home/padmec/TwoPhase_Adaptation/Geometria2D.geo -o /home/padmec/TwoPhase_Adaptation/Geometria2D.msh");

	cout << endl;
	cout << "Criado arquivo de malha Geometria2D.msh" << endl;
	cout << endl; 
	



}



void Rebuilder3::MakeMerge_2D(char * Arquivo1,char * Arquivo2, char * ArquivoSaida){
	GmshSetOption("General", "Verbosity", 99.);
	GmshSetOption("Mesh", "Algorithm", 5.);
	GmshSetOption("General", "Terminal", 1.);
	GModel *m = new GModel();
	m->readMSH(Arquivo1);//M1.msh
	cout << "Argumentos 1 " << Arquivo1 << endl;
	cout << "Argumentos 2 " << Arquivo2 << endl;
	GmshMergeFile(Arquivo2);//M2.msh
	m->removeInvisibleElements();
	m->removeDuplicateMeshVertices(0.0000001);
	m->writeMSH(ArquivoSaida, 1.0); //test.msh
	delete m;
}


void Rebuilder3::MergeMesh_2D(pMesh theMesh, pMesh theMesh2){
	set<pVertex> VNodes1;
	set<pVertex> VNodes2;
	set<pEdge> AEdges1;
	set<pEdge> AEdges2;
	map<string,pVertex> MNodes;
	
	system("rm Final_02.msh");
	
	int NODES = M_numVertices(theMesh);
	NODES = NODES + M_numVertices(theMesh2);
	
	int EDGES = M_numEdges(theMesh);
	EDGES = EDGES + M_numEdges(theMesh2);
	
	int FACES =0;
	int FACES1 = 0;
	FACES1 = M_numFaces(theMesh);
	int FACES2 = 0;
	FACES2 = M_numFaces(theMesh2);
	FACES = FACES1 + FACES2;
	
	ofstream Myfile("./Final_02.msh", ios::app);

	if (!Myfile) {
		Myfile.open("./Final_02.msh");
	}

	Myfile << "$NOD" << endl << NODES << endl;
	
	int MID = 0;
	
	double xyz[3];
	VIter itVertex = M_vertexIter(theMesh);
	while (pVertex VT = VIter_next(itVertex)){
	
	
		double xz2[3];
		V_coord(VT, xz2);
		
		if ( xz2[0]==0 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 10); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes1.insert(VT);

		}
		if ( xz2[0]==1 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 51); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes1.insert(VT);

		}

		if ( xz2[0]==0 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes1.insert(VT);

		}
		if ( xz2[0]==1 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes1.insert(VT);

		}
	
		V_coord(VT, xyz);
		Myfile << EN_id(VT) << " " << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;
		if (MID<EN_id(VT)){
			MID=EN_id(VT);
		}
	}
	VIter_delete(itVertex);

	MID++;
	
	VIter itV = M_vertexIter(theMesh2);  
	while (pVertex VT = VIter_next(itV)){

		double xz2[3];
		V_coord(VT, xz2);
		
		
		
		if ( xz2[0]==0 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 10); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes2.insert(VT);

		}
		if ( xz2[0]==1 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 51); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes2.insert(VT);

		}

		if ( xz2[0]==0 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes2.insert(VT);

		}
		if ( xz2[0]==1 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes2.insert(VT);

		}	
	
		V_coord(VT, xyz);
		Myfile << EN_id(VT) + MID << " " << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;

		
	}
	VIter_delete(itV);
	
	Myfile << "$ENDNOD" << endl << "$ELM" << endl;
	
	
	

	
	
	//Myfile << "MID " << MID << endl;
	
	
	int EDGEIDS = 1;
	
	EIter eit = M_edgeIter(theMesh);
	while (pEdge edge = EIter_next(eit)){
		pVertex VT1, VT2;
		VT1 = E_vertex(edge,0);
		VT2 = E_vertex(edge,1);

		//Myfile << EDGEIDS << " 1 " << 2200 << " 1 2 " << EN_id(VT1) << " " << EN_id(VT2) << endl;
		EN_setID(edge, EDGEIDS);
		EDGEIDS++;
		if (E_numFaces(edge)==1){
			AEdges1.insert(edge);
		}
		
		double coords1[3];
		double coords2[3];
		V_coord(VT1, coords1);
		V_coord(VT2, coords2);
		string ponto = "."; 

		ostringstream oss1;
		oss1 << coords1[0];
		oss1 << ponto;
		oss1 << coords1[1];
		string XY1 = oss1.str();
		
		ostringstream oss2;
		oss2 << coords2[0];
		oss2 << ponto;
		oss2 << coords2[1];
		string XY2 = oss2.str();
		

		MNodes.insert( pair<string, pVertex>(XY1, VT1) );
		MNodes.insert( pair<string, pVertex>(XY2, VT2) );

	}
	EIter_delete(eit);
	
	EIter eiterator = M_edgeIter(theMesh2);
	while (pEdge edge = EIter_next(eiterator)){
		pVertex VT1, VT2;
		VT1 = E_vertex(edge,0);
		VT2 = E_vertex(edge,1);
		
		//Myfile << EDGEIDS << " 1 " << 2200 << " 1 2 " << EN_id(VT1) << " " << EN_id(VT2) << endl;
		EN_setID(edge, EDGEIDS);
		EDGEIDS++;
		if (E_numFaces(edge)==1){
			AEdges2.insert(edge);
		}		
	}
	EIter_delete(eiterator);

	Myfile << FACES + VNodes1.size() + VNodes2.size()+ AEdges1.size() + AEdges2.size() << endl;


	set<pVertex>::iterator GH;
	int H = 0;
	for ( GH=VNodes1.begin() ; GH != VNodes1.end(); GH++ ){
	

		EN_getDataInt (*GH, MD_lookupMeshDataId("Domain"), &H);
		
		Myfile << EN_id(*GH) << " 15 "  << H << " " << EN_id(*GH) << " 1 " << EN_id(*GH) << endl;
	}
	
	for ( GH=VNodes2.begin() ; GH != VNodes2.end(); GH++ ){
	

		EN_getDataInt (*GH, MD_lookupMeshDataId("Domain"), &H);
		
		Myfile << EN_id(*GH) + MID << " 15 "  << H << " " << EN_id(*GH) + MID << " 1 " << EN_id(*GH) + MID << endl;
	}

	int FACESIDS = 1;

	FIter facit = M_faceIter(theMesh);
	while (pFace Fac = FIter_next(facit)){
		pVertex DT1, DT2, DT3;
		DT1 = F_vertex(Fac,0);
		DT2 = F_vertex(Fac,1);
		DT3 = F_vertex(Fac,2);

		Myfile << FACESIDS << " 2 " << getFaceFlag(Fac) << " " << getFaceFlag(Fac) << " 3 " << EN_id(DT1) << " " << EN_id(DT2) << " " << EN_id(DT3) << endl;
	
	FACESIDS++;
	
	}

	FIter_delete(facit);
	
	FIter fit = M_faceIter(theMesh2);
	while (pFace Fac = FIter_next(fit)){
		pVertex DT1, DT2, DT3;
		DT1 = F_vertex(Fac,0);
		DT2 = F_vertex(Fac,1);
		DT3 = F_vertex(Fac,2);
	
	Myfile << FACESIDS << " 2 " << getFaceFlag(Fac) << " " << getFaceFlag(Fac) << " 3 " << EN_id(DT1) + MID << " " << EN_id(DT2) + MID << " " << EN_id(DT3) + MID << endl;
	
	FACESIDS++;
	
	}

	FIter_delete(fit);
	
	
	
	set<pEdge>::iterator ED;
	for ( ED=AEdges1.begin(); ED != AEdges1.end(); ED++ ){

		pVertex VT1, VT2;
		VT1 = E_vertex(*ED,0);
		VT2 = E_vertex(*ED,1);

		Myfile << EN_id(*ED) << " 1 " << 2000 << " 1 2 " << EN_id(VT1) << " " << EN_id(VT2) << endl;

	}
	
	map<string,pVertex>::iterator itBNd;
	
	for ( ED=AEdges2.begin(); ED != AEdges2.end(); ED++ ){


		int IDVT1 = 0;
		int IDVT2 = 0;
		
		pVertex VT1, VT2;
		VT1 = E_vertex(*ED,0);
		VT2 = E_vertex(*ED,1);

		double coords1[3];
		double coords2[3];

		V_coord(VT1, coords1);
		V_coord(VT2, coords2);
		string ponto = "."; 

		ostringstream oss1;
		oss1 << coords1[0];
		oss1 << ponto;
		oss1 << coords1[1];
		string XY1 = oss1.str();
		
		ostringstream oss2;
		oss2 << coords2[0];
		oss2 << ponto;
		oss2 << coords2[1];
		string XY2 = oss2.str();

		
		
		itBNd=MNodes.find(XY1); // Buscando no map o id original

		if (itBNd!=MNodes.end()) {
			IDVT1 = EN_id(VT1) + MID;
		}
		else {
			IDVT1 = EN_id((*itBNd).second);
		}
		
		itBNd=MNodes.find(XY2); // Buscando no map o id original

		if (itBNd!=MNodes.end()) {
			IDVT2 = EN_id(VT2) + MID;
		}
		else {
			IDVT2 = EN_id((*itBNd).second);
		}

		Myfile << EN_id(*ED) << " 1 " << 2000 << " 1 2 " << IDVT1 << " " << IDVT2 << endl;
		
	}
	
	

Myfile << "$ENDELM" << endl;



}


void Rebuilder3::RefineMesh_2D(){ // Refina o arquivo de malha Geometria2d.msh
	cout << "Refinando malha Geometria2D.msh" << endl;
	system("gmsh -refine Geometria2D.msh");
	cout << "concluido refinamento da malha Geometria2D.msh" << endl;
	cout << endl;
}


void Rebuilder3::MakeAdaptMesh_2D(){ // Essa funcao pega a geometria que foi criada dos elementos de contorno e recria a malha
	cout << "Iniciando geração da malha a partir do arquivo de geometria - Geometria2D" << endl;
	system("gmsh -2 AdaptGeometria2D"); // ALTERAR ISSO PARA MUDAR O NOME DO ARQUIVO DE MALHA PARA MALHA2.MSH, ALGO ASSIM...
	cout << endl;
	cout << "Criado arquivo de malha Geometria2D.msh" << endl;
	cout << endl;
}

/*
void Rebuilder3::MergeMesh_BCKP_2D(pMesh theMesh, pMesh theMesh2){
	set<pVertex> VNodes1;
	set<pVertex> VNodes2;
	set<pEdge> AEdges1;
	set<pEdge> AEdges2;
	map<string,pVertex> MNodes;
	
	system("rm Final_01.msh");
	
	int NODES = M_numVertices(theMesh);
	NODES = NODES + M_numVertices(theMesh2);
	
	int EDGES = M_numEdges(theMesh);
	EDGES = EDGES + M_numEdges(theMesh2);
	
	int FACES =0;
	int FACES1 = 0;
	FACES1 = M_numFaces(theMesh);
	int FACES2 = 0;
	FACES2 = M_numFaces(theMesh2);
	FACES = FACES1 + FACES2;
	
	ofstream Myfile("./Final_01.msh", ios::app);

	if (!Myfile) {
		Myfile.open("./Final_01.msh");
	}

	Myfile << "$NOD" << endl << NODES << endl;
	
	int MID = 0;
	
	double xyz[3];
	VIter itVertex = M_vertexIter(theMesh);
	while (pVertex VT = VIter_next(itVertex)){
	
	
		double xz2[3];
		V_coord(VT, xz2);
		
		if ( xz2[0]==0 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 10); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes1.insert(VT);

		}
		if ( xz2[0]==1 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 51); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes1.insert(VT);

		}

		if ( xz2[0]==0 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes1.insert(VT);

		}
		if ( xz2[0]==1 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes1.insert(VT);

		}
	
		V_coord(VT, xyz);
		Myfile << EN_id(VT) << " " << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;
		if (MID<EN_id(VT)){
			MID=EN_id(VT);
		}
	}
	VIter_delete(itVertex);

	MID++;
	
	VIter itV = M_vertexIter(theMesh2);  
	while (pVertex VT = VIter_next(itV)){

		double xz2[3];
		V_coord(VT, xz2);
		
		
		
		if ( xz2[0]==0 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 10); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes2.insert(VT);

		}
		if ( xz2[0]==1 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 51); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes2.insert(VT);

		}

		if ( xz2[0]==0 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes2.insert(VT);

		}
		if ( xz2[0]==1 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		VNodes2.insert(VT);

		}	
	
		V_coord(VT, xyz);
		Myfile << EN_id(VT) + MID << " " << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;

		
	}
	VIter_delete(itV);
	
	Myfile << "$ENDNOD" << endl << "$ELM" << endl;
	
	
	

	
	
	//Myfile << "MID " << MID << endl;
	
	
	int EDGEIDS = 1;
	
	EIter eit = M_edgeIter(theMesh);
	while (pEdge edge = EIter_next(eit)){
		pVertex VT1, VT2;
		VT1 = E_vertex(edge,0);
		VT2 = E_vertex(edge,1);

		//Myfile << EDGEIDS << " 1 " << 2200 << " 1 2 " << EN_id(VT1) << " " << EN_id(VT2) << endl;
		EN_setID(edge, EDGEIDS);
		EDGEIDS++;
		if (E_numFaces(edge)==1){
			AEdges1.insert(edge);
		}
		
		double coords1[3];
		double coords2[3];
		V_coord(VT1, coords1);
		V_coord(VT2, coords2);
		string ponto = "."; 

		ostringstream oss1;
		oss1 << coords1[0];
		oss1 << ponto;
		oss1 << coords1[1];
		string XY1 = oss1.str();
		
		ostringstream oss2;
		oss2 << coords2[0];
		oss2 << ponto;
		oss2 << coords2[1];
		string XY2 = oss2.str();
		

		MNodes.insert( pair<string, pVertex>(XY1, VT1) );
		MNodes.insert( pair<string, pVertex>(XY2, VT2) );

	}
	EIter_delete(eit);
	
	EIter eiterator = M_edgeIter(theMesh2);
	while (pEdge edge = EIter_next(eiterator)){
		pVertex VT1, VT2;
		VT1 = E_vertex(edge,0);
		VT2 = E_vertex(edge,1);
		
		//Myfile << EDGEIDS << " 1 " << 2200 << " 1 2 " << EN_id(VT1) << " " << EN_id(VT2) << endl;
		EN_setID(edge, EDGEIDS);
		EDGEIDS++;
		if (E_numFaces(edge)==1){
			AEdges2.insert(edge);
		}		
	}
	EIter_delete(eiterator);

	Myfile << FACES + VNodes1.size() + VNodes2.size()+ AEdges1.size() + AEdges2.size() << endl;


	set<pVertex>::iterator GH;
	int H = 0;
	for ( GH=VNodes1.begin() ; GH != VNodes1.end(); GH++ ){
	

		EN_getDataInt (*GH, MD_lookupMeshDataId("Domain"), &H);
		
		Myfile << EN_id(*GH) << " 15 "  << H << " " << EN_id(*GH) << " 1 " << EN_id(*GH) << endl;
	}
	
	for ( GH=VNodes2.begin() ; GH != VNodes2.end(); GH++ ){
	

		EN_getDataInt (*GH, MD_lookupMeshDataId("Domain"), &H);
		
		Myfile << EN_id(*GH) + MID << " 15 "  << H << " " << EN_id(*GH) + MID << " 1 " << EN_id(*GH) + MID << endl;
	}

	int FACESIDS = 1;

	FIter facit = M_faceIter(theMesh);
	while (pFace Fac = FIter_next(facit)){
		pVertex DT1, DT2, DT3;
		DT1 = F_vertex(Fac,0);
		DT2 = F_vertex(Fac,1);
		DT3 = F_vertex(Fac,2);

		Myfile << FACESIDS << " 2 " << getFaceFlag(Fac) << " " << getFaceFlag(Fac) << " 3 " << EN_id(DT1) << " " << EN_id(DT2) << " " << EN_id(DT3) << endl;
	
	FACESIDS++;
	
	}

	FIter_delete(facit);
	
	FIter fit = M_faceIter(theMesh2);
	while (pFace Fac = FIter_next(fit)){
		pVertex DT1, DT2, DT3;
		DT1 = F_vertex(Fac,0);
		DT2 = F_vertex(Fac,1);
		DT3 = F_vertex(Fac,2);
	
	Myfile << FACESIDS << " 2 " << getFaceFlag(Fac) << " " << getFaceFlag(Fac) << " 3 " << EN_id(DT1) + MID << " " << EN_id(DT2) + MID << " " << EN_id(DT3) + MID << endl;
	
	FACESIDS++;
	
	}

	FIter_delete(fit);
	
	
	
	set<pEdge>::iterator ED;
	for ( ED=AEdges1.begin(); ED != AEdges1.end(); ED++ ){

		pVertex VT1, VT2;
		VT1 = E_vertex(*ED,0);
		VT2 = E_vertex(*ED,1);

		Myfile << EN_id(*ED) << " 1 " << 2000 << " 1 2 " << EN_id(VT1) << " " << EN_id(VT2) << endl;

	}
	
	map<string,pVertex>::iterator itBNd;
	
	for ( ED=AEdges2.begin(); ED != AEdges2.end(); ED++ ){


		int IDVT1 = 0;
		int IDVT2 = 0;
		
		pVertex VT1, VT2;
		VT1 = E_vertex(*ED,0);
		VT2 = E_vertex(*ED,1);

		double coords1[3];
		double coords2[3];

		V_coord(VT1, coords1);
		V_coord(VT2, coords2);
		string ponto = "."; 

		ostringstream oss1;
		oss1 << coords1[0];
		oss1 << ponto;
		oss1 << coords1[1];
		string XY1 = oss1.str();
		
		ostringstream oss2;
		oss2 << coords2[0];
		oss2 << ponto;
		oss2 << coords2[1];
		string XY2 = oss2.str();

		
		
		itBNd=MNodes.find(XY1); // Buscando no map o id original

		if (itBNd!=MNodes.end()) {
			IDVT1 = EN_id(VT1) + MID;
		}
		else {
			IDVT1 = EN_id((*itBNd).second);
		}
		
		itBNd=MNodes.find(XY2); // Buscando no map o id original

		if (itBNd!=MNodes.end()) {
			IDVT2 = EN_id(VT2) + MID;
		}
		else {
			IDVT2 = EN_id((*itBNd).second);
		}

		Myfile << EN_id(*ED) << " 1 " << 2000 << " 1 2 " << IDVT1 << " " << IDVT2 << endl;
		
	}
	
	

Myfile << "$ENDELM" << endl;



}
*/

void Rebuilder3::MergeMesh(pMesh theMesh, pMesh theMesh2){

set<pVertex> BNodes1;
set<pVertex> BNodes2;
set<pVertex> CNodes;
set<pEdge> Sedges;

set<pEdge> CEdges;

	EIter edgiter = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(edgiter)){
		if(E_numFaces(entity)==1){
			BNodes1.insert(E_vertex(entity,0));
			BNodes1.insert(E_vertex(entity,1));
			pVertex V1 = E_vertex(entity,0);
			pVertex V2 = E_vertex(entity,1);
			int NF = getVertexFlag(V1);
			if(NF>=1 && NF <=100){
				CNodes.insert(V1);
			}
			NF = getVertexFlag(V2);
			if(NF>=1 && NF <=100){
				CNodes.insert(V2);
			
			}
		}
		
		int geoflag = 0;
		EN_getDataInt (entity, MD_lookupMeshDataId("geoflag"), &geoflag);

		if (geoflag!=0){
		
			Sedges.insert(entity);
		
		}
		
		
			
	}
	EIter_delete(edgiter);

	EIter edg = M_edgeIter(theMesh2);
	while (pEdge entity = EIter_next(edg))
	{
		if(E_numFaces(entity)==1){
			BNodes2.insert(E_vertex(entity,0));
			BNodes2.insert(E_vertex(entity,1));
			pVertex V1 = E_vertex(entity,0);
			pVertex V2 = E_vertex(entity,1);
			int NF = getVertexFlag(V1);
			if(NF>=1 && NF <=100){
				CNodes.insert(V1);
			
			}
			NF = getVertexFlag(V2);
			if(NF>=1 && NF <=100){
				CNodes.insert(V2);
			
			}
			
		}
		
		int geoflag = 0;
		EN_getDataInt (entity, MD_lookupMeshDataId("geoflag"), &geoflag);

		if (geoflag!=0){
		
			Sedges.insert(entity);
		
		}
		
		
	}
	EIter_delete(edg);


	set<pVertex>::iterator B1;
	set<pVertex>::iterator B2;
					
	int COT = 0;
	for( B2=BNodes2.begin() ; B2 != BNodes2.end(); B2++ ){
		pVertex V2 = *B2;
		for( B1=BNodes1.begin() ; B1 != BNodes1.end(); B1++ ){
			pVertex V1 = *B1;
			double xyz1[3], xyz2[3];
			V_coord(V1, xyz1);
			V_coord(V2, xyz2);
			if(xyz1[0]==xyz2[0] && xyz1[1]==xyz2[1]){
				EN_attachDataInt (V2, MD_lookupMeshDataId("NOVOID"), EN_id(V1)); 
				COT++;
			}

		}
	}


	system("rm Final_02.msh");
	ofstream Myfile("Final_02.msh", ios::app);

	if (!Myfile) {
		Myfile.open("Final_02.msh");
	}

	int TotalVertex = M_numVertices(theMesh) + M_numVertices(theMesh2) - COT;

	Myfile << "$NOD" << endl;
	Myfile << TotalVertex << endl;

	VIter itVertex = M_vertexIter(theMesh);
	while (pVertex VT = VIter_next(itVertex)){
		
		double xyz[3];
		V_coord(VT, xyz);
		
		Myfile << EN_id(VT) << " " << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;
	}
	VIter_delete(itVertex);
	
	VIter itVx = M_vertexIter(theMesh2);
	while (pVertex VT = VIter_next(itVx)){
		
		double xyz[3];
		V_coord(VT, xyz);
		
		int NID = 0;
		EN_getDataInt (VT, MD_lookupMeshDataId("NOVOID"), &NID);
		if (NID==0){
			Myfile << EN_id(VT) << " " << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;
		}



		
	}
	VIter_delete(itVx);

	Myfile << "$ENDNOD" << endl;
	Myfile << "$ELM" << endl;

	int ELMID = 0;
	set<pEdge>::iterator FG;
	

	

	int CT =0;
	for (FG=Sedges.begin();FG!=Sedges.end();FG++){
		bool coincide = false; //Se a aresta coincide com aresta da outra malha
		
		ELMID++;
		
		pVertex V1 = E_vertex(*FG, 0);
		pVertex V2 = E_vertex(*FG, 1);
		
		int geoflag = 0;
		EN_getDataInt (*FG, MD_lookupMeshDataId("geoflag"), &geoflag);
		
		int ID1 = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("NOVOID"), &ID1);

		int ID2 = 0;
		EN_getDataInt (V2, MD_lookupMeshDataId("NOVOID"), &ID2);

		if(ID1!=0 && ID2!=0) coincide = true;

		if (ID1==0){
			ID1=EN_id(V1);
		}
		if (ID2==0){
			ID2=EN_id(V2);
		}
		if(!coincide)
			CT++;

	}
	
	//Myfile << M_numFaces(theMesh) + M_numFaces(theMesh2) + Sedges.size() << endl;
	Myfile << M_numFaces(theMesh) + M_numFaces(theMesh2) + CT << endl;

	for (FG=Sedges.begin();FG!=Sedges.end();FG++){
		bool coincide = false; //Se a aresta coincide com aresta da outra malha
		
		ELMID++;
		
		pVertex V1 = E_vertex(*FG, 0);
		pVertex V2 = E_vertex(*FG, 1);
		
		int geoflag = 0;
		EN_getDataInt (*FG, MD_lookupMeshDataId("geoflag"), &geoflag);
		
		int ID1 = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("NOVOID"), &ID1);

		int ID2 = 0;
		EN_getDataInt (V2, MD_lookupMeshDataId("NOVOID"), &ID2);

		if(ID1!=0 && ID2!=0) coincide = true;

		if (ID1==0){
			ID1=EN_id(V1);
		}
		if (ID2==0){
			ID2=EN_id(V2);
		}
		if(!coincide)
			Myfile << ELMID << " 1 "<< "2000" << " " << geoflag << " 2 " << ID1 << " " << ID2 << endl;

	}





	FIter itFace = M_faceIter(theMesh);
	while (pFace FC = FIter_next(itFace)){
		ELMID++;
		int df = getFaceFlag(FC);
		
		pVertex V1 = F_vertex(FC, 0);
		pVertex V2 = F_vertex(FC, 1);
		pVertex V3 = F_vertex(FC, 2);

		int ID1 = EN_id(V1);
		int ID2 = EN_id(V2);
		int ID3 = EN_id(V3);

		Myfile << ELMID << " 2 "<< df << " " << getFaceFlag(FC) - 3299 <<" 3 " << ID1 << " " << ID2  << " " << ID3 << endl;


	}

	FIter_delete(itFace);
	
	FIter itF = M_faceIter(theMesh2);
	while (pFace FC = FIter_next(itF)){
		ELMID++;

		int df = getFaceFlag(FC);
		
		pVertex V1 = F_vertex(FC, 0);
		pVertex V2 = F_vertex(FC, 1);
		pVertex V3 = F_vertex(FC, 2);
		
		int ID1, ID2, ID3;
		
		int Ver = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("NOVOID"), &Ver);
		if(Ver == 0){
			ID1 = EN_id(V1);
		}
		else{
			ID1 = Ver;
		}
		EN_getDataInt (V2, MD_lookupMeshDataId("NOVOID"), &Ver);
		if(Ver == 0){
			ID2 = EN_id(V2);
		}
		else{
			ID2 = Ver;
		}
		EN_getDataInt (V3, MD_lookupMeshDataId("NOVOID"), &Ver);
		if(Ver == 0){
			ID3 = EN_id(V3);
		}
		else{
			ID3 = Ver;
		}


		Myfile << ELMID << " 2 "<< df << " " << getFaceFlag(FC) - 3299 <<" 3 " << ID1 << " " << ID2  << " " << ID3 << endl;

	}
	FIter_delete(itF);

}


void Rebuilder3::AttachFlags(pMesh theMesh, map<int,int> GeoMap){
	set<pVertex> CNodes; // set para guardar os nodes que vao receber os flags
	set<pEdge> CEdges; // set para guardar as arestas que vao receber os flags
	cout << "Iniciou AttachFlags" << endl;
	
	cout << "Verificando o Map" << endl;
	map<int,int>::iterator GMP;
	for (GMP=GeoMap.begin();GMP!=GeoMap.end();GMP++){
	
		cout << "ID Chave -> " << GMP->first << " geoflag correto (id da mae) -> " << GMP->second << endl;
	
	}
	
	if (GeoMap.size()>2){
		cout << "Rebuilder3 1047 - STOP()" << endl;
		//STOP();
	}

	system("rm Final_01.msh");
	ofstream Myfile("Final_01.msh", ios::app);

	if (!Myfile) {
		Myfile.open("Final_01.msh");
	}

	int TotalVertex = M_numVertices(theMesh);

	Myfile << "$NOD" << endl;
	Myfile << TotalVertex << endl;
	

	VIter itVertex = M_vertexIter(theMesh);
	while (pVertex VT = VIter_next(itVertex)){

		double xyz[3];
		V_coord(VT, xyz);
		
		Myfile << EN_id(VT) << " " << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;
	
		for (int L = 0; L <= 14; L++){
			if (xyz[0] == Flag_Coords[L][1] && xyz[1] == Flag_Coords[L][2]){
				CNodes.insert(VT);

				if (Flag_Coords[L][0] > 0.9){
					EN_attachDataInt (VT, MD_lookupMeshDataId("Flag"), Flag_Coords[L][0]);
					//cout << "O flag " << Flag_Coords[L][0] << " foi inserido!" << endl;
				}
				if (Flag_Coords[L][0]==10){
				//	cout <<"Tem o FLAG 10!!"<< endl;
					//STOP();
				}

			}

		}

		if (  xyz[0] == 0 && xyz[1] == 0){ // OUTRO BACALHO 
			EN_attachDataInt (VT, MD_lookupMeshDataId("Flag"), 10);
		}

	}
	VIter_delete(itVertex);

	
	map<int,int>::iterator GT;
	int J = 0;
	EIter edg = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(edg))
	{
		int G = 0;
		EN_getDataInt (entity, MD_lookupMeshDataId("geoflag"), &G);
		GT = GeoMap.find(G);
		int CT = GeoMap.count(G);

		if (CT){
			EN_attachDataInt (entity, MD_lookupMeshDataId("geoflag"), GT->second);
			cout << "Aresta encontrada em GeoMap, geoflag attachado  " << GT->second << endl;
			if (GT->second>10){
				J = GT->second;
			}
		}
		if(E_numFaces(entity)==1){
			CEdges.insert(entity);
			EN_attachDataInt (entity, MD_lookupMeshDataId("Flag"), 2000);
			
		}
		if(E_numFaces(entity)==2){
			pFace F1 = E_face(entity,0);
			pFace F2 = E_face(entity,1);
			if (getFaceFlag(F1)!=getFaceFlag(F2)){
				CEdges.insert(entity);
				EN_attachDataInt (entity, MD_lookupMeshDataId("Flag"), 2000);
			}
		
		}
	}
	EIter_delete(edg);
	
	if (J>10){
		STOP();
	}
	int TotalEdges=0;
	set<pEdge> Sedges;
	
	cout << "Entrou no iterador de arestas"<< endl;
	
	EIter edgit = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(edgit))
	{
		if (E_numFaces(entity)==1) {
		
			Sedges.insert(entity);
			TotalEdges++;
		}
		if (E_numFaces(entity)==2) {
		
			pFace F1 = E_face(entity,0);
			pFace F2 = E_face(entity,1);
			
			if(getFaceFlag(F1)!=getFaceFlag(F2)){
			
				Sedges.insert(entity);
			
			}
		} 
	}
	EIter_delete(edgit);
	
	
	
	
	
	

	Myfile << "$ENDNOD" << endl;
	Myfile << "$ELM" << endl;
	Myfile << M_numFaces(theMesh) + CNodes.size() + Sedges.size() << endl;

	
	
	set<pVertex>::iterator ITV;
	set<pEdge>::iterator ITE;

	for (ITV=CNodes.begin(); ITV!=CNodes.end(); ITV++){
		int flag = 0;
		int L=1;
		EN_getDataInt (*ITV, MD_lookupMeshDataId("Flag"), &flag);

//		while(Flag_Coords[L][0]!=0){
		
//			double xyzCnode[3];
//			V_coord(*ITV, xyzCnode);
//			if (xyzCnode[0]==Flag_Coords[L][1] && xyzCnode[1]==Flag_Coords[L][2]){
//				flag = Flag_Coords[L][0];
//			}
//			
//		L++;
//		}
		
			Myfile << EN_id(*ITV) << " 15 " << flag << " " << EN_id(*ITV) << " 1 " << EN_id(*ITV) << endl;

		if (flag==10) {
		
			//cout << "Tem o flag 10 na insercao!!" << endl;
			//STOP();
		}
	}
	int I=4000;
	
	set<pEdge>::iterator SE;
	
//	for (ITE=CEdges.begin(); ITE!=CEdges.end(); ITE++){
	
	
//		Myfile << I << " 1 2000 3 2 " << EN_id(E_vertex(*ITE,0)) << " " << EN_id(E_vertex(*ITE,1)) << endl;
//		I++;
//	}
	for(SE=Sedges.begin(); SE!=Sedges.end() ; SE++ ){  //inserindo as arestas no map
		I++;
		
		pVertex V1 = E_vertex(*SE, 0);
		int ID1 = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("RenumberID"), &ID1);

		pVertex V2 = E_vertex(*SE, 1);
		int ID2 = 0;
		EN_getDataInt (V2, MD_lookupMeshDataId("RenumberID"), &ID2);
		
		int geoflags = 0;
		EN_getDataInt (*SE, MD_lookupMeshDataId("geoflag"), &geoflags);
		
		Myfile << I << " 1 "<< "2000 " << geoflags <<" 2 " << EN_id(V1) << " " << EN_id(V2) << endl;

	}




	FIter itFace = M_faceIter(theMesh);
	while (pFace FC = FIter_next(itFace)){
	
		int df = getFaceFlag(FC);
		
		pVertex V1 = F_vertex(FC, 0);
		pVertex V2 = F_vertex(FC, 1);
		pVertex V3 = F_vertex(FC, 2);

		int ID1 = EN_id(V1);
		int ID2 = EN_id(V2);
		int ID3 = EN_id(V3);

		Myfile << I << " 2 "<< df << " " << getFaceFlag(FC) - 3299 <<" 3 " << ID1 << " " << ID2  << " " << ID3 << endl;
		I++;

	}

	FIter_delete(itFace);
	
	Myfile << "$ENDELM" << endl; 
}

void Rebuilder3::get_Flags(pMesh theMesh){  

// Só pega os flags dos nodes por que os das arestas dá pra pegar na hora de gravar os arquivos mesmo. Estou deixando uma matriz que de pra até 110 nodes.

	int L = 1;
	VIter itVertex = M_vertexIter(theMesh);
	while (pVertex VT = VIter_next(itVertex)){

		double xyz[3];

		int flag = getVertexFlag(VT);

		if (flag >= 1 && flag <= 100 || flag == 1100){


			V_coord(VT, xyz);
			Flag_Coords [L][0]=(double)flag;
			Flag_Coords [L][1]=xyz[0];
			Flag_Coords [L][2]=xyz[1];
			L++;
			
//			if (flag == 10){
			
//				cout << "Tem o flag 10..." << endl;
//				cout <<"As coordenadas são: " << Flag_Coords [L][0] <<" e " << Flag_Coords [L][1] << endl;
//				STOP();
			
//			}
			
		}
		
		
		
	}
	VIter_delete(itVertex);



}




