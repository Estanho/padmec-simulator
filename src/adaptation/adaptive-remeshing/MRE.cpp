#include "MRE.h"
#include "auxiliar.h"
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <math.h>
#include <cmath>
#include <fstream>


// git add --all
// git commit -m " comentario da atualização "
// git push -u origin


Remover::Remover (pMesh theMesh){
	MtxTriangles = 0;
}

Remover::~Remover () {
}

void Remover::Clear_Containers(){
	PlaneSurfaceEdges.clear();
	GeometryBoundNodes.clear();
	GeomBoundNodes.clear();
	GeomBoundEdges.clear();
	MeshBoundEdges.clear();
	MeshBoundNodes.clear();
	AdaptNodes.clear();
	AdaptEdges.clear();
	BoundaryNodes.clear();
	BoundaryEdges.clear();
	RemovefromBoundEdges.clear(); 
	RemovefromBoundNodes.clear();
	RemovefromBoundFaces.clear();
	//BoundaryFaces.clear(); 
	GeomBoundFaces.clear();
	Tetras.clear();
	argi.clear();
	CommonVertex.clear();
	triangles.clear();
	VertexClsAux.clear();
	VertexCls.clear();
 	if (MtxTriangles){
 		delete[] MtxTriangles; MtxTriangles = 0;
 	}
 	GreatestEdge = 0;
}


void Remover::Refresh(pMesh & theMesh){

	theMesh->modifyState(3,2,1);
	theMesh->modifyState(3,1,1);
	theMesh->modifyState(3,0);

	theMesh->modifyState(2,1);
	theMesh->modifyState(2,3);
	theMesh->modifyState(2,0);

	theMesh->modifyState(1,3);
	theMesh->modifyState(1,2);
	theMesh->modifyState(1,0);

	theMesh->modifyState(0,2);
	theMesh->modifyState(0,1);
	theMesh->modifyState(0,3);

}


void Remover::CriadordeLista3D(pMesh theMesh){ // funcao pra criar uma lista de elementos 3D  <----- Lembrar de retirar essa funcao depois

	int iddotetra = 40;


	////////// Atualizei ate aqui ///////////

	set <pRegion> Tetrasvizinhos;
	set <pFace> facesvizinhas;

	//set <pFace> facesremove;

	RIter rit = M_regionIter(theMesh);
	while (pRegion tity = RIter_next(rit)){

		if (EN_id(tity)==iddotetra){

			facesvizinhas.insert(R_face(tity, 0));
			facesvizinhas.insert(R_face(tity, 1));
			facesvizinhas.insert(R_face(tity, 2));
			facesvizinhas.insert(R_face(tity, 3));

			set<pFace>::iterator itfc;
			for ( itfc=facesvizinhas.begin() ; itfc != facesvizinhas.end(); itfc++ ){
				pFace FEC = *itfc;
				int numReg = F_numRegions(FEC);
				for(int t=0;t<numReg;t++){
					Tetrasvizinhos.insert(F_region(FEC, t));
				}
			}
		}
	}

	RIter_delete(rit);


	set<pRegion>::iterator itfcs;
	set<pFace>::iterator itfc;

	for (int v=2;v<=6;v++){

		for ( itfcs=Tetrasvizinhos.begin() ; itfcs != Tetrasvizinhos.end(); itfcs++ ){
			if (v==9){
			}
			pRegion FEC = *itfcs;
			facesvizinhas.insert(R_face(FEC, 0));
			facesvizinhas.insert(R_face(FEC, 1));
			facesvizinhas.insert(R_face(FEC, 2));
			facesvizinhas.insert(R_face(FEC, 3));

		}


		for ( itfc=facesvizinhas.begin() ; itfc != facesvizinhas.end(); itfc++ ){
			pFace FEC = *itfc;
			int numReg = F_numRegions(FEC);
			for(int t=0;t<numReg;t++){
				Tetrasvizinhos.insert(F_region(FEC, t));
			}
		}

	}

	cout << "Terminou funcao Criador de lista 3D" << endl;

	Tetrasvizinhos.set::~set();
	facesvizinhas.set::~set();
}

// funcao pra criar uma lista de elementos 2D  <----- Lembrar de retirar essa funcao depois
void Remover::CriadordeLista(pMesh theMesh){
	int iddaface = 754;

	set <pEdge> Edgesvizinhas;
	set <pFace> facesvizinhas;
	set <pFace> facesremove;

	FIter facit = M_faceIter(theMesh);
	while (pFace Fac = FIter_next(facit)){
		if (EN_id(Fac)==iddaface){

			Edgesvizinhas.insert(F_edge(Fac, 0));
			Edgesvizinhas.insert(F_edge(Fac, 1));
			Edgesvizinhas.insert(F_edge(Fac, 2));
			set<pEdge>::iterator itfc;
			for ( itfc=Edgesvizinhas.begin() ; itfc != Edgesvizinhas.end(); itfc++ ){
				pEdge FEC = *itfc;
				facesvizinhas.insert(E_face(FEC, 0));
				facesvizinhas.insert(E_face(FEC, 1));

				pFace face0 = E_face(FEC, 0);
				pFace face1 = E_face(FEC, 1);


			}

		}
	}

	FIter_delete(facit);

	set<pFace>::iterator itfcs;
	set<pEdge>::iterator itfc;

	for (int v=2;v<=8;v++){
		for ( itfcs=facesvizinhas.begin() ; itfcs != facesvizinhas.end(); itfcs++ ){
			pFace FEC = *itfcs;
			Edgesvizinhas.insert(F_edge(FEC, 0));
			Edgesvizinhas.insert(F_edge(FEC, 1));
			Edgesvizinhas.insert(F_edge(FEC, 2));
		}


		for ( itfc=Edgesvizinhas.begin() ; itfc != Edgesvizinhas.end(); itfc++ ){
			pEdge FEC = *itfc;
			facesvizinhas.insert(E_face(FEC, 0));
			facesvizinhas.insert(E_face(FEC, 1));
		}
		if (v==3){
			for ( itfcs=facesvizinhas.begin() ; itfcs != facesvizinhas.end(); itfcs++ ){
				pFace RETG = *itfcs;
				facesremove.insert(*itfcs);


			}	
		}

	}
}


//void Remover::SaveBGMView1(const std::list<pEntity>& elementList, int size, PRS::PhysicPropData *pPPData){
void Remover::SaveBGMView1(const std::list<pEntity>& elementList, int size){

	bool WayNodes = false;
	MtxTriangles = new eleMatriz[size];
	std::list<pEntity>::const_iterator itFaces;
	double xyz1[3];
	double xyz2[3];
	double xyz3[3];
	
	int i = 0;
	for(itFaces=elementList.begin(); (itFaces!=elementList.end()); itFaces++){
		pFace face = *itFaces;
		//pVertex Node = (pVertex)face->get(0,0);
		float FCT = 1;
		double height;
		int WN = 0;
		EN_getDataDbl((pVertex)face->get(0,0),MD_lookupMeshDataId( "elem_height" ),&height);
		if (WayNodes){
			EN_getDataInt ((pVertex)face->get(0,0), MD_lookupMeshDataId("WayNode"), &WN);
		
			if (WN){
				FCT = 0.8;
			}
			else{
				FCT = 1;
			}
		}
		MtxTriangles[i].Cl1 = (float)height * FCT;
		//double val = .0;
		//val = pPPData->getSaturation(node);
		
		EN_getDataDbl((pVertex)face->get(0,1),MD_lookupMeshDataId( "elem_height" ),&height);
		if (WayNodes){
			EN_getDataInt ((pVertex)face->get(0,1), MD_lookupMeshDataId("WayNode"), &WN);
			if (WN){
				FCT = 0.8;
			}
			else{
				FCT = 1;
			}
		}
		MtxTriangles[i].Cl2 = (float)height * FCT;
		
		EN_getDataDbl((pVertex)face->get(0,2),MD_lookupMeshDataId( "elem_height" ),&height);
		if (WayNodes){
			EN_getDataInt ((pVertex)face->get(0,2), MD_lookupMeshDataId("WayNode"), &WN);
			if (WN){
				FCT = 0.8;
			}
			else{
				FCT = 1;
			}
		}
		MtxTriangles[i].Cl3 = (float)height * FCT;

		V_coord((pVertex)face->get(0,0), xyz1);
		V_coord((pVertex)face->get(0,1), xyz2);
		V_coord((pVertex)face->get(0,2), xyz3);

		MtxTriangles[i].x1=xyz1[0];
		MtxTriangles[i].y1=xyz1[1];
		MtxTriangles[i].z1=xyz1[2];

		MtxTriangles[i].x2=xyz2[0];
		MtxTriangles[i].y2=xyz2[1];
		MtxTriangles[i].z2=xyz2[2];

		MtxTriangles[i].x3=xyz3[0];
		MtxTriangles[i].y3=xyz3[1];
		MtxTriangles[i].z3=xyz3[2];

		MtxTriangles[i].Pt1 = (pVertex)face->get(0,0);
		MtxTriangles[i].Pt2 = (pVertex)face->get(0,1);
		MtxTriangles[i].Pt3 = (pVertex)face->get(0,2);
		i++;
	}
	cout << "Concluido SaveBGMView1" << endl;
}

void Remover::SaveBGMView2(int size, pMesh theMesh, Rmsh::MainDrive *drive) {
	cout << "Iniciou SaveBGMView2" << endl;
	system("rm View2D.pos"); 

	ofstream Myfile("./View2D.pos", ios::app);
	if (!Myfile) {
		Myfile.open("./View2D.pos");
	}
	Myfile << endl;
	// inicio da rotina de gravacao da "View" que serve de background mesh
	Myfile << endl;
	Myfile << "View \"0\" {" << endl;  // o zero desta linha deve estar entre aspas...  <---- Alerta!!
	set<pVertex>::iterator itnodes;
	float GRTEDG = 0;
	float EdgeLength = 0;
	int NEDS = 0;
	
	set<pEdge>BOEDGS;
	set<pEdge>::iterator BEDT;
	set<pVertex>BONODS;
	
	EIter eiter = M_edgeIter(theMesh);
		while (pEdge entedge = EIter_next(eiter)){
			if(E_numFaces(entedge)<=1){
				BOEDGS.insert(entedge);
			}
		}

	EIter_delete(eiter);
	
	for ( BEDT=BOEDGS.begin() ; BEDT != BOEDGS.end(); BEDT++ ){

		pVertex VG1 = E_vertex(*BEDT,0);
		pVertex VG2 = E_vertex(*BEDT,1);
		BONODS.insert(VG1);
		BONODS.insert(VG2);
		
	}
	

	for ( itnodes=BONODS.begin() ; itnodes != BONODS.end(); itnodes++ ){
		double xyz[3];
		V_coord(*itnodes, xyz);
		// Colocoar rotina para calcular para cada ponto o seu GE

		GRTEDG = 0;
		NEDS = V_numEdges(*itnodes);
		for (int h=0; h<NEDS; h++){
			pEdge Edge = V_edge(*itnodes,h);
			pVertex vertex1 = *itnodes;
			pVertex vertex2 = E_vertex(Edge,1); // tem uma funcao FMDB que pega o outro vertex
			
			if (vertex2 == vertex1){
				vertex2 = E_vertex(Edge,0);
			}
					
			double xyz1[3];
			double xyz2[3];

			V_coord(vertex1, xyz1);
			V_coord(vertex2, xyz2);
		
			EdgeLength = sqrt ((xyz1[0]-xyz2[0])*(xyz1[0]-xyz2[0]) + (xyz1[1]-xyz2[1])*(xyz1[1]-xyz2[1]) + (xyz1[2]-xyz2[2])*(xyz1[2]-xyz2[2]) );
			
			if (EdgeLength > GRTEDG){
				GRTEDG = EdgeLength;
			}
		}

		EN_attachDataDbl (*itnodes, MD_lookupMeshDataId("GRTEDG"), GRTEDG);
		if (drive) drive->addPosPoint(xyz[0], xyz[1], xyz[2], GRTEDG);
		Myfile<<"SP("<< xyz[0] <<","<<" "<<xyz[1]<<","<<" "<<xyz[2]<<")"<<"{"<<GRTEDG<<"};"<<endl;
		if (GreatestEdge<GRTEDG){
			GreatestEdge = GRTEDG;
		}
	}

	set<pEdge>::iterator itedges;
	for(itedges=BOEDGS.begin();itedges!=BOEDGS.end();itedges++){ // lendo o set e gravando no arquivo os dados.

		pVertex Point1 = E_vertex(*itedges, 0);
		pVertex Point2 = E_vertex(*itedges, 1);

		double xyz1[3];
		double xyz2[3];

		V_coord(Point1, xyz1);
		V_coord(Point2, xyz2);

		if (drive) drive->addPosLine(xyz1[0], xyz1[1], xyz1[2], xyz2[0], xyz2[1], xyz2[2], 0, 0);
		Myfile << "SL(" << xyz1[0] << ", " << xyz1[1] << ", " << xyz1[2] << ", " << xyz2[0] << ", " << xyz2[1] << ", " << xyz2[2] << "){0,0};" << endl;

	}

	for(int c=0;c<size;c++){
		int g;
		int h;
		double Clx;
		h=V_numEdges(MtxTriangles[c].Pt1);  // Se o ponto tiver mais de duas arestas vizinhas entao nao é do contorno externo da geometria esta linha verifica isso
		g=BoundaryNodes.count(MtxTriangles[c].Pt1); // mas pode ser do contorno interno da geometria e esta verifica isso
		if (g==1 && h>2){
		//	double Correcao = 1; // Este fator Correcao é para refinar mais nos contornos dos dominio e reduzir o efeito de orientacao de malha.
		//	if (V_numFaces(MtxTriangles[c].Pt1)==0) Correcao =0.7;
			EN_getDataDbl(MtxTriangles[c].Pt1,MD_lookupMeshDataId( "GRTEDG" ),&Clx);
			//MtxTriangles[c].Cl1 = Clx*Correcao;
			MtxTriangles[c].Cl1 = Clx;
		}
		h=V_numEdges(MtxTriangles[c].Pt2);
		g=BoundaryNodes.count(MtxTriangles[c].Pt2);
		if (g==1 && h>2){
		//	double Correcao = 1;
		//	if (V_numFaces(MtxTriangles[c].Pt1)==0) Correcao =0.7;
			EN_getDataDbl(MtxTriangles[c].Pt2,MD_lookupMeshDataId( "GRTEDG" ),&Clx);
			MtxTriangles[c].Cl2 = Clx;
		}
		h=V_numEdges(MtxTriangles[c].Pt3);
		g=BoundaryNodes.count(MtxTriangles[c].Pt3);
		if (g==1 && h>2){
		//	double Correcao = 1;
		//	if (V_numFaces(MtxTriangles[c].Pt1)==0) Correcao =0.7; 
			EN_getDataDbl(MtxTriangles[c].Pt3,MD_lookupMeshDataId( "GRTEDG" ),&Clx);
			MtxTriangles[c].Cl3 = Clx;
		}

		if (drive) drive->addPosTriangle(MtxTriangles[c].x1, MtxTriangles[c].y1, MtxTriangles[c].z1,
			MtxTriangles[c].x2, MtxTriangles[c].y2, MtxTriangles[c].z2,
			MtxTriangles[c].x3, MtxTriangles[c].y3, MtxTriangles[c].z3,
			MtxTriangles[c].Cl1, MtxTriangles[c].Cl2, MtxTriangles[c].Cl3);
		Myfile << "ST(" << MtxTriangles[c].x1 << ", " << MtxTriangles[c].y1 << ", " << MtxTriangles[c].z1 << ", " 
		       << MtxTriangles[c].x2 << ", " << MtxTriangles[c].y2 << ", " << MtxTriangles[c].z2 << ", " 
			   << MtxTriangles[c].x3 << ", " << MtxTriangles[c].y3 << ", " << MtxTriangles[c].z3 << "){" 
			   << MtxTriangles[c].Cl1 << ", " << MtxTriangles[c].Cl2 << ", " << MtxTriangles[c].Cl3 << "};" << endl;
	}

	Myfile << "};" << endl;
	Myfile << endl;
	Myfile << "Background Mesh View[0];" << endl;


	Myfile.close(); // fim da gravação da View

	cout << "Concluido SaveBGMView2" << endl;

	delete[] MtxTriangles; MtxTriangles = 0;
}


int Remover::Iterador(set <int> ListofElements, const list<pEntity>& BoundaryFaces, int I, pMesh theMesh){ // Esta funcao percorre toda a malha uma vez e levanta as informacoes necessárias

	int GreaterID=0;
	cout << "Entrou no iterador - ListofElements possui: " << ListofElements.size() << endl;

	// Considerando que seja uma malha 2D
	if (I==2){

		int EdgeDomain1, EdgeDomain2;
		int id = 0;
		
		EIter eiter = M_edgeIter(theMesh);
		while (pEdge entedge = EIter_next(eiter)){
			id++;
			EN_setID(entedge, id);
			// Verificando agora se a aresta é do contorno do dominio

			EN_getDataInt (entedge, MD_lookupMeshDataId("Domain"), &EdgeDomain1);
			EN_getDataInt (entedge, MD_lookupMeshDataId("Domain2"), &EdgeDomain2);
			
			if(E_numFaces(entedge)==1 || (EdgeDomain1!=EdgeDomain2 && EdgeDomain1!=0 && EdgeDomain2 != 0)){

				MeshBoundEdges.insert(entedge);
				pVertex VT1 = E_vertex(entedge , 0);
				if (EN_id(VT1)>GreaterID){
					GreaterID = EN_id(VT1);
				}
				pVertex VT2 = E_vertex(entedge , 1);
				if (EN_id(VT2)>GreaterID){
					GreaterID = EN_id(VT2);
				}
				MeshBoundNodes.insert(VT1);
				MeshBoundNodes.insert(VT2);
			}
			EN_attachDataInt (entedge, MD_lookupMeshDataId("ALRLISTED"), 0);
		}

		EIter_delete(eiter);
		
		
		VIter vit = M_vertexIter(theMesh); // Atribuindo CL = 0 a todos os nodes
		while (pVertex Vertex = VIter_next(vit)){
			EN_attachDataDbl (Vertex, MD_lookupMeshDataId("CL"), 0);
		}
		VIter_delete(vit);
		
	}

	return GreaterID;
}





void Remover::BoundaryElements2D(pMesh theMesh, const std::list<pEntity>& elementList, set<pFace> StgElements){
	cout << "          Iniciou BoundaryElements2D" << endl;

	std::list<pFace>::const_iterator itFaces;
	
	set<pFace>::iterator FCS;
	set<pFace> Faces;


	cout << "StgElements.size() " << StgElements.size() << endl;

	if (StgElements.size() != 0){
		FIter facit = M_faceIter(theMesh);
		while (pFace Fac = FIter_next(facit)){

			Faces.insert(Fac);
			//elementList.push_back(Fac);

		}
		FIter_delete(facit);

		//STOP();

		for ( FCS=Faces.begin() ; FCS != Faces.end(); FCS++ ){
			pFace Face = *FCS;

			pEdge Edge1 = Face->get(1, 0);
			pEdge Edge2 = Face->get(1, 1);
			pEdge Edge3 = Face->get(1, 2);

			BoundaryEdges.insert(Edge1);
			BoundaryEdges.insert(Edge2);
			BoundaryEdges.insert(Edge3);

			pVertex Vertex1 = Face->get(0, 0);
			pVertex Vertex2 = Face->get(0, 1);
			pVertex Vertex3 = Face->get(0, 2);

			BoundaryNodes.insert(Vertex1);
			BoundaryNodes.insert(Vertex2);
			BoundaryNodes.insert(Vertex3);

			if (E_numFaces(Edge1)==1){
				GeomBoundEdges.insert(Edge1);
			}

			if (E_numFaces(Edge2)==1){
				GeomBoundEdges.insert(Edge2);
			}

			if (E_numFaces(Edge3)==1){
				GeomBoundEdges.insert(Edge3);
			}

		}

	}






	for ( itFaces=elementList.begin() ; itFaces != elementList.end(); itFaces++ ){
		pFace Face = *itFaces;
		
		pEdge Edge1 = Face->get(1, 0);
		pEdge Edge2 = Face->get(1, 1);
		pEdge Edge3 = Face->get(1, 2);

		BoundaryEdges.insert(Edge1);
		BoundaryEdges.insert(Edge2);
		BoundaryEdges.insert(Edge3);

		pVertex Vertex1 = Face->get(0, 0);
		pVertex Vertex2 = Face->get(0, 1);
		pVertex Vertex3 = Face->get(0, 2);

		BoundaryNodes.insert(Vertex1);
		BoundaryNodes.insert(Vertex2);
		BoundaryNodes.insert(Vertex3);

		if (E_numFaces(Edge1)==1){
			GeomBoundEdges.insert(Edge1);
		}

		if (E_numFaces(Edge2)==1){
			GeomBoundEdges.insert(Edge2);
		}

		if (E_numFaces(Edge3)==1){
			GeomBoundEdges.insert(Edge3);
		}
		cout << "entrou no for" << endl;
		

	}
	//STOP();
	cout << "Concluiu BoundaryELements2D" << endl;
}

// Nessa funcao as aprtes comentadas serviriam apenas para o caso 3D...
void Remover::removeinternalfaces_2D(pMesh theMesh, std::list<pEntity> &elementList){
	cout << "Iniciou removeinternalfaces" << endl;
	std::list<pFace>::iterator itfaces;
	for ( itfaces=elementList.begin() ; itfaces != elementList.end(); itfaces++ ){
		theMesh->DEL(*itfaces);
	}
}

set<int> Remover::removeinternalfaces(pMesh Vmesh, std::list<pEntity> &elementList){
	cout << "Iniciou removeinternalfaces" << endl;
	std::list<pFace>::iterator itfaces;
	set<int> IDS;
	set<pFace> Faces;
	set<pFace>::iterator itf;

	//Criando a matriz de faces... Face-Nodes

	int MtzE_Faces[M_numEdges(Vmesh)][2];

	FIter fa = M_faceIter(Vmesh);
	while (pFace Fac = FIter_next(fa)){
		pEdge EF1 = F_edge(Fac,0);
		int G1 = EN_id(EF1);		
		pEdge EF2 = F_edge(Fac,0);
		int G2 = EN_id(EF2);	
		int F1 = EN_id(Fac);

		
		if(MtzE_Faces[G1][0] != 0 ){
			MtzE_Faces[G1][1] = F1;
		}
		else{
			MtzE_Faces[G1][0] = F1;
		}

		if(MtzE_Faces[G2][0] != 0 ){
			MtzE_Faces[G2][1] = F1;
		}
		else{
			MtzE_Faces[G2][0] = F1;
		}

	}
	FIter_delete(fa);



/*
	EIter edit = M_edgeIter(Vmesh);
	while (pEdge entity = EIter_next(edit)){
		MtzFaces[EN_id(entity)][0] = 0;
		MtzFaces[EN_id(entity)][1] = 0;
	}
	EIter_delete(edit);



	


	EIter edt = M_edgeIter(Vmesh);
	while (pEdge entity = EIter_next(edt)){
		for(int J=0;J<=1;J++){
			pFace F = E_face(entity,J);
			MtzFaces[EN_id(entity)][J] = EN_id(F);
		}
	}
	EIter_delete(edt);
*/






	////


	for ( itfaces=elementList.begin() ; itfaces != elementList.end(); itfaces++ ){
		//Vmesh->DEL(*itfaces);
		IDS.insert(EN_id(*itfaces));
	}

	FIter facit = M_faceIter(Vmesh);
	while (pFace Fac = FIter_next(facit)){
		if (IDS.count(EN_id(Fac))==1){

			Faces.insert(Fac);
		}
	}
	FIter_delete(facit);

	for ( itf=Faces.begin() ; itf != Faces.end(); itf++ ){
		cout << "Removeu uma face" << endl;
		Vmesh->DEL(*itf);
	}
	

}


int Remover::Ver_AC(pMesh theMesh, pEdge Ed){ 
	int AC = 0;

	if(E_numFaces(Ed)>1){
		pFace F1 = E_face(Ed,0);
		pFace F2 = E_face(Ed,1);
		int B1=0;
		int B2=0;
		EN_getDataInt (F1, MD_lookupMeshDataId("TDI"), &B1);
		EN_getDataInt (F2, MD_lookupMeshDataId("TDI"), &B2);
		AC = B1+B2;
		if (AC==2){
			int CED = 0;
			EN_getDataInt (Ed, MD_lookupMeshDataId("CEDGE"), &CED);
			if (CED==1){
				cout << "CEDGE = 1" << endl;
				AC=1;
			}
		}
							
	}

	if( E_numFaces(Ed)==1){
	//	pFace F1 = E_face(Ed,0);
	//	int B1=0;
	//	EN_getDataInt (F1, MD_lookupMeshDataId("TDI"), &B1);
	//	AC = B1;
		AC = 1;
	}
	return AC;
}


set<pFace> Remover::Identify_StgElements_II(pMesh theMesh, std::list<pEntity> &elementList, set<int> setdomains){  // Deve retornar os StgNodes

  
	cout << "Entrou em Identify_StgElements_II" << endl;
	set<pFace> StgFaces;
	
	set<pEdge>::iterator itEdges;
	std::list<pFace>::iterator itfaces;

	for ( itfaces=elementList.begin() ; itfaces != elementList.end(); itfaces++ ){
		EN_attachDataInt (*itfaces, MD_lookupMeshDataId("TDI"), 1);
	}

	int Recursive = 1;

	while (Recursive){
		//Recursive = 0;
		int siz1 = StgFaces.size();

		int J = 0;


		// Rotina para contar as arestas de cada dominio

		map <int,int> Map_Domains;
		set<int>::iterator gj;
		map<int,int>::iterator MD;

		for (gj=setdomains.begin();gj!=setdomains.end();gj++){  //Criando o mapa de dominios
			Map_Domains.insert( std::pair<int,int>(*gj, 0) );
		}
			Map_Domains.insert( std::pair<int,int>(0, 0) );
		//////////


		VIter vit = M_vertexIter(theMesh);
		while (pVertex entity = VIter_next(vit)){

			int NEV = V_numEdges(entity);
			for (int l=0; l<NEV; l++){

				int V=0;
				pEdge Ed = V_edge(entity,l);   // Tem que ver se sao arestas de contorno...

				//int AC = 0;
				int AC = Ver_AC(theMesh, Ed); // AC = Aresta de Contorno
				


				if (AC == 1 ){
					//cout << "Edge " << EN_id(Ed) << endl;
					int A = 0;
					EN_getDataInt (Ed, MD_lookupMeshDataId("Domain"), &A);
					//cout <<endl<< "Domain " << A << endl;
					if(A!=0){
						Map_Domains.find(A)->second++;
					}
					int B = 0;
					EN_getDataInt (Ed, MD_lookupMeshDataId("Domain2"), &B);
					//cout << "Domain2 " << A << endl << endl;
					if(B != 0 && B != A){
						Map_Domains.find(B)->second++;
					}
				
				}
				
			}
			

			for (MD=Map_Domains.begin();MD!=Map_Domains.end();MD++){
				if ((*MD).first!=0){
					int K=1;
					if ((*MD).second > 2){  // Se o ponto possui mais de duas arestas do mesmo dominio 
								//-- que tenham menos de 2 faces...

						int VF = V_numFaces(entity);

//////////////
						int VE = V_numEdges(entity);
						
						int Maisde2E = 0;
						for (int y =0;y<VE;y++){

							pEdge FC1 = entity->get(1,y);
							int R = E_numFaces(FC1);
				//			if (R>1){
				//				Maisde2E++;
				//			}
							int F = 0;
							for (int fj = 0;fj<R;fj++){
								pFace DG = E_face(FC1,fj);
								int DT = 0;
								EN_getDataInt (DG, MD_lookupMeshDataId("TDI"), &DT);
								if (DT == 1){
									F++;
								}
							
							}
							if (F){
								Maisde2E++;
							}
						}
						
						
						int TY = 0;
						for (int y = 0;y<VF;y++){   // Verificando se o no fica em uma ponta de face
							pFace FC1 = entity->get(2,y);
							int DT = 0;
							EN_getDataInt (FC1, MD_lookupMeshDataId("TDI"), &DT);
							if (DT == 1){
								TY++;
							}
						}
						int XS=0;
						if ((VF-TY)==1 || VF==1 ){
							XS=1;
						}
								
						if (Maisde2E>2 || XS==1){
///////////

							for (int y =0;y<VF;y++){

								pFace FC1 = entity->get(2,y);
								K=StgFaces.count(FC1);
								StgFaces.insert(FC1);
							
								if (K!=1){
									y=VF;
								}
							
								EN_attachDataInt (FC1, MD_lookupMeshDataId("TDI"), 1);
								if(!K){
									J=1;
								}
							}
						}
					}
				}
			}

			for (MD=Map_Domains.begin();MD!=Map_Domains.end();MD++){ //zerando o map para recomeçar a contagem para o próximo ponto
				(*MD).second = 0;
			}

//////////////////////
			int VF = V_numFaces(entity);
			int TY = 0;
			for (int y = 0;y<VF;y++){   // Verificando se o no fica em uma ponta de face
				pFace FC1 = entity->get(2,y);
				int DT = 0;
				EN_getDataInt (FC1, MD_lookupMeshDataId("TDI"), &DT);
				if (DT == 1){
					TY++;
				}
			}
			int XS=0;
			if ((VF-TY)==1 || VF==1 ){
				XS=1;
			}
					
			if (XS==1){

				for (int y =0;y<VF;y++){
					pFace FC1 = entity->get(2,y);
					int RF=StgFaces.count(FC1);
					StgFaces.insert(FC1);
				
					if (RF!=1){
						y=VF;
					}
							
					EN_attachDataInt (FC1, MD_lookupMeshDataId("TDI"), 1);
					if(!RF){
						J=1;
					}
				}
			}
	
/////////////////////////

		}
		

		VIter_delete(vit);





		FIter facit = M_faceIter(theMesh);
		while (pFace Fac = FIter_next(facit)){

			int TDI = 0;
			EN_getDataInt (Fac, MD_lookupMeshDataId("TDI"), &TDI);
			if (TDI == 0){
				set<pEdge> NeighEdges;
				for(int d=0;d<3;d++){
	
					pEdge E = F_edge(Fac,d);
					NeighEdges.insert(E);
	
				}
				int NFaces=0;
				int H = 0;
				for ( itEdges=NeighEdges.begin() ; itEdges != NeighEdges.end(); itEdges++ ){
				
					for (int F=0;F<E_numFaces(*itEdges);F++){
						pFace F1 = E_face(*itEdges,F);
						int A = 0;
						EN_getDataInt (F1, MD_lookupMeshDataId("TDI"), &A);
						if (A!=0){
							H++;
						}
					}
				}
	
				NFaces = H;
				int K = 1;
				if (NFaces>=2){
					K=StgFaces.count(Fac);
					StgFaces.insert(Fac);
					EN_attachDataInt (Fac, MD_lookupMeshDataId("TDI"), 1);
				}
				if(!K){
					J=1;
				}
				
			}
		}
		FIter_delete(facit);








		int siz2 = StgFaces.size();
		//if (siz1 != siz2) {
		if (J) {

			Recursive=1;

		}
		//if (siz1 == siz2) {
		if (!J) {

			Recursive=0;

		}


		//Recursive++;
	}
	//STOP();
	cout << "StgFaces.size() " << StgFaces.size() << endl;
	return StgFaces;
}

/*
set<int> Remover::Identify_StgElements(pMesh Vmesh){  // Deve retornar os StgNodes

	cout << "Iniciou Identify_StgElements" << endl << "Vmesh tem: " << M_numFaces(Vmesh) << endl;
	int R=1;
	set<int> FacesID; 
	//set<pFace> Faces;
	set<pVertex> Vertices;
	set<pVertex>::iterator VTI;

	Faces.clear();








	while (R){
		cout << endl << " R --- " << R << endl << endl;
		VIter vit = M_vertexIter(Vmesh);
		while (pVertex entity = VIter_next(vit)){
			set<pFace> V_Faces;
			int F = V_numFaces(entity);
			int E = V_numEdges(entity);


			if (E != F){
				Vertices.insert(entity);
			}


		
		}

		VIter_delete(vit);


		cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;

		//set<pFace>::iterator itfaces;
		for ( VTI=Vertices.begin() ; VTI != Vertices.end(); VTI++ ){
			int Fc = V_numEdges(*VTI);
			for ( int d = 0; d < Fc; d++ ){
				pVertex VT = *VTI;
				//pFace FG = VT->get(2,d);
				pEdge ER = V_edge(VT,d);
				int EF = E_numFaces(ER);
				for (int s =0;s<EF;s++){
					pFace FG = E_face(ER,s);
					int E_ID = EN_id(ER);
					FacesID.insert(MtzE_Faces[E_ID][0]);
					FacesID.insert(MtzE_Faces[E_ID][1]);
					//Vmesh->DEL(FG);
				}
			}
		}

		Refresh(Vmesh);
	
		R++;
			
		if (Vertices.size()!=0){
			R=0;
			//STOP();
		}
	
		Vertices.clear();
		//Faces.clear();

	}

	cout << "FacesID.size() " << FacesID.size() << endl;

	//STOP();

	return FacesID;
}
*/

/*
set<pFace> Remover::Conta_Arestas(pMesh theMesh, pVertex Vertex){
	//cout << "Iniciou Conta_Arestas" << endl;
	set<int> Domains;
	set<pFace> Faces;
	int NE = V_numEdges(Vertex);
	for(int G=0;G<NE;G++){   
		int R1 = 0;
		int R2 = 0;
		pEdge E = V_edge(Vertex,G);
		EN_getDataInt (E, MD_lookupMeshDataId("Domain"), &R1);
		EN_getDataInt (E, MD_lookupMeshDataId("Domain2"), &R2);
		Domains.insert(R1);
		Domains.insert(R2);
	}
	set<int>::iterator T;



	int VF = V_numFaces(Vertex);
	int m = 0;

	for (int y=0;y<VF;y++){
		int h = 0;
		pFace GH = Vertex->get(2,y);
		EN_getDataInt (GH, MD_lookupMeshDataId("Remover"), &h);
		if (h==1){
			m++;
		}
	}



	int Contador = 0;
	for ( T=Domains.begin() ; T != Domains.end(); T++ ){
		int CInterno = 0;
		for(int G=0;G<NE;G++){   
			int R1 = 0;
			int R2 = 0;
			pEdge E = V_edge(Vertex,G);



			int EF = E_numFaces(E);
			int C=0;
			int CT = 0;
			for (int u =0;u<EF;u++){ // Verificando se ao menos uma das faces é removida
				pFace F = E_face(E,u);
				
				EN_getDataInt (F, MD_lookupMeshDataId("Remover"), &CT);
				if (CT==1){
					C++;
				}
			}
			

			if (C>=1 || EF<=1){

				EN_getDataInt (E, MD_lookupMeshDataId("Domain"), &R1);
				EN_getDataInt (E, MD_lookupMeshDataId("Domain2"), &R2);
	

				if (R1 == *T){
					CInterno++;
				}

				if (R2 != R1 && R2 == *T){
					CInterno++;
				}

				
			}

		}
		if (Contador < CInterno){
			Contador = CInterno;
		}
	}

	//if (Contador > 2 || (VF-m)==1){
	if (Contador > 2){
		int NF = V_numFaces(Vertex);
		for(int G=0;G<NF;G++){
			pFace Fc = Vertex->get(2,G);
			Faces.insert(Fc);
		}
	}


	
	set<pFace>::iterator F;
	//for (F=Faces.begin(); F!=Faces.end(); F++){

		//EN_attachDataInt (*F, MD_lookupMeshDataId("Remover"), 1);

	//}
	//cout << "Concluiu Conta_Arestas" << endl;
	return Faces;
}
*/


set<pFace> Remover::Conta_Arestas(pMesh theMesh, pVertex Vertex){
	//cout << "Iniciou Conta_Arestas" << endl;
	set<pEdge> ArestasqContam;

	set<int> Domains;
	set<pFace> Faces;
	int NE = V_numEdges(Vertex);
	for(int G=0;G<NE;G++){     // Verificando os dominios do ponto e as arestas que serão avaliadas
		int R1 = 0;
		int R2 = 0;
		pEdge E = V_edge(Vertex,G);
		int EF = E_numFaces(E);
		EN_getDataInt (E, MD_lookupMeshDataId("Domain"), &R1);
		EN_getDataInt (E, MD_lookupMeshDataId("Domain2"), &R2);
		Domains.insert(R1);
		Domains.insert(R2);
		for (int u =0;u<EF;u++){ // Verificando se ao menos uma das faces é removida
			pFace F = E_face(E,u);
			int CT=0;
			EN_getDataInt (F, MD_lookupMeshDataId("Remover"), &CT);
			if (CT==1){
				ArestasqContam.insert(E);
			}
		}
	}
	set<int>::iterator T;
	set<pEdge>::iterator Edg_IT;

	int Contador = 0;
	int RESP = 0;
	for (T=Domains.begin() ; T != Domains.end(); T++ ){
		int CU=0;
		for (Edg_IT = ArestasqContam.begin();Edg_IT!= ArestasqContam.end() ; Edg_IT++){

			int R1 = 0;
			int R2 = 0;
			EN_getDataInt (*Edg_IT, MD_lookupMeshDataId("Domain"), &R1);
			EN_getDataInt (*Edg_IT, MD_lookupMeshDataId("Domain2"), &R2);
			int D = *T;
			if(R1 == D || R2 == D){

				CU++;

			}
		}
		if (CU>2){

			RESP = 1;
			//cout << "RESP = 1" << endl;
			
		}
	}
	if (RESP==1){
		int NF = V_numFaces(Vertex);
		for(int G=0;G<NF;G++){
			pFace Fc = Vertex->get(2,G);
			Faces.insert(Fc);

		}
	}

	// Iniciando o critério de uma face só

	int NF = V_numFaces(Vertex);
	int H = 0;
	for(int G=0;G<NF;G++){
		pFace Fc = Vertex->get(2,G);
		int M = 0;
		
		EN_getDataInt (Fc, MD_lookupMeshDataId("Remover"), &M);
		if (M==0){

			H++;

		}

	}
	if (H==1){
		for(int G=0;G<NF;G++){
			pFace Fc = Vertex->get(2,G);
			Faces.insert(Fc);
		}

	}
	

	return Faces;
}






void Remover::removestgNodes_II(pMesh theMesh, std::list<pEntity> &elementList){

	cout << "Iniciou removestgNodes_II" << endl;
	set<pFace>::iterator FF;
	FIter facit = M_faceIter(theMesh);
	while (pFace Fac = FIter_next(facit)){
		EN_attachDataInt (Fac, MD_lookupMeshDataId("Remover"), 0);
	}
	FIter_delete(facit);

	set<pVertex> ListNodes;
	std::list<pFace>::iterator itfaces;
	for ( itfaces=elementList.begin() ; itfaces != elementList.end(); itfaces++ ){
		EN_attachDataInt (*itfaces, MD_lookupMeshDataId("Remover"), 1);
		for(int k=0;k<3;k++){
			ListNodes.insert(F_vertex(*itfaces,k));
		}
	}

	set<pVertex> StgNodes;
	set<pFace>::iterator FCG;
	int i = 0;
	set<pEdge> EDGES;
	set<pFace> FACES;
	set<pFace> GOTOLIST;
	int RT=1;
	int GTLS = 0;
	while (RT){
		GTLS = GOTOLIST.size();

		VIter vit = M_vertexIter(theMesh);
		while (pVertex entity = VIter_next(vit)){
			//Se o vertex pertence a par de planes é um stgvertex
			set<pFace> T = Conta_Arestas(theMesh, entity);
			if (T.size()!=0){
				//cout << "T.size() " << T.size() << endl;
				//STOP();
				int G = V_numFaces(entity);
				for (int h = 0; h<G; h++){
					pFace Y = entity->get(2,h);
					GOTOLIST.insert(Y);
					
				}
			}
		}

		VIter_delete(vit);

		int newGTLS = GOTOLIST.size();

		if (GTLS == newGTLS){
			RT = 0;
		}
	}
	
	for(FF=GOTOLIST.begin(); FF!=GOTOLIST.end(); FF++){
		elementList.push_back(*FF);
		EN_attachDataInt (*FF, MD_lookupMeshDataId("Remover"), 1);
	}

	GOTOLIST.clear();
}


void Remover::removestgNodes(pMesh theMesh){

	cout << "Iniciou RemoveStgNodes" << endl;

	theMesh->modifyState(2,1,0);
	theMesh->modifyState(2,1,1);

	theMesh->modifyState(1,2,0);
	theMesh->modifyState(1,2,1);

	int w=1;
	int conter = 0;

	list<pVertex> ControlNodes;
	set<pVertex> NewNodes;

	set<pFace>::iterator itfcs;
	set<pVertex>::iterator itnds;
	list<pVertex>::iterator litnds;

	for ( itnds=BoundaryNodes.begin() ; itnds != BoundaryNodes.end(); itnds++ ){
		ControlNodes.push_back(*itnds);
	}

	while (ControlNodes.size()>0){

		set<pFace> RemFACES;
		set<pEdge> VEDGES;
		conter++;

		for ( litnds=ControlNodes.begin() ; litnds != ControlNodes.end(); litnds++ ){
			pVertex Node = *litnds;

			if (MeshBoundNodes.count(Node)==0){ // Alterar isso para nao usar o set, usar attachdataint

				int numEdges = V_numEdges(Node);
				int numFaces = V_numFaces(Node); // O Modifystate entre nodes e faces está com problema...


				for (int i=0; i<numEdges; i++){
					pEdge VEDG = V_edge(Node,i);
					if(E_numFaces(VEDG)!=0){
						VEDGES.insert(VEDG);
					}

				}


				set<pEdge>::iterator itedgs;

				set<pFace> EFACES;

				for ( itedgs=VEDGES.begin() ; itedgs != VEDGES.end(); itedgs++ ){
					if (E_numFaces(*itedgs)>0){
						EFACES.insert(E_face(*itedgs,0));
						if(E_numFaces(*itedgs)==2){
							pEdge DG = E_face(*itedgs,1);
							EFACES.insert(DG);

						}
					}
				}

				// Porque o modifystate entre nos e faces esta com problema eu tenho que comparar o tamanho dos sets
				if ((VEDGES.size() > EFACES.size()+1)||((VEDGES.size() > EFACES.size())&& EFACES.size()==1) ){
					for(itfcs=EFACES.begin() ; itfcs != EFACES.end(); itfcs++ ){
						for(int y=0; y<=2; y++){

							pVertex V = F_vertex(*itfcs, y);
							BoundaryNodes.insert(V); // Inserir em outro set pra depois inserir nesse
							NewNodes.insert(V);

						}

						for(int y=0; y<=2; y++){

							pEdge E = F_edge(*itfcs, y);
							BoundaryEdges.insert(E);

						}

						RemFACES.insert(*itfcs);

					}
				}

				VEDGES.clear();
				EFACES.clear();

			}
			else{

				int numEdges = V_numEdges(Node);
				int numFaces = V_numFaces(Node);

				int Ctr=0;

				for (int i=0; i<numEdges; i++){
					pEdge VEDG = V_edge(Node,i);
					if(E_numFaces(VEDG)!=0){
						VEDGES.insert(VEDG);
					}
					if (MeshBoundEdges.count(VEDG)==1){
						Ctr = Ctr + E_numFaces(VEDG);
					}
				}
				if (Ctr==0){
					set<pEdge>::iterator itedgs;
					set<pFace>::iterator itfaces;
					set<pFace> EFACES;

					for ( itedgs=VEDGES.begin() ; itedgs != VEDGES.end(); itedgs++ ){
						if (E_numFaces(*itedgs)>0){
							EFACES.insert(E_face(*itedgs,0));
							if(E_numFaces(*itedgs)==2){
								pEdge DG = E_face(*itedgs,1);
								EFACES.insert(DG);
							}
						}
					}
					if(EFACES.size()>0){
						for(itfaces=EFACES.begin(); itfaces!=EFACES.end(); itfaces++){

							for(int y=0; y<=2; y++){

								pVertex V = F_vertex(*itfaces, y);
								BoundaryNodes.insert(V); // Inserir em outro set pra depois inserir nesse		
								NewNodes.insert(V);
							}

							for(int y=0; y<=2; y++){

								pEdge E = F_edge(*itfaces, y);
								BoundaryEdges.insert(E);

							}

							RemFACES.insert(*itfaces);

						}
					}
					EFACES.clear();
				}
				VEDGES.clear();
			}
		}

		ControlNodes.clear();

		for ( itnds=NewNodes.begin() ; itnds != NewNodes.end(); itnds++ ){
			ControlNodes.push_back(*itnds);
		}
		NewNodes.clear();


		for (itfcs=RemFACES.begin() ; itfcs != RemFACES.end(); itfcs++ ){
			theMesh->DEL(*itfcs);
		}
		RemFACES.clear();

		theMesh->modifyState(2,1,0);
		theMesh->modifyState(2,1,1);

		theMesh->modifyState(1,2,0);
		theMesh->modifyState(1,2,1);

	}

}

void Remover::removeinternaledges(pMesh theMesh){
	cout << "          Iniciou removeinternaledges" << endl;

	set<pEdge>::iterator itEdges;

	EIter edit = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(edit)){

		if (E_numFaces(entity)==0 && MeshBoundEdges.find(entity)==MeshBoundEdges.end()){	// NNAAOO PRECISA SER SET
	//	int D = 0;
	//	EN_getDataInt (entity, MD_lookupMeshDataId("CEDGE"), &D);
	//	if (E_numFaces(entity)==0 && D==0){

			RemovefromBoundEdges.insert(entity);

		}
	}
	EIter_delete(edit);


	for ( itEdges=RemovefromBoundEdges.begin() ; itEdges != RemovefromBoundEdges.end(); itEdges++ ){
		//if (BoundaryEdges.count(*itEdges)==1){
			BoundaryEdges.erase(*itEdges);
		//}
		theMesh->DEL(*itEdges);
	}

	RemovefromBoundEdges.clear();

cout << "Finalizou removeinternaledges" << endl;
}

void Remover::removeinternalnodes(pMesh theMesh){
	cout << "Iniciou removeinternalnodes" << endl;

	set <pVertex> ToRemoveVertex;
	VIter vit = M_vertexIter(theMesh);

	while (pVertex entity = VIter_next(vit)){
		if (V_numEdges(entity)==0){
			ToRemoveVertex.insert(entity);
		}
	}

	VIter_delete(vit);

	set<pVertex>::iterator itVts;
	for ( itVts=ToRemoveVertex.begin() ; itVts != ToRemoveVertex.end(); itVts++ ){
		theMesh->DEL(*itVts);
	}

}

set <pEdge> Remover::ReturnBoundaryEdges (){
	return BoundaryEdges;
}

set <pVertex> Remover::ReturnBoundaryNodes (){
	return BoundaryNodes;
}


void Remover::removeexternaledges(pMesh theMesh){ // Essa funcao agora nao funciona por que o set GeomBoundEdges nao é mais preenchido

	cout << "Iniciou removeexternaledges" << endl;

	set<pEdge>::iterator itEdges;

	set<pEdge> EREMOVE;
	EIter eit = M_edgeIter(theMesh);
	while (pEdge entity = EIter_next(eit)){
		if (E_numFaces(entity)==0){
			EREMOVE.insert(entity);
		}
	}
	EIter_delete(eit);


	for ( itEdges=EREMOVE.begin() ; itEdges != EREMOVE.end(); itEdges++ ){
		theMesh->DEL(*itEdges);
	}
	EREMOVE.clear();

}




void Remover::removeexternalnodes(pMesh theMesh){

	cout << "Iniciou removeexternalnodes" << endl;

//	cout << __LINE__ << "  " << __FILE__ << endl; 
	theMesh->modifyState(3,2,1);
	theMesh->modifyState(3,1,1);
	theMesh->modifyState(3,0);
//	cout << __LINE__ << "  " << __FILE__ << endl;
	theMesh->modifyState(2,1);
	theMesh->modifyState(2,3);
	theMesh->modifyState(2,0);
//	cout << __LINE__ << "  " << __FILE__ << endl;
	theMesh->modifyState(1,3);
	theMesh->modifyState(1,2);
	theMesh->modifyState(1,0);
//	cout << __LINE__ << "  " << __FILE__ << endl;
	theMesh->modifyState(0,2);
	theMesh->modifyState(0,1);
	theMesh->modifyState(0,3);
//	cout << __LINE__ << "  " << __FILE__ << endl;
	// Aqui é pra garantir que os pontos foram todos removidos
	set <pVertex> ToRemoveVertex;
	VIter vit = M_vertexIter(theMesh);
	while (pVertex entity = VIter_next(vit)){
		if (V_numEdges(entity)==0){
			ToRemoveVertex.insert(entity);
		}
	}
	VIter_delete(vit);
//	cout << __LINE__ << "  " << __FILE__ << endl;
	set<pVertex>::iterator itVts;
	for ( itVts=ToRemoveVertex.begin() ; itVts != ToRemoveVertex.end(); itVts++ ){
		theMesh->DEL(*itVts);
	}
//	cout << __LINE__ << "  " << __FILE__ << endl;
	cout << "terminou removeexternalnodes" << endl;
}



void Remover::removetetras(pMesh theMesh){

	cout << "Iniciou removetetras" << endl;

	set<pRegion>::iterator itTetras;
	for ( itTetras=Tetras.begin() ; itTetras != Tetras.end(); itTetras++ ){

		theMesh->DEL(*itTetras);

	}
}
/*
void Remover::Remove_Lines_1(pMesh theMesh, set<int> setdomains){ 


	SurfacesDomains.clear();

	map<int, list< set<pEdge> > >::iterator itDPS;
	list< set<pEdge> >:: iterator lit;
	set<pEdge>:: iterator eit;

	set<pFace> Faces;
	set<pFace>::iterator FS;
	set<pEdge> Edges;
	set<pEdge> Edges2;
	set<pEdge> Edges3;

	set<pVertex> NodesJaListados;
	for (std = setdomains.begin(); std != setdomains.end(); std++){
		
		int DOMN = *std;
		itDPS = DomainsPS.find(DOMN); // Encontrando a lista do dominio

		for (lit=(itDPS->second).begin(); lit!=(itDPS->second).end() ;lit++){ // iterando dentro da lista do dominio de set em set; lit é iterador para um set.


			for (eit=(lit)->begin(); eit!=(lit)->end() ;eit++){

				pEdge D = *eit;			
				pVertex V1,V2;
				V1 = E_vertex(D, 0);
				V2 = E_vertex(D, 1);
				
				///
				
				int VF1 = V_numFaces(V1);
				int VF2 = V_numFaces(V2);
				int EF1 = V_numEdges(V1);
				int EF2 = V_numEdges(V2);

				for (int h=0;h<EF1;h++){
				
					pEdge E = V1->get(1,h);
					if (E_numFaces(E)==1){
						Edges.insert(E);
					}
					
				}

				if (Edges.size()>2){
				
					for (int h=0;h<EF1;h++){
				
						pEdge E = V1->get(1,h);
						Edges2.insert(E);
										
					}

					for (int h=0;h<VF1;h++){
				
						pFace F = V1->get(2,h);
						Faces.insert(F);

					}
				}
				
				Edges.clear();
				
				///
				
				for (int h=0;h<EF2;h++){
				
					pEdge E = V2->get(1,h);
					if (E_numFaces(E)==1){
						Edges.insert(E);
					}
					
				}

				if (Edges.size()>2){
				
					for (int h=0;h<EF1;h++){
				
						pEdge E = V2->get(1,h);
						Edges2.insert(E);
										
					}

					for (int h=0;h<VF2;h++){
				
						pFace F = V2->get(2,h);
						Faces.insert(F);

					}
				}
				
				///

			}
			for (

		}
		
	}
	
}

*/



void Remover::RemoveStgElements_2D(pMesh theMesh, float GE) { //detecta elementos estranhos numa malha 2D, os remove e atualiza set's de contorno

	set<pVertex>::iterator itVertex;
	set<pEdge>::iterator itedge;
	set<pEdge>::iterator itedge2;
	set<pFace>::iterator itface;

	set <pVertex> stgNodes;
	set <pEdge> stgEdges;
	set <pFace> stgFaces;

	stgNodes.clear();
	stgEdges.clear();
	stgFaces.clear();

	cout << "iniciou RemoveStgElements_2D" << endl;



	int h=1;
	while (h==1){
		h=0;  // Novo criterio: Todas as faces tem que ser vizinhas de ao menos 2 outras faces, se a face for vizinha de apenas 1 face, ou nenhuma outra face, então ela é uma face estranha e deve ser removida.
		for ( itVertex=BoundaryNodes.begin() ; itVertex != BoundaryNodes.end(); itVertex++ ){


			if(V_numFaces(*itVertex)>0){


				int NEVertex = V_numEdges(*itVertex);
				set<pEdge> NeighEdges;
				set<pFace> NeighFaces;

				for (int y = 0; y<NEVertex; y++){ // inserindo todas as arestas do no no set

					// para cada nó, verificar a qtd de faces e para cada face, verificar a quantidade de faces vizinhas dela...
					// se a qtd de faces vizinhas daquela face for <= 1, trata-se de face estranha.
					pEdge NeighEdge = V_edge(*itVertex,y);
					NeighEdges.insert(NeighEdge);
				}


				for(itedge=NeighEdges.begin();itedge!=NeighEdges.end();itedge++){ // inserindo todas as faces do no no set
					int Nfaces=E_numFaces(*itedge);
					if(Nfaces>0){
						pFace Nface = E_face(*itedge,0);
						NeighFaces.insert(Nface);
						if (Nfaces>1){
							NeighFaces.insert(E_face(*itedge,1));
						}
					}
				}


				NeighEdges.clear();

				//	cout<<"Terminou de inserir todas as faces vizinhas dos Nodes, agora analisado cada uma:"<<endl;

				int psr = 0;  // <--- psr é plane surface reader
				int stgpsr =0; // <--- plane surface reader dos elementos estranhos, guarda a informacao de qual ps estamos trabalha	ndo no momento

				for(itface=NeighFaces.begin();itface!=NeighFaces.end();itface++){ // analisando a qtd de vizinhos da face
					int FV=0;
					//		cout<< "Analisando face..." << endl;
					for (int i=0;i<=2;i++){
						pEdge Edg = F_edge(*itface,i);
						if (E_numFaces(Edg)==2){
							FV++;
						}
					}


					if (FV<2){ //todo este laco se aproveita... mas o criterio do if muda...
						h=1;

						if (V_numEdges(*itVertex)<=2){
							if(MeshBoundNodes.count(*itVertex)!=1){
								stgNodes.insert(*itVertex);
							}
						}



						stgFaces.insert(*itface);	


						//theMesh->DEL(stgFace); // removeu a face da malha, nao existe o set de faces para 2D


					}

				}

				NeighFaces.clear();
			}
		}		


		//cout << "todos os vertex analisados, procedendo remocao de elementos estranhos" << endl;

		// Agora, neste momento comecam a ser removidos os elementos da malha

		for (itface=stgFaces.begin() ;  itface != stgFaces.end(); itface++ ){
			for(int y=0; y<=2; y++){
				pVertex V = F_vertex(*itface, y);
				BoundaryNodes.insert(V);
			}
			for(int y=0; y<=2; y++){
				pEdge E = F_edge(*itface, y);
				BoundaryEdges.insert(E);
			}

			theMesh->DEL(*itface);

		}

		theMesh->modifyState(2,1,0);
		theMesh->modifyState(2,1,1);
		theMesh->modifyState(1,2,0);
		theMesh->modifyState(1,2,1);
		theMesh->modifyState(0,1,0);
		theMesh->modifyState(0,1,1);
		theMesh->modifyState(0,2,0);
		theMesh->modifyState(0,2,1);

		stgNodes.clear();
		stgEdges.clear();
		stgFaces.clear();

//		if (h==1){
			//	cout << "Faces estranhas foram removidas, iniciando nova varredura" << endl;
			//	cout << endl;
//		}
//		if (h==0){
			//	cout << "Nao foram encontradas faces estranhas nesta varredura, finalizando limpeza" << endl;
//		}
	}
	cout << "          finalizou RemoveStgElements_2D" << endl;
	cout << endl;


}

void Remover::Get_Angular_Coef(set<pEdge> CommonEdges) { 
	set<pEdge>::iterator itped;

	for(itped=CommonEdges.begin(); itped!=CommonEdges.end(); itped++){
		double m; // Coeficiente angular da aresta

		pVertex Node1 = E_vertex(*itped,0);
		pVertex Node2 = E_vertex(*itped,1);

		double xyz1[3];
		double xyz2[3];
		V_coord(Node1, xyz1);
		V_coord(Node2, xyz2);

		double Denominador = xyz2[0]-xyz1[0];

		if (Denominador!=0){
			m = (xyz2[1]-xyz1[1])/(Denominador);
		}
		else{
			m = 1000;
		}


		EN_attachDataDbl (*itped,MD_lookupMeshDataId("Angular_Coef"), m);
	}
}


double Remover::Get_Angular_Coef_II(pEdge Edge) { 

	double m; // Coeficiente angular da aresta

	pVertex Node1 = E_vertex(Edge,0);
	pVertex Node2 = E_vertex(Edge,1);

	double xyz1[3];
	double xyz2[3];

	V_coord(Node1, xyz1);
	V_coord(Node2, xyz2);

	double Denominador = xyz2[0]-xyz1[0];

	if (Denominador!=0){
		m = (xyz2[1]-xyz1[1])/(Denominador);
	}

	else{
		m = 1000;
	}
		
	return m;
}




void Remover::Save_LineLoop2D(pMesh theMesh, set<pEdge> EDGES){ 



	set<pEdge>:: iterator eit;
	

	int NewEdgeID = 0;

	int MDOM = 0;

	set<int>::iterator std;

	set<int> PhysicalSurf[MDOM+1];

	system("rm LineLoop.geo");
	
	ofstream Myfile("./LineLoop.geo", ios::app);
	if (!Myfile) {
		Myfile.open("./LineLoop.geo");
	}


	// ROTINA QUE CRIA A LISTA DE PONTOS INICIAL DO ARQUIVO - IDS E COORDENADAS DE TODOS OS PONTOS
	

	int i = 0;
	set<pEdge>::iterator ETI;


	set<pVertex>::iterator Vc;
	set<pVertex> Nodes;
	for (ETI = EDGES.begin(); ETI != EDGES.end() ; ETI++){ 
		
		pVertex V1,V2;
		V1 = E_vertex(*ETI, 0);
		V2 = E_vertex(*ETI, 1);
	
		Nodes.insert(V1);
		Nodes.insert(V2);


	}


	for (Vc = Nodes.begin(); Vc != Nodes.end() ; Vc++){ 
		double xyz[3];
		V_coord(*Vc, xyz);
		Myfile<<"Point("<<EN_id(*Vc)<<")"<<" "<<"="<<" "<<"{"<<xyz[0]<<","<<" "<<xyz[1]<<","<<" "<<xyz[2]<<","<<" 				"<< 0.5 <<"};"<<endl;
	}






	for (ETI = EDGES.begin(); ETI != EDGES.end() ; ETI++){ 
		
		pVertex V1,V2;
		V1 = E_vertex(*ETI, 0);
		V2 = E_vertex(*ETI, 1);
	
		Myfile<<"Line("<<EN_id(*ETI)<<")"<<" = " <<"{" << EN_id(V1) << ", " << EN_id(V2) << "};"<<endl;
	}




	// FIM DA ROTINA, PARTE INICIAL DO ARQUIVO GRAVADO


	//		//		//		//		//		//		//		//		//


	// ROTINA QUE SALVA NO ARQUIVO TODAS AS LINES E IDS DOS PONTOS QUE AS COMPOEM AINDA DENTRO DA FUNCAO SaveFileGeometry_2D
	


	Myfile.close(); // fim da gravação da geometria


	//delete F; 
	//F = 0;
	//delete a; a = 0;
}











set<pEdge> Remover::Merge_PS_Edges(set <pEdge> PSEDGES1, pMesh theMesh, int Neid, set<pEdge> &M_Edges) {

//////// Inicio da rotina de merge edges /////////
	set<pEdge>:: iterator TR;
	cout << endl <<  "Iniciou Merge_PS_Edges - Arestas recebidas: " << PSEDGES1.size() << endl;
	cout <<"Conferindo as arestas que Merge_PS_Edges está recebendo"<< endl;

	for(TR=PSEDGES1.begin();TR!=PSEDGES1.end();TR++){

		int MM = 0;
		EN_getDataInt (*TR, MD_lookupMeshDataId("geoflag"), &MM);
		pVertex V1 = E_vertex(*TR,0);
		pVertex V2 = E_vertex(*TR,1);
		//cout << "Aresta " << EN_id(*TR) << " Nós " << EN_id(V1) << " " << EN_id(V2) << "    Geoflag  " << MM << endl;


	}
	cout << endl;


	int NEID = Neid;


	int NewEdgeID = 2*M_numEdges(theMesh);
	
	set<pEdge> theMeshDel;
	
	set<pEdge> SameCoefEdges;
	set<pEdge> SameEdge;
	set<pEdge> PSEDGES;
	set<pEdge>::iterator itEds;
	set<pEdge> PSEDGES_Orig;

	for(itEds = PSEDGES1.begin(); itEds != PSEDGES1.end(); itEds++){ 
		PSEDGES_Orig.insert(*itEds);
	}

	for(itEds=PSEDGES_Orig.begin(); itEds!=PSEDGES_Orig.end(); itEds++){ 
		int m1 = 0;
		EN_getDataInt (*itEds, MD_lookupMeshDataId("geoflag"), &m1);
		if(m1!=0){
			PSEDGES.insert(*itEds);
		}
	}

	for(itEds=PSEDGES.begin(); itEds!=PSEDGES.end(); itEds++){ 

		PSEDGES_Orig.erase(*itEds);

	}

	//Get_Angular_Coef(PSEDGES);  // Esta função attacha em "Angular_Coef" o coeficiente angular da aresta cabe a mim depois verificar esta valor e separar as que tenham o mesmo coeficiente angular
					
	while(PSEDGES.size()>0){
	
	
		//SameCoefEdges.clear();
		pEdge Edge1 = *PSEDGES.begin();  //PEGA UMA ARESTA
		// Verifica o par de plane surfaces
		int PS1 = 0;
		EN_getDataInt (Edge1, MD_lookupMeshDataId("Domain"), &PS1);
		int PS2 = 0;
		EN_getDataInt (Edge1, MD_lookupMeshDataId("Domain2"), &PS2);
		
		int m1 = 0;
		int m2 = 0;
		
		EN_getDataInt (Edge1, MD_lookupMeshDataId("geoflag"), &m1);
	
		SameCoefEdges.insert(Edge1);

		//cout << endl << "m1 " << m1 << " Aresta " << EN_id(Edge1) << endl << endl;
		int QF1 = E_numFaces(Edge1);
		int QF2 = 0;
		if(QF1==0){
			for(itEds=PSEDGES.begin(); itEds!=PSEDGES.end(); itEds++){  // E PEGA AS OUTRAS Q TIVEREM MESMO COEFICIENTE ANGULAR e pertencerem ao mesmo PAR de plane surfaces e tenham a mesma quantidade de faces
				
					pEdge Edge2 = *itEds;
					QF2 = E_numFaces(Edge2);
					if(QF2==0){
				
						EN_getDataInt (Edge2, MD_lookupMeshDataId("geoflag"), &m2);

						//cout << "m2 " << m2 << " Aresta " << EN_id(Edge2) << endl;

						int MSMPS = 0;
		
						int P1 = 0;
						EN_getDataInt (Edge2, MD_lookupMeshDataId("Domain"), &P1);
						int P2 = 0;
						EN_getDataInt (Edge2, MD_lookupMeshDataId("Domain2"), &P2);
			
						if ((P1==PS1 && P2==PS2)||(P1==PS2 && P2==PS1)){
							MSMPS = 1;
						}
					
						if (m1==m2 && MSMPS==1 && QF1 == QF2){
					
							//cout << "m1 == m2    m1 " << m1 << " m2 " << m2 << endl;
							if(getEdgeFlag(Edge2)==2000){
	
								SameCoefEdges.insert(Edge2);
							
	
							}
						}
					}
					
				
			}
		}
		cout << "Antes do if - SameCoefEdges.size() " << SameCoefEdges.size() << endl;

		Save_LineLoop2D(theMesh, SameCoefEdges);

		if (SameCoefEdges.size()!=0){
			int SCFZ = SameCoefEdges.size();
			while (SameCoefEdges.size()>1){

				cout << "SameCoefEdges.size() tinha " << SCFZ << ", Agora tem " << SameCoefEdges.size() << endl;

				// iniciando as rotinas merge_edge
				pEdge Old_Edg1 = *SameCoefEdges.begin();
				pEdge Old_Edg2 = *SameCoefEdges.begin();

				pVertex Vert1 = E_vertex(Old_Edg1,0);
				pVertex Vert2 = E_vertex(Old_Edg2,1);
				SameEdge.insert(Old_Edg1);

				int GF = 0;
				EN_getDataInt (Old_Edg1, MD_lookupMeshDataId("geoflag"), &GF);
				//Nao usa mais o Geoflag como ID, pois este pode coincidir com alguma aresta... tem que criar um id... e inserir em um map com o correspondente 				geoflag para anexar às arestas depois...
				int v1 = 0;
				int v2 = 0;
				int V = 1;

				//agora está identificando as arestas que serao unidas

				int d1 = 0;
				int d2 = 0;
				while (V==1){  // Preenchendo SameEdge
					v1 = 0;
					v2 = 0;
	
					int Numb_Edges1 = V_numEdges(Vert1);
					int Numb_Edges2 = V_numEdges(Vert2);
	
					if (V_numFaces(Vert1)==0){
						for(int i = 0 ; i < Numb_Edges1 ; i++){ 
							pEdge Edg1 = V_edge(Vert1, i);
							if (Edg1!=Old_Edg1 && SameCoefEdges.count(Edg1)==1 ){
							
								Vert1 = E_otherVertex(Edg1, Vert1);
								Old_Edg1 = Edg1;
								SameEdge.insert(Edg1);
								v1 = 1;

							}
						}
					}
					
					if (V_numFaces(Vert2)==0){
					for(int i = 0 ; i < Numb_Edges2 ; i++){
							pEdge Edg2 = V_edge(Vert2, i);
							if (Edg2!=Old_Edg2 && SameCoefEdges.count(Edg2)==1 ){
								//cout <<"V=1 STOP()" << endl;
								Vert2 = E_otherVertex(Edg2, Vert2);
								Old_Edg2 = Edg2;
								SameEdge.insert(Edg2);
								v2 = 1;

							
							}
						}
					}
					if (v1==0 && v2==0){ // Se não há novas insercoes


						V=0;
					}//if (v1==0 && v2==0){
				}  //While V==1
						if(SameEdge.size() > 1){			// Cria-se a nova aresta a partir do merge das arestas anteriores
							pGEntity ent = E_whatIn(Old_Edg2);
							//pEdge edg = M_createE(theMesh, Vert1, Vert2, ent);
							// Verificar se já não existe a tal aresta...
							int T = 0;
							pEdge edg;
							EIter eit = M_edgeIter(theMesh);
							while (pEdge edge = EIter_next(eit)){
								pVertex V1 = E_vertex(edge,0);
								pVertex V2 = E_vertex(edge,1);
								if(V1==Vert1 && V2==Vert2){	//Existe!!
									edg = edge;
									T=1;
									
								}
								if(V2==Vert1 && V1==Vert2){	//Existe!!
									edg = edge;
									T=1;
								}

							}
							EIter_delete(eit);
							
							
							if(T==0){
							
								edg = M_createE(theMesh, Vert1, Vert2, ent);
								NEID++;
								GeoMap.insert( std::pair<int,int>(NEID*1000, GF) );
								EN_setID(edg, NEID*1000);
								EN_attachDataInt (edg, MD_lookupMeshDataId("CEDGE"), 1);

								M_Edges.insert(edg);
						
							}
							

							NewEdgeID = NewEdgeID++;
							PSEDGES_Orig.insert(edg);
							EN_attachDataInt (edg, MD_lookupMeshDataId("Domain"), d1);
							EN_attachDataInt (edg, MD_lookupMeshDataId("Domain2"), d2);
							NewEdgeID++;
							for(itEds = SameEdge.begin(); itEds != SameEdge.end(); itEds++) {
							
								SameCoefEdges.erase(*itEds);
								PSEDGES.erase(*itEds);

							}



						}//if(SameEdge.size() > 1){

						if(SameEdge.size() == 1){
							itEds=SameEdge.begin();
							PSEDGES.erase(*itEds);
							PSEDGES_Orig.insert(*itEds);
							SameCoefEdges.erase(*itEds);
			
							GF = 0;
							EN_getDataInt (*itEds, MD_lookupMeshDataId("geoflag"), &GF);
			
							GeoMap.insert( std::pair<int,int>(EN_id(*itEds), GF));
						}
						for(itEds = SameEdge.begin(); itEds != SameEdge.end(); itEds++) {
							
							SameCoefEdges.erase(*itEds);

						}
						SameEdge.clear();
					//				cout << " -------------10-------------" << endl;
					//}//if (v1==0 && v2==0){  Se não há novas insercoes
				//}  //While V==1

				
	
			} //while samecoefedges >1
			if (SameCoefEdges.size()==1){
				itEds=SameCoefEdges.begin();
				PSEDGES.erase(*itEds);
				PSEDGES_Orig.insert(*itEds);
				int GF = 0;
				EN_getDataInt (*itEds, MD_lookupMeshDataId("geoflag"), &GF);
			
				GeoMap.insert( std::pair<int,int>(EN_id(*itEds), GF));
			}
			SameCoefEdges.erase(*itEds);
		
		}//if samecoefedges != 0
		else{
			EN_attachDataInt (Edge1, MD_lookupMeshDataId("CEDGE"), 1);
			EN_attachDataInt (Edge1, MD_lookupMeshDataId("Domain"), 0);
			EN_attachDataInt (Edge1, MD_lookupMeshDataId("Domain2"), 0);
			PSEDGES.erase(Edge1);
			PSEDGES_Orig.insert(Edge1);
			SameCoefEdges.clear();
			
			//EN_attachDataInt (Edge1, MD_lookupMeshDataId("TODELETE"), 1);
			
			int GF = 0;
			EN_getDataInt (Edge1, MD_lookupMeshDataId("geoflag"), &GF);

			GeoMap.insert( std::pair<int,int>(EN_id(Edge1), GF) );
			
		}
	}//while(PSEDGES.size()>0){

	//PSEDGES.clear();
	cout << "Depois de tudo SameCoefEdges.size() " << SameCoefEdges.size() << endl << endl;
	SameCoefEdges.clear();
	

	
	cout <<"Conferindo as arestas que Merge_PS_Edges está devolvendo"<< endl;
	for(TR=PSEDGES_Orig.begin();TR!=PSEDGES_Orig.end();TR++){
	
	
		pVertex V1 = E_vertex(*TR,0);
		pVertex V2 = E_vertex(*TR,1);
	//	cout <<"Aresta " << EN_id(*TR) << " Nós " << EN_id(V1) << " " << EN_id(V2) << endl;
	
	}
	cout << endl;
	cout << "Finalizou Merge_PS_Edges - Arestas devolvidas: " << PSEDGES_Orig.size() << endl << endl;
	//STOP();
	return PSEDGES_Orig;

}

pEdge achaAresta(pEdge ed, list< set<pEdge> > dom) {
	list< set<pEdge> >::iterator listIT;
	set<pEdge>::iterator setIT;

	pVertex ponto1ed = E_vertex(ed,0);
	pVertex ponto2ed = E_vertex(ed,1);

	for(listIT = dom.begin(); listIT != dom.end(); listIT++) {

	    for(setIT = listIT->begin(); setIT != listIT->end(); setIT++) {

		pVertex ponto1 = E_vertex(*setIT,0);
		pVertex ponto2 = E_vertex(*setIT,1);

		if( (EN_id(ponto1) == EN_id(ponto1ed)) && (EN_id(ponto2) == EN_id(ponto2ed)) ) {
		    //achou

		    
		    
		    return *setIT;
		}
		else if( (EN_id(ponto2) == EN_id(ponto1ed)) && (EN_id(ponto1) == EN_id(ponto2ed)) ) {
		    //achou
		    return *setIT;
		}

	    }

	}
	return NULL;
}
	


pEdge percorreMap(pEdge ed, map<int, list< set<pEdge> > > doms) {


    map<int, list< set<pEdge> > >::iterator mapIt;

    for(mapIt = doms.begin(); mapIt != doms.end(); mapIt++) {
	pEdge novaAresta = achaAresta(ed, mapIt->second);
	
        if(novaAresta) {
            //achou...
            return novaAresta;
        }

    }
    
    return NULL;
   
}
	
void Remover::Merge_PS(map<int, list< set<pEdge> > > DomainsPS) {
     
	map<int, list< set<pEdge> > >::iterator mapIt;
	list< set<pEdge> >::iterator listIT;
	set<pEdge>::iterator setIT;



	for(mapIt = DomainsPS.begin(); mapIt != DomainsPS.end(); mapIt++) {

	    for(listIT = mapIt->second.begin(); listIT != mapIt->second.end(); listIT++) {

		for(setIT = listIT->begin(); setIT != listIT->end();) {
		    pEdge novaAresta = percorreMap(*setIT, DomainsPS);
		    pEdge arestaAntiga = *setIT;
		    
		    
		    setIT++;
		    
		    if(novaAresta) {
		        //achou...		        
			listIT->erase(arestaAntiga);
			listIT->insert(novaAresta);
		        
		    }



		}

	    }
	}

}




			





//}

int Remover::Identify_and_Remove_Edges_2D_II(pMesh theMesh, set<int> setdomains, std::list<pEntity> &elementList, set<pFace> FCS){ 


// É necessário criar uma rotina que mapeie as arestas que pertencem a alguma face e que me retorne essas arestas...


	cout << "Iniciou Identify_and_Remove_Edges_2D_II" << endl;

	//EIter eit = M_edgeIter(theMesh);
	//while (pEdge edge = EIter_next(eit)){
	//	pFace EFC = E_face(edge,0);


	//}
	//EIter_delete(eit);

	//map<int, list< set<pEdge> > > DPS;

	set<pFace>::iterator ITF;

	map<int,  set<pFace> > mapset;
	set <pFace> Lista;
	std::list<pFace>::iterator itfaces;
	
	for ( itfaces=elementList.begin() ; itfaces != elementList.end(); itfaces++ ){
		Lista.insert(*itfaces);
	}

	for ( ITF=FCS.begin() ; ITF != FCS.end(); ITF++ ){
		Lista.insert(*ITF);
	}

	set<int>::iterator stdom;
	set<pFace>::iterator stf;
	


	// Posso criar um map de set, adicionar a cada set as faces do dominio e depois iterar pelo map separando os PS e colocando no map de lista de set;
	set<pFace> Faces;
	
	// Primeiro, para cada dominio, separar os elementos deste dominio, map de sets
	for(stdom=setdomains.begin(); stdom!=setdomains.end(); stdom++){

		set<pFace> Faces;
		int flg;
		for(stf=Lista.begin(); stf!=Lista.end(); stf++){
			flg = getFaceFlag(*stf);
			if(flg == *stdom){
				Faces.insert(*stf);
			}
		}
		mapset.insert( std::pair<int,  set<pEdge> >(*stdom, Faces) );

	}
	// Separado... agora, iterar no map
	
	map<int,  set<pFace> >::iterator MS;
	list< set<pFace> > ListofPSFaces;
	list< set<pEdge> > ListofPSEdges;
	list <pFace> PSL;
	list<pFace>::iterator Lit;

	set<pFace> PS;
	int size = 0;
	int SZ=0;
	int Count =0;
	set<pEdge> Edges;
	set<pEdge> RemvEdges;
	set<pEdge>::iterator gh;
	set<pEdge> Merged_Edges;
	
	for(MS=mapset.begin(); MS!=mapset.end(); MS++){
	
		/// Criar uma funcao para separar os PS agora
		cout << "Dominio " << (MS->first) << endl;
		
		while(((MS->second).size())!=0){ //Qtd de Faces do dominio da vez as faces aqui pertencem ao dominio e à lista de elementos a remover
			pFace FD = (*(MS->second).begin()); // Pega a primeira face
			PS.insert(FD); // Insere esta face no set que armazenara as faces de um Plane Surface
			cout << "PS " << Count << " MS->second.size() " << (MS->second).size() << endl;
			int g=1; //flag

			while(g<=5){  // quando nao houver mais insercoes é pq terminou de varrer o PS

				//g=0;
				size = PS.size(); // guarda este valor para verificar se houve alguma nova inserção
				for (stf=PS.begin(); stf!=PS.end(); stf++){ // Percorre o set e insere nele alguma face do dominio
			
					pFace FC = *stf;
					pEdge Ed;
					pVertex Vt;
					for(int y = 0; y<=2; y++){

						Ed = F_edge(FC, y);

						int t = 0;
						
						if (E_numFaces(Ed) == 2){

							t = 1;
						}

						for(int w = 0; w <= t; w++){  // iterando sobre as faces vizinhas e verificando se pertencem ao dominio e à lista.

							pFace EF1 = E_face(Ed,w);
							if (getFaceFlag(EF1) == MS->first && Lista.count(EF1)==1){
								PS.insert(EF1);
								
							}
					
						}
						
						
	
					}
			
				}

				if (size==PS.size()){ // Nao houve insercoes... incrementa g, se rodar 5 vezes e nao houver insercoes então terminou de separar mesmo o PS.
					g++;
					for (stf=PS.begin(); stf!=PS.end(); stf++){
						((MS->second)).erase(*stf);

						pEdge E1;
						for (int j=0;j<=2;j++){
						
							E1 = F_edge(*stf, j);
							Edges.insert(E1);
						
						}
						//Deletar a face da malha
						theMesh->DEL(*stf);
						
						
					}
					PS.clear();

					theMesh->modifyState(1,2,0);
					theMesh->modifyState(1,2);
					theMesh->modifyState(2,1,0);
					theMesh->modifyState(2,1);

					
					for (gh=Edges.begin(); gh!=Edges.end(); gh++){
					
						int D1,D2;
						EN_getDataInt (*gh, MD_lookupMeshDataId("Domain"), &D1);
						EN_getDataInt (*gh, MD_lookupMeshDataId("Domain2"), &D2);

						// Deletar as arestas da malha, mantendo as que pertencerem a mais de um dominio
						//if (E_numFaces(*gh) == 0 && D1 == D2){
						int V = 0;
						EN_getDataInt (*gh, MD_lookupMeshDataId("CEDGE"), &V);
						if (E_numFaces(*gh)==0 && V == 0){
							if (D1==D2){
								pEdge Edg = *gh;
								RemvEdges.insert(Edg);
							}

						}
					}

					for (gh=RemvEdges.begin(); gh!=RemvEdges.end(); gh++){
					
						Edges.erase(*gh);
						theMesh->DEL(*gh);
					
					}

					RemvEdges.clear();
				}

			}
			SZ=0;
			//cout <<"Separou um PS do dominio, Esse aqui tem " << PS.size() << " faces" << endl;
			Count++;

			
			
			
			ListofPSEdges.push_back(Edges);  // colocando o novo set na lista

		//	cout << "Listando as arestas pra conferir" << endl;
			set <pEdge>::iterator ETIT;
			
			for (ETIT=Edges.begin();ETIT!=Edges.end();ETIT++){
			
				pVertex V1 = E_vertex(*ETIT,0);
				pVertex V2 = E_vertex(*ETIT,1);

				//cout << "Aresta " << EN_id(*ETIT) << " Nós " << EN_id(V1) << " " << EN_id(V2) << endl;
			
			}


			if (Edges.size()<3){
			
				cout << "Opa deu pau no Edges  --  Identify_andRemoveEdges2D_II 1605" << endl; 
				STOP();
			
			}

			//cout << "Edges.size() " << Edges.size() << " Merged_Edges.size() " << Merged_Edges.size() << endl;
			//STOP();
			Edges.clear();
			Merged_Edges.clear();

		}
		
		DomainsPS.insert ( std::pair<int, list< set<pEdge> > >((MS->first), ListofPSEdges));  // O int é o domínio, para cada domínio uma lista de sets, onde cada set é um PS
		ListofPSEdges.clear();
		//cout << "Plane surfaces " << Count << endl;
		
	}

	// Testando o Map
	
	map<int,list< set<pEdge> > >::iterator FG;
	for (FG=DomainsPS.begin();FG!=DomainsPS.end();FG++){
	
		//cout << "Dominio " << FG->first << " tamanho " << DomainsPS.size() << endl;
	
	}
	
	
//	EIter eit = M_edgeIter(theMesh);
//	while (pEdge edge = EIter_next(eit)){
	
	//	int V = 0;
	//	EN_getDataInt (edge, MD_lookupMeshDataId("CEDGE"), &V);
	
	//	if (E_numFaces(edge)==0 && V == 0){
		
			
		
	//	}

//	}
//	EIter_delete(eit);
	
	
	
	cout << "Finalizou Identify_and_Remove_Edges_2D_II" << endl;
	//STOP();

	return Count;	
}

/// Criar uma funcao que faca o merge edges depois de identify and remvoe edges







set<pEdge> Remover::Merge_Edges(pMesh theMesh, int ndom, set<int> setdomains){ 

	cout << endl << "Iniciou Merge_Edges"<< endl << endl;
	set<int>::iterator std;
	map<int, list< set<pEdge> > >::iterator itDPS;
	list< set<pEdge> >:: iterator lit;
	set<pEdge>:: iterator ETR;

	set <pEdge> M_Edges;
	
	for(std=setdomains.begin();std!=setdomains.end() ;std++){ 
		int DOMN = *std;
			
		itDPS = DomainsPS.find(DOMN); // Encontrando a lista de plane surfaces do dominio em verificacao

		set <pEdge> Merged_Edges;
		for (lit=(itDPS->second).begin(); lit!=(itDPS->second).end() ;lit++){ // iterando dentro da lista do dominio de set em set; lit é iterador para um set. cada set é um PS

			
				Refresh(theMesh);
				int neid = M_numEdges(theMesh);
				Merged_Edges.clear();
				Merged_Edges = Merge_PS_Edges(*lit, theMesh, neid, M_Edges); //Aqui inserida a função que faz o merge nas arestas

			
			
			//Substituindo o set do PS pelo novo set com as arestas mergidas
		//	*lit->clear();
		
				*lit = Merged_Edges;

		}
	}


	
	for(std=setdomains.begin();std!=setdomains.end() ;std++){
		int DOMN = *std;
			
		itDPS = DomainsPS.find(DOMN); // Encontrando a lista de plane surfaces do dominio em verificacao

		//set <pEdge> Merged_Edges;
		for (lit=(itDPS->second).begin(); lit!=(itDPS->second).end() ;lit++){ // iterando dentro da lista do dominio de set em set; lit é iterador para um set. cada set é um PS
			
			set<pEdge> TD; // colocando aqui as arestas que vao ser removidas dos PS devido a substituicao por uma unica mergida
			for(ETR=lit->begin();ETR!=lit->end();ETR++){
				int D1 = 0;
				EN_getDataInt (*ETR, MD_lookupMeshDataId("TODELETE"), &D1);
				if (D1==1 && E_numFaces(*ETR)==0){
					TD.insert(*ETR);
				}
			}
			for(ETR=TD.begin(); ETR!=TD.end();ETR++){
				lit->erase(*ETR);
			}
			TD.clear();
			
		//	cout << "Verificando o lit" << endl;
			for (ETR=lit->begin();ETR!=lit->end();ETR++){
			
			
				pVertex V1 = E_vertex(*ETR,0);
				pVertex V2 = E_vertex(*ETR,1);
			//	cout << "Aresta " << EN_id(*ETR) << " Nós " << EN_id(V1) << " " << EN_id(V2) << endl;
			
			}
			
			cout << endl;
		}
	}
	
		cout << "Finalizou Merge_Edges"<< endl << endl;

	return M_Edges;
}


int Remover::Identify_and_Remove_Edges_2D(pMesh theMesh, set<int> setdomains){ // Nova versao de IdentifySurfaces_2D deve rodar antes de remover as arestas

	cout << "Iniciou Identify_and_Remove_Edges_2D" << endl;

	set<pEdge> RemoveFromMesh;

	EIter eit = M_edgeIter(theMesh);
	while (pEdge edge = EIter_next(eit)){
		int firstvalue = 0; // atribuindo valor zero a todas as arestas no tag PlaneSurface.
		EN_attachDataInt (edge, MD_lookupMeshDataId("PlaneSurface1"), firstvalue);
		EN_attachDataInt (edge, MD_lookupMeshDataId("PlaneSurface2"), firstvalue);


	}
	EIter_delete(eit);

	


	set<pEdge>:: iterator pite;
	set<int>::iterator stdom;

	set<pEdge> ControlEdges;
	for(pite=BoundaryEdges.begin(); pite!=BoundaryEdges.end(); pite++){
		ControlEdges.insert(*pite);
	}

	int D1, D2;
	set<pEdge>:: iterator pEt;
	int PScounter = 1;
	int DomainCounter = 1;
	set <pEdge> DomainEdges;

	/// Iterar pelos dominios

	int contadordePS = 0;

	//while(ControlEdges.size()!=0){
		set <pEdge> PSFEdges;
		
		for(stdom=setdomains.begin(); stdom!=setdomains.end(); stdom++){
		
			cout << "No domain " << *stdom << endl;
			// A lista nova é aqui
			
			list< set<pEdge> > ListofPSEdges;
			
			/////////// Um laço para buscar as arestas que pertencam ao Domain da vez
			for (pEt=ControlEdges.begin();pEt!=ControlEdges.end(); pEt++){
	
				EN_getDataInt (*pEt, MD_lookupMeshDataId("Domain"), &D1);
				EN_getDataInt (*pEt, MD_lookupMeshDataId("Domain2"), &D2);
	
				//cout << "Dominio D1 " << D1 << endl;
				//STOP();
		
				if (D1==*stdom || D2==*stdom){
	
					DomainEdges.insert(*pEt);
				}
				
			}
			/////////////
	
			while (DomainEdges.size()!=0){  // Agora vou separar as arestas dos plane surfaces desse Domain
			
				PSFEdges.insert(*DomainEdges.begin());
			
				int withNInst = 1;
	
				while (withNInst==1){  // Esse while separa as arestas pertencentes a um Plane Surface
	
					withNInst = 0; // <-- Abreviacao de with new insertions...
	
	

					pEdge E;

					for(pite=PSFEdges.begin(); pite!=PSFEdges.end(); pite++){
		
						pVertex EdVt1 = E_vertex(*pite, 0);
		
						pVertex EdVt2 = E_vertex(*pite, 1);
		
						int size = PSFEdges.size();
		
						for(int N=0; N<V_numEdges(EdVt1); N++){  // Pegando as arestas de um nó e inserindo no set
		
							E = V_edge(EdVt1, N);
	
		
							if(E_numFaces(E)<=1 && DomainEdges.count(E)==1){ // TAVA ESCAPANDO PELAS ARESTAS DO CONTORNO DA GEOMETRIA
		
								PSFEdges.insert(E);
		
								if (size != PSFEdges.size()){
		
									withNInst=1; // Se foi uma nova insercao seta -> withNInst=1;
		
								}
		
							}
		
						}
	
		
	
						for(int N=1; N<=V_numEdges(EdVt2); N++){ // Pegando as arestas de um nó e inserindo no set
							pEdge E = V_edge(EdVt2, N-1);
							if(E_numFaces(E)<=1 && DomainEdges.count(E)==1){
								PSFEdges.insert(E);
								if (size != PSFEdges.size()){
									withNInst=1; // Se foi uma nova insercao seta -> withNInst=1;
								}
							}
						}
	
		
	
	
					}
					
					
				} // Aqui fecha o while, todas as arestas de um plane surface foram separadas
				

				
				
				ListaSets.push_back(new set<pVertex>);
				
				set<pVertex>* setVertices = ListaSets.back();
				
				set<pEdge> RemovePSFEdges;
				
				for(pite=PSFEdges.begin(); pite!=PSFEdges.end(); pite++){ // Limpando o set de arestas indesejaveis agora.

///////////////////////////////////////// Para o arquivo ADP /////////////
					pVertex V1 = E_vertex(*pite,0);
					pVertex V2 = E_vertex(*pite,1);
	
					setVertices->insert(V1);
					setVertices->insert(V2);
//////////////////////////////////////////////////////////////////////////
	
	
	
					int Dmn1,Dmn2;
	
					if(E_numFaces(*pite)== 0 && MeshBoundEdges.count(*pite)== 0 ){	// Removendo arestas internas do PS que nao vao formar o lineloop				
						RemovePSFEdges.insert(*pite);
						
					}


					
				}
	
	
				for(pite=RemovePSFEdges.begin(); pite!=RemovePSFEdges.end(); pite++){ 
				
					PSFEdges.erase(*pite);
					DomainEdges.erase(*pite);
					ControlEdges.erase(*pite);
					BoundaryEdges.erase(*pite);
					//theMesh->DEL(*pite);	
					RemoveFromMesh.insert(*pite);			
				}
				for(pite=PSFEdges.begin(); pite!=PSFEdges.end(); pite++){ 
					DomainEdges.erase(*pite);
				}
	
				RemovePSFEdges.clear();

				cout << "PSFEdges.size() " << PSFEdges.size() << endl;
				//STOP();
	
	
				/// Aqui é a hora de inserir as arestas de um PS na lista.
				ListofPSEdges.push_back(PSFEdges);
				cout << "PSFEdges.size() " << PSFEdges.size() << endl;
				contadordePS++;
				PSFEdges.clear();
				PScounter++;
			}
		
		DomainsPS.insert ( std::pair<int, list< set<pEdge> > >(*stdom, ListofPSEdges) );
		}
		//DomainCounter++;
		
		
	//}
	

	PScounter = PScounter - 1;
  	
  	cout << endl << "Qtd de dominios " << DomainsPS.size() << endl;
  	cout << endl << "Qtd de PS " << contadordePS << endl;
  	
  	map<int, list< set<pEdge> > >::iterator itDPS;
	list< set<pEdge> >:: iterator lit;
	set<int>:: iterator ait;
	set<pEdge>:: iterator sit;
	
	for (ait=setdomains.begin();ait!=setdomains.end() ;ait++){  //Preciso modificar isso para iterar dentro do DomainsPS
		int DOMN = *ait;
			
		cout << "Trabalhando no Dominio " << DOMN << endl;

		itDPS = DomainsPS.find(DOMN); // Encontrando a lista do dominio

		//cout << "Existem " << (itDPS->second).size() << " Plane Surfaces neste dominio" << endl;
  	
  	}
  	
  	for(pite=RemoveFromMesh.begin(); pite!=RemoveFromMesh.end(); pite++){ 
  	
  		theMesh->DEL(*pite);
  	
  	}
  	
	return PScounter;
	
}

map<int, list< set<pEdge> > > Remover::rDomainsPS(){

	return DomainsPS;

}

map<int, int > Remover::ReturnGeoMap(){

	return GeoMap;

}


void Remover::Merge_Edges(pMesh theMesh2) { // Copia de uma malha para a outra, as arestas que tem ponto em comum com a malha para onde vai, as arestas que vao ser copiadas sao SourceEdges e elas serao criadas na malha que contem os ReferenceNodes


	cout << "Iniciou Merge_Edges" << BoundaryNodes.size() << endl;


	// Pego os BoundaryNodes de theMesh2 e mapeio (coloco em um map)
	set<pVertex>::iterator itRNodes;
	for ( itRNodes=BoundaryNodes.begin() ; itRNodes != BoundaryNodes.end(); itRNodes++ ){
		double xyz[3];
		V_coord(*itRNodes, xyz);
		string ponto = "."; 
		ostringstream oss;
		oss << xyz[0];
		oss << ponto;
		oss << xyz[1];

		string XY = oss.str();

		CommonVertex.insert ( pair<string, pVertex>(XY, *itRNodes) );
	}

	// quando criar a aresta em theMesh2, procuro quem é o ponto em theMesh2 (em CommonVertex) e o uso.

	// iniciando criação das arestas
	map<string,pVertex>::iterator itMapVert;
	set<pEdge>::iterator itSEdges;
	for ( itSEdges=AdaptEdges.begin() ; itSEdges != AdaptEdges.end(); itSEdges++ ){

		pGEntity ent = E_whatIn(*itSEdges);
		pVertex vtx1 = E_vertex(*itSEdges, 0);
		pVertex vtx2 = E_vertex(*itSEdges, 1);

		double xyz[3];

		// criando chave para o no 1
		V_coord(vtx1, xyz);
		string ponto = "."; 
		ostringstream oss;
		oss << xyz[0];
		oss << ponto;
		oss << xyz[1];
		string XY = oss.str();


		itMapVert = CommonVertex.find(XY);
		pVertex Node1 = (*itMapVert).second;

		// criando chave para o no 2
		V_coord(vtx2, xyz);
		ostringstream oss2;
		oss2 << xyz[0];
		oss2 << ponto;
		oss2 << xyz[1];
		XY = oss2.str();

		itMapVert = CommonVertex.find(XY);
		pVertex Node2 = (*itMapVert).second;

		// criando arestas na malha alvo

		pEdge edg = M_createE(theMesh2, Node1, Node2, ent);  // Esse theMesh2 aí ta suspeito...

		// Agora pegando o PS de um dos nós e atribuindo à aresta

		int psreader = 0;
		EN_getDataInt (Node1, MD_lookupMeshDataId("PlaneSurface"), &psreader);
		EN_attachDataInt (edg, MD_lookupMeshDataId("PlaneSurface"), psreader);

		//Inserindo a aresta em BoundaryEdges
		BoundaryEdges.insert(edg);

	}

}


void Remover::ResetMRE() {  // Apaga o conteúdos de todos os sets da classe MRE para reinício e criação da malha de adaptação
	// Faz uma cópia de BoundaryNodes e BoundaryEdges em AdaptNodes e AdaptEdges para gerar o arquivo de geometria com eles.

	set<pVertex>::iterator itpVert;
	for ( itpVert=BoundaryNodes.begin() ; itpVert != BoundaryNodes.end(); itpVert++ ){
		AdaptNodes.insert(*itpVert);
	}

	set<pEdge>::iterator itpEdge;
	for ( itpEdge=BoundaryEdges.begin() ; itpEdge != BoundaryEdges.end(); itpEdge++ ){
		AdaptEdges.insert(*itpEdge);
	}


	GeometryBoundNodes.clear(); // acho que este está sem uso... verificar...
	GeomBoundNodes.clear();
	GeomBoundEdges.clear();
	MeshBoundEdges.clear();
	MeshBoundNodes.clear();
	BoundaryNodes.clear();
	BoundaryEdges.clear();
	RemovefromBoundEdges.clear(); // tive que colocar pra nao dar erro na contagem.
	RemovefromBoundNodes.clear();
	RemovefromBoundFaces.clear();
	//BoundaryFaces.clear(); // transformei o antigo set Faces em set BoundaryFaces
	GeomBoundFaces.clear(); // criado o GeomBoundFaces
	//	set<pFace> Facestoremove; //set do estimador de erro
	//	set<pFace> Facestoremove2; //set do estimador de erro
	//	set<pFace> Facestoremove3; //set do estimador de erro
	//	set<pFace> Facestoremove4; //set do estimador de erro
	Tetras.clear(); // Originalmente so este set é pRegion e o resto é pFace.
	argi.clear();


}


void Remover::get_merged_meshes(pMesh theMesh){

	VIter itVertex = M_vertexIter(theMesh);
	while (pVertex VT = VIter_next(itVertex)){

		int J=0;
		double xz2[3];
		V_coord(VT, xz2);
		
		EN_getDataInt (VT, MD_lookupMeshDataId("Domain"), &J);
		
		if ( xz2[0]==0 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 10); // Atribuindo um Id renumerado, em ordem para todos os vertex

		}
		if ( xz2[0]==1 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 51); // Atribuindo um Id renumerado, em ordem para todos os vertex

		}

		if ( xz2[0]==0 &&  xz2[1]==1){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		}
		if ( xz2[0]==1 &&  xz2[1]==0){


		EN_attachDataInt (VT, MD_lookupMeshDataId("Domain"), 1100); // Atribuindo um Id renumerado, em ordem para todos os vertex

		}
	
		
	}

	VIter_delete(itVertex);

/*
FIter facit = M_faceIter(theMesh);
	while (pFace Fac = FIter_next(facit)){
	
		double xyz1[3];
		double xyz2[3];
		double xyz3[3];
	
		//Calculando o centro da face
		V_coord((pVertex)Fac->get(0,0), xyz1);
		V_coord((pVertex)Fac->get(0,1), xyz2);
		V_coord((pVertex)Fac->get(0,2), xyz3);

		double X = (xyz1[0] + xyz2[0] + xyz3[0])/3;
		double Y = (xyz1[1] + xyz2[1] + xyz3[1])/3;


		int Domain = 0;
		//Domain = getFaceFlag(Fac);
	}

	FIter_delete(facit);
*/

	EIter eit = M_edgeIter(theMesh);
	while (pEdge edge = EIter_next(eit)){

		if (E_numFaces(edge)==1){

			pFace F1 = E_face(edge,0);
			int EdgeDomain1;
			EN_getDataInt (F1, MD_lookupMeshDataId("Domain"), &EdgeDomain1);
			EN_attachDataInt (edge, MD_lookupMeshDataId("Domain"), 2200);

		}

		if (E_numFaces(edge)==2){


			pFace F1 = E_face(edge,0);
			pFace F2 = E_face(edge,1);

			int EdgeDomain1, EdgeDomain2;

			EN_getDataInt (F1, MD_lookupMeshDataId("Domain"), &EdgeDomain1);
			EN_getDataInt (F2, MD_lookupMeshDataId("Domain"), &EdgeDomain2);

			if (EdgeDomain1!=EdgeDomain2){
		
				EN_attachDataInt (edge, MD_lookupMeshDataId("Domain"), 2200);	
		
			}

			if (EdgeDomain1==EdgeDomain2){

				EN_attachDataInt (edge, MD_lookupMeshDataId("Domain"), EdgeDomain1);
			}

		}

	}
	EIter_delete(eit);	

}


void Remover::wrt_merged_mesh(pMesh theMesh){

	cout << "M_numVertices(theMesh) " << M_numVertices(theMesh) << endl;

	int newID = 0;

	system("rm Final_01.msh");

	cout << "Salvando malha 2D - Final_01.msh" << endl;

	ofstream Myfile("Final_01.msh", ios::app);

	if (!Myfile) {
		Myfile.open("Final_01.msh");
	}

	int TotalVertex = M_numVertices(theMesh);

	Myfile << "$NOD" << endl;
	Myfile << TotalVertex << endl;


	// Criando os vertex no arquivo de malha
	//double newID = 0;
	set<pVertex>::iterator GH;
	int CC = 0;
	set<pVertex> DomainedNodes;
	set<pEdge> DomainedEdges;
	
	VIter itVertex = M_vertexIter(theMesh);
	while (pVertex VT = VIter_next(itVertex)){

		int J=0;
		double xz2[3];
		V_coord(VT, xz2);
		
		EN_getDataInt (VT, MD_lookupMeshDataId("Domain"), &J);
		
		if (J!=0){

			DomainedNodes.insert(VT);
			
		}

		
		newID++;
		EN_attachDataInt (VT, MD_lookupMeshDataId("RenumberID"), newID); // Atribuindo um Id renumerado, em ordem para todos os vertex
		EN_getDataInt (VT, MD_lookupMeshDataId("RenumberID"), &J);
		Myfile << J << " " << xz2[0] << " " << xz2[1] << " " << xz2[2] << endl;
		
	
		
	}

	VIter_delete(itVertex);



	EIter eit = M_edgeIter(theMesh);
 	while (pEdge edge = EIter_next(eit)){

		if (E_numFaces(edge)==2){

			newID++;

			int dmn1 =0; 
			int dmn2 =0;
		
			pFace FCE1 = E_face(edge,0);
			pFace FCE2 = E_face(edge,1);

			EN_getDataInt (FCE1, MD_lookupMeshDataId("Domain"), &dmn1);
			EN_getDataInt (FCE2, MD_lookupMeshDataId("Domain"), &dmn2);

			EN_attachDataInt (edge, MD_lookupMeshDataId("RenumberID"), newID);

			pVertex V1 = E_vertex(edge, 0);

			int ID1 = 0;
			EN_getDataInt (V1, MD_lookupMeshDataId("RenumberID"), &ID1);

			pVertex V2 = E_vertex(edge, 1);

			int ID2 = 0;
			EN_getDataInt (V2, MD_lookupMeshDataId("RenumberID"), &ID2);

			if (dmn1!=dmn2){
		//		Myfile << newID << " 1 " << "2000" << " 1 2 " << ID1 << " " << ID2 << endl;
				DomainedEdges.insert(edge);


			}
			if (dmn1==dmn2){

				//Myfile << newID << " 1 " << dmn1 << " 1 2 " << ID1 << " " << ID2 << endl;
	
			}
		}
		if (E_numFaces(edge)==1){
			newID++;
			int dmn1 =0; 
			pFace FCE1 = E_face(edge,0);
			EN_getDataInt (FCE1, MD_lookupMeshDataId("Domain"), &dmn1);
			EN_attachDataInt (edge, MD_lookupMeshDataId("RenumberID"), newID);

			pVertex V1 = E_vertex(edge, 0);

			int ID1 = 0;
			EN_getDataInt (V1, MD_lookupMeshDataId("RenumberID"), &ID1);

			pVertex V2 = E_vertex(edge, 1);

			int ID2 = 0;
			EN_getDataInt (V2, MD_lookupMeshDataId("RenumberID"), &ID2);
		
		//	Myfile << newID << " 1 " << dmn1 << " 1 2 " << ID1 << " " << ID2 << endl;
			DomainedEdges.insert(edge);
		
		}
 	}
 	EIter_delete(eit);

	int TotalFaces = M_numFaces(theMesh);
	int TotalEdges = DomainedEdges.size();
	int TotalNodes = DomainedNodes.size();
	
	Myfile << "$ENDNOD" << endl;
	Myfile << "$ELM" << endl;
	Myfile << TotalFaces+TotalEdges+TotalNodes<< endl;

	for ( GH=DomainedNodes.begin() ; GH != DomainedNodes.end(); GH++ ){
	
		int k = 0;
		int H = 0;
		pVertex FG = *GH;
		EN_getDataInt (FG, MD_lookupMeshDataId("RenumberID"), &k);
		EN_getDataInt (FG, MD_lookupMeshDataId("Domain"), &H);
		
		Myfile << k << " 15 "  << H << " " << k << " 1 " << k << endl;
	
	}

	set<pEdge>::iterator ped;
	for ( ped=DomainedEdges.begin() ; ped != DomainedEdges.end(); ped++ ){
		int y=0;
		EN_getDataInt (*ped, MD_lookupMeshDataId("RenumberID"), &y);

		pVertex V1 = E_vertex(*ped, 0);

		int ID1 = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("RenumberID"), &ID1);

		pVertex V2 = E_vertex(*ped, 1);

		int ID2 = 0;
		EN_getDataInt (V2, MD_lookupMeshDataId("RenumberID"), &ID2);
		
		
		
		Myfile << y << " 1 " << "2200" << " 1 2 " << ID1 << " " << ID2 << endl;

	}

	FIter itFace = M_faceIter(theMesh);
	while (pFace FC = FIter_next(itFace)){
		newID++;
		int DF = 0;
		//EN_getDataInt (FC, MD_lookupMeshDataId("Domain"), &DF);


		DF = getFaceFlag(FC);
		pVertex V1 = F_vertex(FC, 0);
		int ID1 = 0;
		EN_getDataInt (V1, MD_lookupMeshDataId("RenumberID"), &ID1);

		pVertex V2 = F_vertex(FC, 1);
		int ID2 = 0;
		EN_getDataInt (V2, MD_lookupMeshDataId("RenumberID"), &ID2);

		pVertex V3 = F_vertex(FC, 2);
		int ID3 = 0;
		EN_getDataInt (V3, MD_lookupMeshDataId("RenumberID"), &ID3);


		//if(DF==3300){
		Myfile << newID << " 2 "<< DF <<  " 1 3 " << ID1 << " " << ID2 << " " << ID3 << endl;
		//Myfile << newID << " 2 3300"<<  " 1 3 " << ID1 << " " << ID2 << " " << ID3 << endl;

		//}

		if(DF==3301){
			//Myfile << newID << " 2 "<< DF <<  " 2 3 " << ID1 << " " << ID2 << " " << ID3 << endl;
			//Myfile << newID << " 2 "<< "0" <<  " 1 3 " << ID1 << " " << ID2 << " " << ID3 << endl;
		}

		//if(DF==2){
		//	Myfile << newID << " 2 "<< "3301" << " 2 3 " << ID1 << " " << ID2 << " " << ID3 << endl;
		//	Myfile << newID << " 2 "<< "0" <<  " 1 3 " << ID1 << " " << ID2 << " " << ID3 << endl;
		//}

	}
	FIter_delete(itFace);

	Myfile << "$ENDELM" << endl;


}

void Remover::SaveADPFile(int PSCounter, int size){

	system("rm BGM.adp");

	cout << endl;

	ofstream Myfile("./BGM.adp", ios::app);

	if (!Myfile) {
		Myfile.open("./BGM.adp");
	}

	// ROTINA QUE CRIA A LISTA DE PONTOS INICIAL DO ARQUIVO - IDS E COORDENADAS DE TODOS OS PONTOS

	Myfile << "#Surfaces " << endl;
	
	Myfile << PSCounter << endl;

	double xyz[3];
	double height;
	set<pVertex>::iterator SPV;
	list< set<pVertex>* >::iterator AIT;
	int CT = 1;

	//for(AIT=ListaSets.begin();AIT!=ListaSets.end();AIT++){
	//	Myfile << "Surface " <<  CT << endl;
	//	Myfile << "#Points " << (*AIT)->size() << endl;
	//	CT++;
	//	for (SPV=(*AIT)->begin();SPV!=(*AIT)->end();SPV++){

	//		V_coord(*SPV, xyz);
	//		EN_getDataDbl(*SPV,MD_lookupMeshDataId( "elem_height" ),&height);
	//		Myfile << xyz[0] <<" "<< xyz[1] <<" "<< xyz[2] << " " << height << endl;

	//	}
	
	//}


		Myfile << "Surface " <<  CT << endl;
		Myfile << "#Points " << 3*size << endl;

	for(int c=0;c<size;c++){
	
		Myfile << MtxTriangles[c].x1 << " " << MtxTriangles[c].y1 << " " << MtxTriangles[c].z1 << " " << MtxTriangles[c].Cl1 << endl;
		Myfile << MtxTriangles[c].x2 << " " << MtxTriangles[c].y2 << " " << MtxTriangles[c].z2 << " " << MtxTriangles[c].Cl2 << endl;
		Myfile << MtxTriangles[c].x3 << " " << MtxTriangles[c].y3 << " " << MtxTriangles[c].z3 << " " << MtxTriangles[c].Cl3 << endl;

	}


}


















