//Rebuilder class

#include "includes.h"
#include <string>

class Rebuilder3
{
public:
	// Contructor and Destructor

	pMesh theMesh;

	Rebuilder3 ();
	~Rebuilder3();

	void MakeMesh_2D (string FileName);
	void MakeMesh_2D_RMSH (string FileName);
	void MakeAdaptMesh_2D ();
	//set <int> ReturnListofElements();
	//int Ident;
	void RefineMesh_2D();
	void MergeMesh_2D(pMesh, pMesh);
	void MergeMesh(pMesh, pMesh);
	void AttachFlags(pMesh, map<int,int>);
	void get_Flags(pMesh);
	void MakeMerge_2D(char * , char * ,char * );

	//set <pVertex> CNodes;
	double Flag_Coords [110][3];  // Modificar para receber qq quantidade de nós

private:

	//set <int> ListofElements;
	//set <pFace> BoundaryFaces;
	//set <pEdge> BoundaryEdges;
	//set <pVertex> CNodes;

};

