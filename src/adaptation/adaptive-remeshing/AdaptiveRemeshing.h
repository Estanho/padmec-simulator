/*
 * MeshAdaptaion__Remeshing.h
 *
 *  Created on: 24/01/2013
 *      Author: rogsoares
 */

#ifndef MESHADAPTAION__REMESHING_H_
#define MESHADAPTAION__REMESHING_H_

#include "AMR.h"
#include "Gmsh.h"
#include "ErrorEstimator.h"
#include "MRE.h"
#include "FileManager.h"
//#include "Neural.h"
#include "Rebuilder2.h"
#include "Rebuilder3.h"
#include "Gmsh.h"
#include <stdio.h>
#include <stdlib.h>
#include "Gmsh.h"
#include <sstream>
#include <iostream>
#include <set>
#include "GModel.h"
#include "PView.h"
#include "PViewData.h"
#include "PluginManager.h"
#include "auxiliar.h"

class AdaptiveRemeshing : public AMR{
public:

	AdaptiveRemeshing(int argc, char* argv[]);
	~AdaptiveRemeshing();

	//void rodar(ErrorAnalysis*,pMesh);
	void rodar(ErrorAnalysis *pErrorAnalysis, pMesh & theMesh);
	void Refresh (pMesh & theMesh);


private:

	int ndom;
	int* domList;
};


#endif /* MESHADAPTAION__REMESHING_H_ */
