#include "ErrorAnalysis.h"
#include <string>


class ErrorEstimator
{
public:
	// Contructor and Destructor
	ErrorEstimator(pMesh theMesh);
	~ErrorEstimator();

	set<int> ElementsIds;

	list<pEntity> elementList;

	void VertexList (pMesh theMesh);
	void LeitorDeLista(ErrorAnalysis *pErrorAnalysis, pMesh theMesh);
	void Clear_Containers();
	void Create_Domains(pMesh theMesh);

private:
	char meshFilename[];

};

