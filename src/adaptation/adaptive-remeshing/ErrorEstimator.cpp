#include "ErrorEstimator.h"
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <math.h>
#include <cmath>
#include "PhysicPropData.h"
#include "AttachData.h"

ErrorEstimator::ErrorEstimator (pMesh theMesh){}

ErrorEstimator::~ErrorEstimator (){
	Clear_Containers();
}

void ErrorEstimator::Clear_Containers(){

	ElementsIds.clear();
	elementList.clear();
}

void ErrorEstimator::Create_Domains (pMesh theMesh){
	
	FIter facit = M_faceIter(theMesh);
	while (pFace Fac = FIter_next(facit)){
	
		int Domain = 0;
		Domain = getFaceFlag(Fac);

		int EdgeDomain = 0;

		for (int t=0;t<=2;t++){

			EN_getDataInt (F_edge(Fac, t), MD_lookupMeshDataId("Domain"), &EdgeDomain);

			if (EdgeDomain != 0){
				EN_attachDataInt (F_edge(Fac, t), MD_lookupMeshDataId("Domain2"), Domain);
			}
			else{
				EN_attachDataInt (F_edge(Fac, t), MD_lookupMeshDataId("Domain"), Domain);		
			}
		}



	}

	FIter_delete(facit);


	EIter eit = M_edgeIter(theMesh);
	while (pEdge edge = EIter_next(eit)){


		int GeoFLG = 0;
		EN_getDataInt (edge, MD_lookupMeshDataId("geoflag"), &GeoFLG);

		int EdgeDomain1, EdgeDomain2, ENF;
		
		if (E_numFaces(edge)==1){
			EN_attachDataInt (edge, MD_lookupMeshDataId("Domain"), getFaceFlag(E_face(edge,0)));
			EN_attachDataInt (edge, MD_lookupMeshDataId("Domain2"), 0);
			EN_attachDataInt (edge, MD_lookupMeshDataId("CEDGE"), 1);
		}
		if (E_numFaces(edge)==2){
			EN_attachDataInt (edge, MD_lookupMeshDataId("Domain"), getFaceFlag(E_face(edge,0)));
			EN_attachDataInt (edge, MD_lookupMeshDataId("Domain2"), getFaceFlag(E_face(edge,1)));
			if (getFaceFlag(E_face(edge,0))!=getFaceFlag(E_face(edge,1))){
				EN_attachDataInt (edge, MD_lookupMeshDataId("CEDGE"), 1);
			}
			else{
			
				EN_attachDataInt (edge, MD_lookupMeshDataId("CEDGE"), 0);
			
			}
		}

		EN_getDataInt (edge, MD_lookupMeshDataId("Domain"), &EdgeDomain1);
		EN_getDataInt (edge, MD_lookupMeshDataId("Domain2"), &EdgeDomain2);
		ENF = E_numFaces(edge);
		int DF1 = 0;
		for (int r=0; r<ENF; r++){
				int FF;
				pFace FD = E_face(edge, r);
				if (DF1!=0){
					FF = DF1;
				}
				EN_getDataInt (FD, MD_lookupMeshDataId("Domain"), &DF1);
				
				if (DF1 == FF){
					EN_attachDataInt (edge, MD_lookupMeshDataId("DOMINIO"), FF);
				}
				if (DF1 != FF){
					EN_attachDataInt (edge, MD_lookupMeshDataId("DOMINIO"), 2200);
				}
		}
	
		dblarray vel(2, .0);
		dblarray vel2(2, .0);
		EdgePhysicalProperties* pEdgeCoeff = 0;// = getAttachedData_pointer<EdgePhysicalProperties>(edge);
		EN_getDataPtr(edge,MD_lookupMeshDataId( "PP_Data" ),(void**)&pEdgeCoeff);

		//if (EdgeDomain1!=0){
		
			cout << "Aresta " << EN_id(edge) << endl;
			vel = pEdgeCoeff->v_new[EdgeDomain1];
		//	if(EdgeDomain2 == 0){
				cout << "Dominio " << EdgeDomain1 << " vel "<< vel[0] <<" "<< vel[1] << endl;
				//STOP();
		//	}
		//}
			vel2 = pEdgeCoeff->v_new[EdgeDomain2];
		//	if(EdgeDomain2 == 0){
				cout << "Dominio " << EdgeDomain2 << " vel2 "<< vel2[0] <<" "<< vel2[1] << endl;

	}
	EIter_delete(eit);	
	//STOP();

}

void ErrorEstimator::LeitorDeLista (ErrorAnalysis *pErrorAnalysis, pMesh theMesh){
	cout << endl << "Iniciou Leitor de Lista" << endl << endl;

	bool UseWayNodes = false;

	set<pFace> NewElements;
	set <pVertex> InsertingNodes;
	set <pVertex> WayNodes;
	set<pVertex>::iterator VTiterator;

	for (std::list<pEntity>::iterator iter = elementList.begin(); iter!=elementList.end(); iter++){

		ElementsIds.insert(EN_id(*iter));
		NewElements.insert(*iter);
	}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	
if (UseWayNodes){
	//elementList.clear();

	EIter eiter = M_edgeIter(theMesh);
	while (pEdge edge = EIter_next(eiter)){
	///////////////////////////   Calculando a velocidade das arestas
		double Velocity;
		//for (int R=0;R<=2;R++){  //Laco para as arestas
		dblarray vel(2, .0);
		dblarray vel2(2, .0);
		EdgePhysicalProperties* pEdgeCoeff = 0;

		//pEdge edge = F_edge(*iter, R);
		int EdgeDomain1 = 0;
		int EdgeDomain2 = 0;

		EN_getDataInt (edge, MD_lookupMeshDataId("Domain"), &EdgeDomain1);
		EN_getDataInt (edge, MD_lookupMeshDataId("Domain2"), &EdgeDomain2);

		EN_attachDataDbl (edge, MD_lookupMeshDataId("Edge_Velocity"), 0);
		if (EdgeDomain2){
		
			
			EN_getDataPtr(edge,MD_lookupMeshDataId( "PP_Data" ),(void**)&pEdgeCoeff);
				
				vel = pEdgeCoeff->v_new[EdgeDomain1];
				vel2 = pEdgeCoeff->v_new[EdgeDomain2];
					
				double V1 = sqrt(vel[0]*vel[0] + vel[1]*vel[1]);
				double V2 = sqrt(vel2[0]*vel[0] + vel2[1]*vel[1]);
				Velocity = (V1+V2)/2;
					
				double V = 0;
				EN_getDataDbl(edge, MD_lookupMeshDataId("Edge_Velocity"), &V);
				if (V<Velocity){
					EN_attachDataDbl (edge, MD_lookupMeshDataId("Edge_Velocity"), Velocity);
				}
		}
			
		else{
			
			EN_getDataPtr(edge,MD_lookupMeshDataId( "PP_Data" ),(void**)&pEdgeCoeff);
			double ST = 0;
			pVertex X = E_vertex(edge,0);
			EN_getDataDbl(X, MD_lookupMeshDataId("sat_id"), &ST);
			if (ST){
			//	cout << "Saturacao " << ST << endl;
			//	cout << "Aresta " << EN_id(edge) << endl;
				vel = pEdgeCoeff->v_new[EdgeDomain1];
			//	cout << "Dominio " << EdgeDomain1 << " vel "<< vel[0] <<" "<< vel[1] << endl;	
				double V1 = sqrt(vel[0]*vel[0] + vel[1]*vel[1]);
				Velocity = V1;
				double V = 0;
				EN_getDataDbl(edge, MD_lookupMeshDataId("Edge_Velocity"), &V);
				if (V<Velocity){
					EN_attachDataDbl (edge, MD_lookupMeshDataId("Edge_Velocity"), Velocity);
				}
			}
		}
		
	}
	EIter_delete(eiter);
	
	///////////////////////////////////////
	
	///////////// Verificando quais são os primeiros nós a indicar o caminho
		VIter vit = M_vertexIter(theMesh);
		while (pVertex X = VIter_next(vit)){

			double ST = 0;
			EN_attachDataInt (X, MD_lookupMeshDataId("WayNode"), 0);
			EN_getDataDbl(X, MD_lookupMeshDataId("sat_id"), &ST);
			if (ST>0 && ST<0.3){
				cout << "Saturacao " << ST << endl;
				int VNE = V_numEdges(X);
				double Vmax = 0;
				for (int ET=0;ET<VNE;ET++){
					pEdge eg = V_edge(X,ET);
					
					double Vatual = 0;
					EN_getDataDbl(eg, MD_lookupMeshDataId("Edge_Velocity"), &Vatual);
					if (Vmax<Vatual){
						Vmax = Vatual;
					}
				}
				if (Vmax > 2){
					//EN_attachDataInt (X, MD_lookupMeshDataId("WayNode"), 1);
					//cout << "1" << endl;
					InsertingNodes.insert(X);
				}
				cout <<"Vmax " << Vmax << endl;
				//STOP();
			}
		}
		VIter_delete(vit);
	//////////////////////////
	
	
	cout << "InsertingNodes.size() " << InsertingNodes.size() << endl;
	
	
	for (int q=1;q<2;q++){
		for (VTiterator = InsertingNodes.begin(); VTiterator!=InsertingNodes.end(); VTiterator++){
			pVertex VIT = *VTiterator;
			int VNE = V_numEdges(VIT);
			int VF = 0;
			for (int u = 0; u<VNE; u++){
				int VE = 0;
				pEdge FE;
				pEdge E = V_edge(VIT, u);
				EN_getDataInt (E, MD_lookupMeshDataId("Edge_Velocity"), &VE);
				if (VF<VE){
					VF=VE;
					FE = E;
				}
				pVertex otherVertex = E_otherVertex(E, VIT);
				double SatV1 = 0;
				EN_getDataDbl(VIT,MD_lookupMeshDataId( "sat_id" ),&SatV1);
				double SatV2 = 0;
				EN_getDataDbl(otherVertex,MD_lookupMeshDataId( "sat_id" ),&SatV2);
				
				if(SatV2<=SatV1){
				
					EN_attachDataInt (otherVertex, MD_lookupMeshDataId("WayNode"), 1);
					InsertingNodes.insert(otherVertex);
					int VNF = E_numFaces(E);
					for (int uy=0;uy<VNF;uy++){
				
						pFace FG = E_face(E, uy);
						NewElements.insert(FG);
					}
				}
			}
		}
		
	}
	cout << "InsertingNodes.size() " << InsertingNodes.size() << endl;
	
	set<pFace>::iterator FITER;
	for (FITER=NewElements.begin();FITER!=NewElements.end();FITER++){
		pFace Fac = *FITER;
		ElementsIds.insert(EN_id(Fac));  
		elementList.push_back(Fac);
	
	}
	
	cout <<" Funcionou tubo blz" << endl;
	//STOP();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}



/////////////////////	 Deixando o remeshing GLOBAL
/*	elementList.clear();
	ElementsIds.clear();
	
	FIter facit = M_faceIter(theMesh);
	while (pFace Fac = FIter_next(facit)){
	
	
			
		ElementsIds.insert(EN_id(Fac));  
		elementList.push_back(Fac);


	}

	FIter_delete(facit);
*/
//////////////////////////////
	
///// CRIANDO UMA LISTA INTERESSANTE PARA MARKOS
/*
	elementList.clear();
	ElementsIds.clear();
	FIter facit = M_faceIter(theMesh);
	while (pFace Fac = FIter_next(facit)){
	
		//calculando o centroide
		pVertex V1,V2,V3;

		V1 = F_vertex(Fac,0);
		V2 = F_vertex(Fac,1);
		V3 = F_vertex(Fac,2);

		double xyz1[3],xyz2[3],xyz3[3];
		
		V_coord(V1, xyz1);
		V_coord(V2, xyz2);
		V_coord(V3, xyz3);
		
		double X = (xyz1[0]+xyz2[0]+xyz3[0])/3;
		double Y = (xyz1[1]+xyz2[1]+xyz3[1])/3;
			
		double D = sqrt((X*X)+(Y*Y));
		
		if (D < 0.35){	
			
			ElementsIds.insert(EN_id(Fac));
			elementList.push_back(Fac);

		}
		
		if ( X > 0.4 && X < 0.6){
			if ( Y > 0.4 && Y < 0.6){
				ElementsIds.insert(EN_id(Fac));
				elementList.push_back(Fac);
			}
		}


	}

	FIter_delete(facit);
*/
}












