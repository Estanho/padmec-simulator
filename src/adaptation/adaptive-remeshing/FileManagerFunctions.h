//FileManager class

#include "includes.h"
#include <string>

#include <stdio.h>
#include <stdlib.h>
#include "Gmsh.h"
#include "GModel.h"
//#include "MElement.h"
#include "MVertexGMSH.h"
#include "meshGEdgeGMSH.h"
#include <sstream>
#include <iostream>
#include <set>
#include "SPoint3GMSH.h"

#include "Rmsh/MainDrive.h"

class FileManagerFunctions
{
public:
	// Contructor and Destructor
	FileManagerFunctions ();
	~FileManagerFunctions();
	void CreateLineLoop (set<pEdge>, int, string, pMesh, Rmsh::MainDrive *drive);
	void CreateLineLoop (set<pEdge>, int, string, pMesh);
	void CreateLineLoop_3D (set<pEdge>, int, string, pMesh); 
	set<pEdge> SeparateCommonEdges  (set<pEdge>, int);

	void CreatePlaneSurfMatrix (int,int);
	string Create_Key_Point(pVertex); 
	set<pEdge> Merge_Edges (set<pEdge>, pMesh, map<int,pEdge>, int, string);
	bool Compare_Nodes(pVertex V1, pVertex V2, double tol);
	
	set<pEdge> AListedEdges;

private:



};

