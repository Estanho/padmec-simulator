/**
 * * RH_Refinement.h
 ** Created: 07-06-2013
 ** Author: Guilherme Caminha ( gpkc @ cin.ufpe.br )
 ** 
 ** RH-Refinement module using MAdLib (Mesh Adaption Library), which performs global node repositioning
 ** and mesh adaptation.
 **/


#ifndef RH_Refinement_Include
#define RH_Refinement_Include

#include <map>
#include <string>
#include <list>

#include "AMR.h"

#include "ModelInterface.h"
#include "MeshDataBaseInterface.h"
#include "AdaptInterface.h"
#include "PWLinearSField.h"
#include "AnalyticalSField.h"
#include "MAdResourceManager.h"

#include <iostream>
#include <fstream>

#define MEM 1

#define LINUX (defined(__linux) || defined(linux)) && MEM

class RH_Refinement : public AMR {
public:
	RH_Refinement(int argc, char* argv[], string, SimulatorParameters*);
	~RH_Refinement();
	
	void rodar(ErrorAnalysis *pErrorAnalysis, pMesh & theMesh);
	
	void setMeshFile(string);
	
	inline double mult(double x) {
		return multiplier = x*multiplier;
	}

private:
	bool interpolate;
	double multiplier = 1.0;

	vector<double> presData;
	vector<double> satData;
	
	string MeshFile; //Mesh filename path
	string GeoFile;  //Geometry filename path
	
	std::list<MAd::pEntity> listaver;
	//Dimension
	int dim;
	
	//Mesh and Model pointers
	MAd::pMesh MAdMesh;
	MAd::pGModel MAdModel;
	
	//Resource Manager
	MAd::MAdResourceManager ResMngr;
	double t0;
#if LINUX
	void displayMemoryUsage(std::string step="");
	void process_mem_usage(double& vm_usage, double& resident_set);
#endif
	
	//Mesh import functions (from FMDB mesh or .msh)
	bool importMesh(pMesh & theMesh);
	void importData(pMesh & theMesh);
	bool importMesh(std::string filename);
	
	//Mesh export functions (to FMDB mesh or .msh)
	void exportMesh(pMesh & theMesh);
	bool exportMesh(std::string filename);
	void exportData(pMesh & theMesh);
	
	//Ligação entre ID's do FMDB e do MAdLib
	std::map<int,int> MAdToMeshIds;
	std::map<int,int> MeshToMAdIds;
	
	//The SizeField used in the current operation
	MAd::PWLSField * sizeField;
	
	//Set the mesh size field
	void makeSizeField(std::list<pEntity>);
	
	//The Mesh Adapter
	MAd::MeshAdapter* adapter;

	//Insert an given face into the list of faces to be adapted
	double setEnt(pEntity);
	void setElementSize(pEntity, double);
	
public:
	//Clear all data
	void clear();
};

#endif

