/**
 * * RH_Refinement.cpp
 ** Created: 07-06-2013
 ** Author: Guilherme Caminha ( gpkc @ cin.ufpe.br )
 **/

// Alterar os arquivos de malha e de geometria aqui sem o "_sim"
// Alterar no numeric.dat usando o "_sim"

#define DEBUG_CHECK printf("\nDEBUG(pressione enter para continuar)"); \
getchar()

#include "RH_Refinement.h"

#include "MAdLib.h"
#include "MeshDataBaseInterface.h"
#include "MeshDataBaseIO.h"
#include "IsoMeshSize.h"
#include <unistd.h>

#include "ModelInterface.h"

#include "mAOMD.h"

#include "AdaptInterface.h"

#include <iostream>
#include <fstream>

RH_Refinement::RH_Refinement(int argc, char* argv[], string file, SimulatorParameters* pSimPar) {
	cout << endl << "======================";
	cout << endl << "STARTING MADLIB MODULE";
	cout << endl << "======================" << endl;

	//Initialize MAdLib
	MAdLibInitialize(&argc,&argv);
	
	MAdMesh = NULL;
	MAdModel = NULL;
	sizeField = NULL;
	adapter = NULL;
	GM_create(&MAdModel, "theModel");
	
	if(pSimPar->getInterpolationMethod() == H_REFINEMENT) {
		interpolate = true;
	} else {
		interpolate = false;
	}

	//Primeira execução BACALHO
	setMeshFile(file);
	
	GM_readFromGEO(MAdModel, GeoFile);
	
	displayMemoryUsage("MAdLib initialized");
	
}


RH_Refinement::~RH_Refinement() {
	if(MAdMesh) {
		M_delete(MAdMesh);
	}
	if(MAdModel) {
		GM_delete(MAdModel);
	}
	//if(out) {
	//	fclose(out);
	//}
}

//Run the adaptation procedure
void RH_Refinement::rodar(ErrorAnalysis *pErrorAnalysis, pMesh & theMesh) {
	cout << endl << "=========================================";
	cout << endl << "STARTING MADLIB MESH ADAPTATION PROCEDURE";
	cout << endl << "=========================================" << endl;
	
	t0 = ResMngr.getTime();
	
	if (!theMesh){
		throw Exception(__LINE__,__FILE__,"NULL Mesh!");
	}
	
	std::list<pEntity> elementList;
	std::set<pEntity> elementSet;
	pErrorAnalysis->getRefUnrefElementsList(theMesh,elementList,elementSet);
	
	//Copy the mesh from a FMDB mesh to a MADMESH
	cout << MAdMesh << endl;
	if(!MAdMesh) {
		importMesh(theMesh);
	}
	else if(!interpolate) {
		M_delete(MAdMesh);
		MAdMesh = NULL;
		MAdMesh = M_new_2(MAdModel);
		M_load(MAdMesh, "MAD2_par.msh");
	}

	M_writeMsh(MAdMesh, "MAD1.msh");

	makeSizeField(elementList);

	//Build the adaptation tool
	adapter = new MAd::MeshAdapter(MAdMesh,sizeField);

	adapter->setCollapseOnBoundary(true,1.e-6);
	adapter->setSwapOnBoundary(true, 1.e-6);
	adapter->setSliverQuality(0.3);
	adapter->setGeoTracking(true,
                              false,
                              0,
                              1.,
                              false,
                              false);
	adapter->setMaxIterationsNumber(50);
	adapter->setQualityMeasure(MAd::MAX_DIHEDRAL_ANGLE);
	
	//Import the Pressure and Saturation data into the MADMESH
	if(interpolate) {
		importData(theMesh);
	}
	
	displayMemoryUsage("Mesh adapter built");

	adapter->run();
	
	adapter->printStatistics(cout);
	
	M_writeMsh(MAdMesh,"MAD2.msh", 1);
	M_writeMsh(MAdMesh,"MAD2_par.msh", 2);
	system("gmsh -2 MAD2.msh -format msh1");
	system("gmsh -2 -parametric MAD2_par.msh");
	
	if(interpolate) {
		exportData(theMesh);
	}

	delete adapter;
	delete sizeField;
	//M_delete(MAdMesh);
	//MAdMesh = NULL;
	sizeField = NULL;
	adapter = NULL;
	listaver.clear();
	
	cout << "Adaptation procedure ocurred in " << ResMngr.getTime() - t0 << " seconds";
	displayMemoryUsage("After adaptation");

}

bool RH_Refinement::importMesh(pMesh & theMesh) {
	cout << "Loading the mesh..." << endl;
	double cpu_mesh_0 = ResMngr.getTime();
	
	//Used for vertex coordinates
	double xyz[3];
	
	//Clear id-to-id relation map
	MAdToMeshIds.clear();
	MeshToMAdIds.clear();
	
	//Start looping through vertices
	int nVerts = M_numVertices(theMesh);
	
	if(!nVerts) return false;
	
	//Create the mesh
	if(!MAdMesh)
		MAdMesh = M_new_2(MAdModel);
	
	cout << MeshFile << endl;
	M_load(MAdMesh, MeshFile.c_str());
	
	dim = theMesh->getDim();
	
	M_classifyEntities(MAdMesh);
	
	double cpu_mesh_tot = ResMngr.getTime() - cpu_mesh_0;
	cout << "Loaded the mesh in " << cpu_mesh_tot << " seconds\n";
	
	displayMemoryUsage("Mesh loaded");
	
	return true;
}


void RH_Refinement::displayMemoryUsage(std::string step)
{
	#if LINUX
	double ram, virt;
	
	process_mem_usage(virt,ram);
	
	#ifdef PARALLEL
	double send[2] = {virt,ram};
	double recv[2];
	MPI_Allreduce(send,recv,2,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	virt  = recv[0];
	ram = recv[1];
	#endif
	
	cout << "\n" << "Memory usage at step \'" << step << "\': " 
	<< ram  << " Mb (resident), "
	<< virt << " Mb (virtual)\n" << "\n";
	#endif
}

#if LINUX
void RH_Refinement::process_mem_usage(double& vm_usage, double& resident_set)
{
	using std::ios_base;
	using std::ifstream;
	using std::string;
	
	vm_usage     = 0.0;
	resident_set = 0.0;
	
	// 'file' stat seems to give the most reliable results
	//
	ifstream stat_stream("/proc/self/stat",ios_base::in);
	
	// dummy vars for leading entries in stat that we don't care about
	//
	string pid, comm, state, ppid, pgrp, session, tty_nr;
	string tpgid, flags, minflt, cminflt, majflt, cmajflt;
	string utime, stime, cutime, cstime, priority, nice;
	string O, itrealvalue, starttime;
	
	// the two fields we want
	//
	unsigned long vsize;
	long rss;
	
	stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
	>> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
	>> utime >> stime >> cutime >> cstime >> priority >> nice
	>> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest
	
	long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
	vm_usage     = vsize / 1024.0 / 1024;
	resident_set = rss * page_size_kb / 1024;
}
#endif

void RH_Refinement::makeSizeField(std::list<pEntity> elementList) {
	cout << "Starting SizeField Creation" << endl;
	sizeField = new MAd::PWLSField(MAdMesh);
	
	sizeField->setCurrentSize();
	
	for(std::list<pEntity>::iterator it = elementList.begin(); it != elementList.end(); ++it) {
		
		double h1, h2, h3, rat1, rat2, rat3;
		EN_getDataDbl(F_vertex(*it, 0), MD_lookupMeshDataId( "elem_height" ), &h1);
		EN_getDataDbl(F_vertex(*it, 0), MD_lookupMeshDataId( "height_ratio" ), &rat1);
		
		EN_getDataDbl(F_vertex(*it, 1), MD_lookupMeshDataId( "elem_height" ), &h2);
		EN_getDataDbl(F_vertex(*it, 1), MD_lookupMeshDataId( "height_ratio" ), &rat2);
	
		EN_getDataDbl(F_vertex(*it, 2), MD_lookupMeshDataId( "elem_height" ), &h3);
		EN_getDataDbl(F_vertex(*it, 2), MD_lookupMeshDataId( "height_ratio" ), &rat3);

		
		MAd::pVertex pv1, pv2, pv3;

		pv1 = M_vertex(MAdMesh, EN_id(F_vertex(*it, 0)));
		pv2 = M_vertex(MAdMesh, EN_id(F_vertex(*it, 1)));
		pv3 = M_vertex(MAdMesh, EN_id(F_vertex(*it, 2)));
		
		
		//if(rat1 < 1.1) {
		h1 = h1*multiplier;
		//}
		//if(rat2 < 1.1) {
		h2 = h2*multiplier;
		//}
		//if(rat3 < 1.1) {
		h3 = h3*multiplier;
		//}
			


		
		//if(h1 < 1) {
			sizeField->setSize((MAd::pEntity)pv1, h1);
		//}
		
		//if(h2 < 1) {
			sizeField->setSize((MAd::pEntity)pv2, h2);
		//}
		
		//if(h3 < 1) {
			sizeField->setSize((MAd::pEntity)pv3, h3);
		//}
		
	}

	//sizeField->scale(0.7);
	sizeField->smooth(0.5);
	
	cout << "Done SizeField Creation" << endl;
}

void RH_Refinement::importData(pMesh & theMesh) {
	cout << "Starting data import" << endl;
	pEntity node;
	double pres;
	double sat;
	double grad0;
	double grad1;
	double grad2;
	
	VIter vit = M_vertexIter(theMesh);

	//Vectors of nodal data
	vector<double> presData(M_numVertices(theMesh));
	vector<double> satData(M_numVertices(theMesh));
	vector<double> Sw_grad_0_0(M_numVertices(theMesh));
	vector<double> Sw_grad_0_1(M_numVertices(theMesh));
	vector<double> Sw_grad_0_2(M_numVertices(theMesh));

	
	int k = 0;
	while( (node = VIter_next(vit)) ){
		int id = EN_id(node);

		//Get nodal data
		EN_getDataDbl(node,MD_lookupMeshDataId("p_id"),&pres);
		EN_getDataDbl(node,MD_lookupMeshDataId("sat_id"),&sat);
		EN_getDataDbl(node,MD_lookupMeshDataId("Swgrad_0_0"),&grad0);
		EN_getDataDbl(node,MD_lookupMeshDataId("Swgrad_0_1"),&grad1);
		EN_getDataDbl(node,MD_lookupMeshDataId("Swgrad_0_2"),&grad2);
		
		presData[k] = pres;
		satData[k] = sat;
		Sw_grad_0_0[k] = grad0;
		Sw_grad_0_1[k] = grad1;
		Sw_grad_0_2[k] = grad2;
		
		k++;

	}
	VIter_delete(vit);

	//Attach the data into the MADMESH
	adapter->registerData("Saturation", satData);
	adapter->registerData("Pressure", presData);
	adapter->registerData("Swgrad_0_0", Sw_grad_0_0);
	adapter->registerData("Swgrad_0_1", Sw_grad_0_1);
	adapter->registerData("Swgrad_0_2", Sw_grad_0_2);
	
	cout << "Data import done" << endl;
}

std::string ReplaceString(std::string subject, const std::string& search,
                          const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
    return subject;
}

void RH_Refinement::setMeshFile(string file) {
	MeshFile = ReplaceString(file, "_sim.", ".");
	GeoFile = ReplaceString(MeshFile, ".msh", ".geo");
}

void RH_Refinement::exportData(pMesh & theMesh) {
	deleteMesh(theMesh);
	theMesh = MS_newMesh(0);

	//readmesh(theMesh,"MAD2.msh");

	MAd::VIter vit = M_vertexIter(MAdMesh);
	MAd::pVertex node;
	MAd::EIter eit = M_edgeIter(MAdMesh);
	MAd::pEdge edge;
	MAd::FIter fit = M_faceIter(MAdMesh);
	MAd::pFace face;
	
	//Vectors of nodal data
	vector<double> presData;
	vector<double> satData;
	adapter->getMeshData("Saturation", &satData);
	adapter->getMeshData("Pressure", &presData);
	
	int i = 0;
	while(node = VIter_next(vit)) {
		double coords[3];
		V_coord(node, coords);
		
		pEntity v = theMesh->createVertex(V_id(node), coords[0], coords[1], coords[2], 0);
	}
	
	while(edge = EIter_next(eit)) {
		//cout << V_id(E_vertex(edge, 0)) << " " << V_id(E_vertex(edge, 1)) << endl;
		int Physical;
		if ( GEN_type(E_whatIn(edge)) == 1 ) {
			Physical = GEN_physTag(E_whatIn(edge));
			theMesh->createEdge(theMesh->getVertex(V_id(E_vertex(edge, 0))), theMesh->getVertex(V_id(E_vertex(edge, 1))), theMesh->getGEntity(Physical, 1));
		} else {
			Physical = 0;
		}
	}
	
	mVertex* vertices[3];
	while(face = FIter_next(fit)) {
// 		//cout << V_id(F_vertex(face, 0)) << " " << V_id(F_vertex(face, 1)) << " " << V_id(F_vertex(face, 2)) << endl;
		vertices[0] = theMesh->getVertex(V_id(F_vertex(face, 0)));
		vertices[1] = theMesh->getVertex(V_id(F_vertex(face, 1)));
		vertices[2] = theMesh->getVertex(V_id(F_vertex(face, 2)));
		
		int Physical;
		if ( GEN_type(F_whatIn(face)) == 2 ) {
			Physical = GEN_physTag(F_whatIn(face));
		} else {
			Physical = 0;
		}
		
		pFace f = theMesh->createFaceWithVertices(vertices[0],vertices[1],vertices[2], theMesh->getGEntity(Physical, 2));
		EN_setID(f, EN_id((MAd::pEntity)face));
	}
	
	VIter_reset(vit);
	while(node = VIter_next(vit)) {
		pEntity v = theMesh->getVertex(V_id(node));

		int Physical;
		if ( GEN_type(V_whatIn(node)) == 0 ) {
			Physical = GEN_physTag(V_whatIn(node));
			cout << Physical << endl;
		} else {
			Physical = 0;
		}
		
		v->classify(theMesh->getGEntity(Physical, 0));
		
		EN_attachDataDbl(v,MD_lookupMeshDataId("p_id"),presData[i]);
		EN_attachDataDbl(v,MD_lookupMeshDataId("sat_id"),satData[i]);

		i++;
	}
	
	theMesh->modifyState(0,2,0);
	theMesh->modifyState(0,2);
	theMesh->modifyState(2,0);

	VIter_delete(vit);
	EIter_delete(eit);
	FIter_delete(fit);
	
}


void RH_Refinement::clear() {
	
}


