/*
 * CalculateElementsError.cpp
 *
 *  Created on: 24/02/2012
 *      Author: rogsoares
 */

#include "ErrorAnalysis_2D.h"

void getSatGradFromVertex(pVertex, SimulatorParameters* pSimPar, GetPFuncGrad pGetGradient, double* grad);
void getPresGradFromVertex(pVertex, SimulatorParameters* pSimPar, int dom, GetPFuncGrad pGetGradient, double* grad);

double matrix2norm(double* m) {
	double a  = m[0];
	double b  = m[1];
	double c  = m[2];
	double d  = m[3];
	
	double po = (-a*a - b*b - c*c - d*d);
	double sq = sqrt(po*po - 4*(-a*d*b*c - b*c*a*d + a*d*a*d + b*c*b*c));
	double s = (a*a + b*b + c*c + d*d);

	return std::max(sqrt(-sq + s)/sqrt(2),sqrt(sq + s)/sqrt(2));
}

void ErrorAnalysis_2D::calculate_ElementsError(pMesh theMesh, SimulatorParameters *pSimPar, PhysicPropData *pPPData, GetPFuncGrad pGetGradient, FIELD field){

	double v0_coords[3], v1_coords[3], v2_coords[3];	// face's nodes coords array
	double vector_0[3], vector_1[3], vector_2[3];		// face's edge vectors
	double grad_0[3], grad_1[3], grad_2[3];			// face's node gradients

#ifdef __ERROR_ANALYSIS_DEBUG__
	double grad[3] = {.0,.0,.0};
#endif

	FIter fit = M_faceIter (theMesh);						// loop over elements
	while (pEntity face = FIter_next(fit)){
		
		if ( !theMesh->getRefinementDepth(face)){

			pVertex vertices[3] = {face->get(0,0), face->get(0,1), face->get(0,2)};

			// get face's vertices coordinates
			V_coord(vertices[0],v0_coords);
			V_coord(vertices[1],v1_coords);
			V_coord(vertices[2],v2_coords);

			// make vector
			makeVector(v0_coords,v1_coords,vector_0);
			makeVector(v1_coords,v2_coords,vector_1);
			makeVector(v2_coords,v0_coords,vector_2);

			// get all three face's node gradients for 'i' field	
			double delta_grad0[3];
			double delta_grad1[3];
			double delta_grad2[3];
			double dg0[3];
			double dg1[3];
			double dg2[3];
			
			if(field == PRESSURE) {	
				int dom = getFaceFlag(face);
				getPresGradFromVertex(vertices[0], pSimPar, dom, pGetGradient, grad_0);
				getPresGradFromVertex(vertices[1], pSimPar, dom, pGetGradient, grad_1);
				getPresGradFromVertex(vertices[2], pSimPar, dom, pGetGradient, grad_2);
				const double* K = pSimPar->getPermeability(dom);
				const double mob0 = pPPData->getTotalMobility(vertices[0]);
				const double mob1 = pPPData->getTotalMobility(vertices[1]);
				const double mob2 = pPPData->getTotalMobility(vertices[2]);
				//double K2norm = matrix2norm(K);

				dg0[0] = (grad_1[0]-grad_0[0]);
				dg0[1] = (grad_1[1]-grad_0[1]);
				dg0[2] = (grad_1[2]-grad_0[2]);

				delta_grad0[0] = mob0*(dg0[0]*K[0] + dg0[1]*K[1]);
				delta_grad0[1] = mob0*(dg0[0]*K[2] + dg0[1]*K[3]);

				
				dg1[0] = (grad_2[0]-grad_1[0]);
				dg1[1] = (grad_2[1]-grad_1[1]);
				dg1[2] = (grad_2[2]-grad_1[2]);

				delta_grad1[0] = mob1*(dg1[0]*K[0] + dg1[1]*K[1]);
				delta_grad1[1] = mob1*(dg1[0]*K[2] + dg1[1]*K[3]);

				
				dg2[0] = (grad_0[0]-grad_2[0]);
				dg2[1] = (grad_0[1]-grad_2[1]);
				dg2[2] = (grad_0[2]-grad_2[2]);

				delta_grad2[0] = mob2*(dg2[0]*K[0] + dg2[1]*K[1]);
				delta_grad2[1] = mob2*(dg2[0]*K[2] + dg2[1]*K[3]);

			}
			else {
				getSatGradFromVertex(vertices[0], pSimPar, pGetGradient, grad_0);
				getSatGradFromVertex(vertices[1], pSimPar, pGetGradient, grad_1);
				getSatGradFromVertex(vertices[2], pSimPar, pGetGradient, grad_2);
				delta_grad0[0] = grad_1[0]-grad_0[0];
				delta_grad0[1] = grad_1[1]-grad_0[1];
				delta_grad0[2] = grad_1[2]-grad_0[2];
				delta_grad1[0] = grad_2[0]-grad_1[0];
				delta_grad1[1] = grad_2[1]-grad_1[1];
				delta_grad1[2] = grad_2[2]-grad_1[2];
				delta_grad2[0] = grad_0[0]-grad_2[0];
				delta_grad2[1] = grad_0[1]-grad_2[1];
				delta_grad2[2] = grad_0[2]-grad_2[2];
			}


			double error = (fabs(dot(delta_grad0,vector_0,2)) + fabs(dot(delta_grad1,vector_1,2)) + fabs(dot(delta_grad2,vector_2,2)))/3.0;

			double error_old = this->getElementError(face);
			//if(error_old < sqrt(error))
				this->setElementError(face, std::max(sqrt(error), error_old) /*(sqrt(error) + error_old)/2*/ );
			//this->setElementErrorVTK(face, sqrt(error));
		}
	}
	FIter_delete(fit);

#ifdef __ERROR_ANALYSIS_DEBUG__
	if (fabs(grad[0]+grad[1]+grad[2]) < 1e-8){
		throw Exception(__LINE__,__FILE__,"Gradients are NULL!");

	}

#endif
}

void getPresGradFromVertex(pVertex node, SimulatorParameters* pSimPar, int dom, GetPFuncGrad pGetGradient, double* grad) {
	SIter_const iter;
	int dom_counter;
	
	for (dom_counter = 0, iter=pSimPar->setDomain_begin(); iter!=pSimPar->setDomain_end(); iter++, dom_counter++){
		if(*iter == dom) {
			int belong;
			
			char dom_string[256]; sprintf(dom_string,"dom_str_%d",dom);
			EN_getDataInt(node,MD_lookupMeshDataId( dom_string ),&belong);
			
			if(!belong) {
				throw Exception(__LINE__,__FILE__,"The node actually does not belong to the given domain?");
			}
			else {
				break;
			}
		}
	}

	int row;
	char tag[4];
	sprintf(tag,"%d",dom_counter);
	pSimPar->getLocalNodeIDNumbering(node,tag,row);
	pGetGradient(node,dom_counter,row,grad);
}

//Itera em todos os domínios até achar o domínio certo de um dado vértice, e retorna seu gradiente
void getSatGradFromVertex(pVertex node, SimulatorParameters* pSimPar, GetPFuncGrad pGetGradient, double* grad) {
	//SIter_const iter;
	//int dom_counter;
	int row;

	//for (dom_counter = 0, iter=pSimPar->setDomain_begin(); iter!=pSimPar->setDomain_end(); iter++, dom_counter++){
		//int belong;
		//if(field == PRESSURE) {
		//	int dom = *iter; //Flag do domínio
		//	char dom_string[256]; sprintf(dom_string,"dom_str_%d",dom);
		//	EN_getDataInt(node,MD_lookupMeshDataId( dom_string ),&belong);
		//}
		//else 
		//	belong = 1;
//
		//if(belong) {
			//char tag[4];
			//sprintf(tag,"%d",0);

			pSimPar->getLocalNodeIDNumbering(node,const_cast<char*>("0"),row);
			pGetGradient(node,0,row,grad);

		//	break;
		//}
	//}

}